package ru.maxidom.convcore.bashexecutors;

public class SignMessage extends EdsExecutor {
	private final String bashSignMess = "/apps/conv-bin/sign.sh";
	private final String bashLog = "/apps/conv-bin/dellog.sh";
	private StringBuilder cmd;
	
	public SignMessage() {
		cmd = new StringBuilder();
	}
	@Override
	public void sign() throws Exception {
		if(isLog) {
			System.out.println(logFile);
		}
		cmd.append(bashSignMess);
		cmd.append(" ");
		cmd.append(inFile);
		cmd.append(" ");
		cmd.append(otFile);
		cmd.append(" ");
		cmd.append(sh1);
		cmd.append(" ");
		cmd.append(logFile);
		Process proc = null;
		int signCode = 0;
		int logCode =0;
		try {
			if(isLog) {
				System.out.println(cmd);
			}
			proc = Runtime.getRuntime().exec(new String[] { "bash", "-c", cmd.toString() });
			signCode = proc.waitFor();
		} catch (Exception e) {
			cmd.setLength(0);
			e.printStackTrace();
			throw new Exception("file error while sign");
		} 
		cmd.setLength(0);
		cmd.append(bashLog);
		cmd.append(" ");
		cmd.append(logFile);
		try {
			if(isLog) {
				System.out.println(cmd);
			}
			proc = Runtime.getRuntime().exec(new String[] { "bash", "-c", cmd.toString() });
			logCode = proc.waitFor();
		}
		 catch (Exception e) {
			 cmd.setLength(0);
			e.printStackTrace();
			throw new Exception("file error while sign");
		}
		cmd.setLength(0);
		if (signCode != 0  || logCode!=0)
			throw new Exception("couln't sign see log");
	};
	@Override
	public void executeBash(String inFile, String otFile, String otPar,boolean isLog) throws Exception {
		super.executeBash(inFile, otFile, otPar,isLog);
		sign();
	}
}
