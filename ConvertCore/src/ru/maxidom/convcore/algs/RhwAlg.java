package ru.maxidom.convcore.algs;
public class RhwAlg extends AlgParse {
@Override
public void execAlg(String inPar, String inFile, String otPar, String otFile,boolean isLoggin) throws Exception {
	/*это алгоритм Rhw*/
	super.execAlg(inPar, inFile, otPar, otFile, isLoggin);
	try {
	mReader.readData();
	mWriter.writeData();
	}
	finally {
		mReader.close();
		mWriter.close();
	super.mFileData = null;
	}
}
}
