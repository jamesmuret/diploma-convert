package ru.maxidom.convcore.algs;

import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.ParseFabric;
import ru.maxidom.convcore.parse.iRead;
import ru.maxidom.convcore.parse.iWrite;

public abstract class AlgParse extends AlgBase {
	FileDataBase mFileData;
	iRead mReader;
	iWrite mWriter;
	boolean isDebug;

@Override
public void execAlg(String inPar, String inFile, String otPar, String otFile, boolean isLoggin) throws Exception {
	/*это алгоритм RW*/
	FileDataBase.setLogged(isLoggin);
	FileDataBase mFileData = ParseFabric.getFileData(inPar, otPar);
	mFileData.setInFile(inFile);
	mFileData.setOtFile(otFile);
	mReader = ParseFabric.getReader(inPar);
	mWriter = ParseFabric.getWriter(otPar);
	mReader.setFileData(mFileData);
	mWriter.setFileData(mFileData);

}}
