package ru.maxidom.convcore.algs;

public interface iAlgExecute {
	public void execAlg(String inPar, String inFile, String otPar, String otFile, boolean isLoggin) throws Exception;
}
