package ru.maxidom.convcore.algs;

import ru.maxidom.convcore.bashexecutors.EdsExecutor;
import ru.maxidom.convcore.parse.ParseFabric;

public class FullAlg extends AlgBase {
@Override
public void execAlg(String inPar, String inFile, String otPar, String otFile, boolean isLoggin)
		throws Exception {
	EdsExecutor exec = ParseFabric.getBashExecutor(otPar);
	exec.executeBash(inFile, otFile, otPar,isLoggin);
}
}
