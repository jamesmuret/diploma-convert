package ru.maxidom.convcore.algs;

import ru.maxidom.convcore.parse.iDirectRead;
import ru.maxidom.convcore.parse.iDirectWrite;

public class RwAlg extends AlgParse {
@Override
public void execAlg(String inPar, String inFile, String otPar, String otFile,boolean isLoggin) throws Exception {
	super.execAlg(inPar, inFile, otPar, otFile, isLoggin);
	/*Rw*/ 
	iDirectWrite dirWriter =(iDirectWrite)mWriter;
	iDirectRead dirReader =(iDirectRead)mReader;
	try {
	dirReader.setWriter(dirWriter);
	dirReader.readData();
	dirWriter.finalizeData();
	}
	catch(Exception ex) {
		ex.printStackTrace();
		dirReader.close();
		dirWriter.close();
		throw new Exception(ex.getMessage());
	}
	finally {
	dirWriter = null;
	dirReader = null;
	super.mFileData = null;
}}
}
