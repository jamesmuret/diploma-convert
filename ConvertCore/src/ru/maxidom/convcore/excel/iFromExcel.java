package ru.maxidom.convcore.excel;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import org.apache.poi.EncryptedDocumentException;

public interface iFromExcel {
	void readFromTab(String filename) throws EncryptedDocumentException, IOException;

	void writeToXml(String filename) throws FileNotFoundException, XMLStreamException;
}
