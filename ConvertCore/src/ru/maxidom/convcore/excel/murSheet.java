package ru.maxidom.convcore.excel;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class murSheet {
	Optional<String> name;
	Optional<Integer> number;
	public List<murRow> rows;
    public List<murColumn> cols;
	public Integer getNumber() {
		return number.orElse(-1);
	}
	public void setNumber(Integer number) {
		this.number = Optional.ofNullable(number);
	}
	public String getName() {
		return name.orElse(" ");
	}
	public murSheet(int i) {
		rows = new LinkedList<>();
		cols = new LinkedList<>();
	}
	
	public murSheet() {
	}
	public void clear() {
		if(rows!=null) {
		rows.clear();
		rows=null;}
		if(cols!=null) {
		cols.clear();
		cols=null;
		}
		name = null;
		number = null;
	}
	public void setName(String name) {
		this.name = Optional.ofNullable(name);
	}
}
