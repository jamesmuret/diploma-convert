package ru.maxidom.convcore.excel;

import java.util.Optional;

public class murCellFormat {
	Optional<Integer> name;
	Optional<String> type;

	public Integer getName() {
		return name.orElse(-1);
	}

	public String getType() {
		return type.orElse(" ");
	}

	public void setName(Integer name) {
		this.name = Optional.ofNullable(name);
	}

	public void setType(String type) {
		this.type = Optional.ofNullable(type);
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " " + this.getName() + " " + this.getType();
	}
}
