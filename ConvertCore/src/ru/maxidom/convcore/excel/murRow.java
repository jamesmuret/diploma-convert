package ru.maxidom.convcore.excel;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class murRow {
	Optional<Integer> name;
	Optional<Integer> height;
	Optional<Boolean> hidden;
	public List<murCell> cells;

	public Integer getName() {
		return name.orElse(-1);
	}

	public murRow(int i) {
		cells = new LinkedList<>();
	}

	public murRow() {
	}

	public void setName(Integer name) {
		this.name = Optional.ofNullable(name);
	}

	public void clear() {
		this.name = null;
		this.height = null;
		this.hidden = null;
		if (cells != null)
			cells.clear();
		cells = null;
		name = null;
		height = null;
		hidden = null;
	}

	public Integer getHeight() {
		return height.orElse(15);
	}

	public Boolean getHidden() {
		return hidden.get();
	}

	public void setHeight(Integer height) {
		this.height = Optional.ofNullable(height);
	}

	public void setHidden(Boolean hidden) {
		this.hidden = Optional.ofNullable(hidden);
	}

}
