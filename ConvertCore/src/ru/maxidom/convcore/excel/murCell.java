package ru.maxidom.convcore.excel;

import java.util.Optional;

public class murCell {
	Optional<String> value;
	Optional<String> formula;
	Optional<String> name;
	Optional<Integer> number;
	Optional<String> datatype;

	/* ссылки на номера форматов и ограничений */
	Optional<Integer> format;
	Optional<Integer> style;
	Optional<Integer> validation;
	Optional<Integer> width;

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " " + this.getName() + " " + this.getNumber() + " "
				+ this.getDatatype() + " " + this.getStyle() + " " + this.getFormula() + " " + this.getValue();

	}

	public String getValue() {
		return value.orElse(" ");
	}

	public String getFormula() {
		return formula.orElse("");
	}

	public String getName() {
		return name.orElse("");
	}

	public Integer getNumber() {
		return number.orElse(-1);
	}

	public String getDatatype() {
		return datatype.orElse("");
	}

	public Integer getFormat() {
		return format.orElse(-1);
	}

	public Integer getStyle() {
		return style.orElse(-1);
	}

	public Integer getValidation() {
		return validation.orElse(-1);
	}

	public void setValue(String value) {
		this.value = Optional.ofNullable(value);
	}

	public void setFormula(String formula) {
		this.formula = Optional.ofNullable(formula);
	}

	public void setName(String name) {
		this.name = Optional.ofNullable(name);
	}

	public void setNumber(Integer number) {
		this.number = Optional.ofNullable(number);
	}

	public void setDatatype(String datatype) {
		this.datatype = Optional.ofNullable(datatype);
	}

	public void setFormat(Integer format) {
		this.format = Optional.ofNullable(format);
	}

	public void setStyle(Integer style) {
		this.style = Optional.ofNullable(style);
	}

	public void setValidation(Integer validation) {
		this.validation = Optional.ofNullable(validation);
	}

	public Integer getWidth() {
		return width.get();
	}

	public void setWidth(Integer width) {
		this.width = Optional.ofNullable(width);
	}

	public void clear() {
		value = null;
		formula = null;
		name = null;
		number = null;
		datatype = null;
		format = null;
		style = null;
		validation = null;
		width = null;
	}

}
