package ru.maxidom.convcore.excel;

import java.util.Optional;

public class murFont {
	Optional<String> name;
	Optional<String> size;
	Optional<Boolean> italic;
	Optional<Boolean> bold;
	Optional<Boolean> strike;
	Optional<Boolean> underlined;
	Optional<String> vertical;
	Optional<String> horizontal;

	public String getName() {
		return name.orElse("");
	}

	public String getSize() {
		return size.orElse(" ");
	}

	public Boolean getItalic() {
		return italic.orElse(false);
	}

	public Boolean getBold() {
		return bold.orElse(false);
	}

	public Boolean getStrike() {
		return strike.orElse(false);
	}

	public Boolean getUnderlined() {
		return underlined.orElse(false);
	}

	public String getVertical() {
		return vertical.orElse(" ");
	}

	public String getHorizontal() {
		return horizontal.orElse(" ");
	}

	public void setName(String name) {
		this.name = Optional.ofNullable(name);
	}

	public void setSize(String size) {
		this.size = Optional.ofNullable(size);
	}

	public void setItalic(Boolean italic) {
		this.italic = Optional.ofNullable(italic);
	}

	public void setBold(Boolean bold) {
		this.bold = Optional.ofNullable(bold);
	}

	public void setStrike(Boolean strike) {
		this.strike = Optional.ofNullable(strike);
	}

	public void setUnderlined(Boolean underlined) {
		this.underlined = Optional.ofNullable(underlined);
	}

	public void setVertical(String vertical) {
		this.vertical = Optional.ofNullable(vertical);
	}

	public void setHorizontal(String horizontal) {
		this.horizontal = Optional.ofNullable(horizontal);
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " " + this.getName() + " " + this.getItalic() + " " + this.getBold()
				+ " " + this.getHorizontal() + " " + this.getSize() + " " + this.getStrike() + " "
				+ this.getUnderlined() + " " + this.getVertical();

	}

	@Override
	public boolean equals(Object arg0) {
		if (this == arg0) {
			return true;
		}
		if (arg0 == null) {
			return false;
		}
		if (this.getClass() != arg0.getClass()) {
			return false;
		}
		murFont other = (murFont) arg0;
		if (name.get().equals(other.getName())) {
			if (bold.get().equals(other.bold.get())) {
				if (this.horizontal.get().equals(other.horizontal.get())) {
					if (this.italic.get().equals(other.italic.get())) {
						if (this.size.get().equals(other.size.get())) {
							if (this.strike.get().equals(other.strike.get())) {
								if (this.underlined.get().equals(other.underlined.get())) {
									if (this.vertical.get().equals(other.vertical.get()))
										return true;

								}
							}
						}

					}
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + name.get().hashCode();
		result = prime * result + bold.get().hashCode();
		result = prime * result + horizontal.get().hashCode();
		result = prime * result + italic.get().hashCode();
		result = prime * result + size.get().hashCode();
		result = prime * result + strike.get().hashCode();
		result = prime * result + underlined.get().hashCode();
		result = prime * result + vertical.get().hashCode();
		return result;

	}

}
