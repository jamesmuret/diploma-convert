package ru.maxidom.convcore.excel;

import java.util.Optional;

public class murValidation {

	Optional<String> name;
	Optional<String> min;
	Optional<String> max;
	Optional<String> datatype;
	Optional<String> operator;
	Optional<String> regions;

	public String getName() {
		return name.orElse(" ");
	}

	public String getMin() {
		return min.orElse(" ");
	}

	public String getMax() {
		return max.orElse(" ");
	}

	public String getDatatype() {
		return datatype.orElse(" ");
	}

	public String getOperator() {
		return operator.orElse(" ");
	}

	public void setName(String name) {
		this.name = Optional.ofNullable(name);
	}

	public void setMin(String min) {
		this.min = Optional.ofNullable(min);
	}

	public void setMax(String max) {
		this.max = Optional.ofNullable(max);
	}

	public void setDatatype(String datatype) {
		this.datatype = Optional.ofNullable(datatype);
	}

	public void setOperator(String operator) {
		this.operator = Optional.ofNullable(operator);
	}

	public String getRegions() {
		return regions.get();
	}

	public void setRegions(String regions) {
		this.regions = Optional.ofNullable(regions);
	}
}
