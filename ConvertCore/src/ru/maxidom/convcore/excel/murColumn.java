package ru.maxidom.convcore.excel;

import java.util.Optional;

public class murColumn {

	public Integer getMin() {
		return min.get();
	}
	public Integer getMax() {
		return max.get();
	}
	public void setMin(Integer min) {
		this.min = Optional.ofNullable(min);
	}
	public void setMax(Integer max) {
		this.max = Optional.ofNullable(max);
	}
	public String getDatatype() {
		return datatype.get();
	}
	public Integer getFormat() {
		return format.get();
	}
	public Integer getWidth() {
		return width.get();
	}

	public void setDatatype(String datatype) {
		this.datatype = Optional.ofNullable(datatype);
	}
	public void setFormat(Integer format) {
		this.format = Optional.ofNullable(format);
	}
	public void setWidth(Integer width) {
		this.width = Optional.ofNullable(width);
	}
	Optional<Integer> min;
	Optional<Integer> max;
	Optional<String> datatype;
	/* ссылки на номера форматов и ограничений */
	Optional<Integer> format;
	Optional<Integer> width;
}
