package ru.maxidom.convcore.excel;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class murBook {
	public List<murSheet> sheets;
	int aSheet;
	public Map<Integer, murStyle> styles;
	public Map<Integer, murCellFormat> formats;

	public murBook(int aSh) {
		aSheet = aSh;
		sheets = new LinkedList<>();
		styles = new HashMap<>();
		formats = new HashMap<>();
	}

	public murBook() {
		styles = new HashMap<>();
		formats = new HashMap<>();
	}
	public void clear() {
		if(sheets!=null) {
		sheets.clear();
		sheets=null;}
		if(styles!=null)
		styles.clear();
		styles=null;
		if(formats!=null)
		formats.clear();
		formats=null;
	}

}
