package ru.maxidom.convcore.excel;

import java.util.Optional;

public class murStyle implements Comparable<murStyle> {

	Optional<Integer> name;
	Optional<murFont> font;
	Optional<String> fontColor;
	Optional<String> backColor;
	Optional<String> frontColor;
	/* граница у стиля */
	Optional<String> left;
	Optional<String> right;
	Optional<String> top;
	Optional<String> bottom;
	Optional<String> pattern;

	public Integer getName() {
		return name.orElse(-1);
	}

	public Optional<murFont> getFont() {
		return font;
	}

	public String getFontColor() {
		return fontColor.orElse(" ");
	}

	public String getBackColor() {
		return backColor.orElse(" ");
	}

	public String getFrontColor() {
		return frontColor.orElse(" ");
	}

	public String getLeft() {
		return left.orElse(" ");
	}

	public String getRight() {
		return right.orElse(" ");
	}

	public String getTop() {
		return top.orElse(" ");
	}

	public String getBottom() {
		return bottom.orElse(" ");
	}

	public void setName(Integer name) {
		this.name = Optional.ofNullable(name);
	}

	public void setFont(Optional<murFont> font) {
		this.font = font;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = Optional.ofNullable(fontColor);
	}

	public void setBackColor(String backColor) {
		this.backColor = Optional.ofNullable(backColor);
	}

	public void setFrontColor(String frontColor) {
		this.frontColor = Optional.ofNullable(frontColor);
	}

	public void setLeft(String left) {
		this.left = Optional.ofNullable(left);
	}

	public void setRight(String right) {
		this.right = Optional.ofNullable(right);
	}

	public void setTop(String top) {
		this.top = Optional.ofNullable(top);
	}

	public void setBottom(String bottom) {
		this.bottom = Optional.ofNullable(bottom);
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + this.getName() + " " + this.getBackColor() + " " + this.getFontColor()
				+ " " + this.getFrontColor() + " " + this.getLeft() + " " + this.getRight() + " " + this.getTop() + " "
				+ this.getBottom();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + name.get().hashCode();
		result = prime * result + backColor.get().hashCode();
		result = prime * result + bottom.get().hashCode();
		result = prime * result + fontColor.get().hashCode();
		result = prime * result + left.get().hashCode();
		result = prime * result + right.get().hashCode();
		result = prime * result + top.get().hashCode();
		result = prime * result + font.get().hashCode();
		result = prime * result + frontColor.get().hashCode();
		return result;

	}

	@Override
	public boolean equals(Object arg0) {
		if (this == arg0) {
			return true;
		}
		if (arg0 == null) {
			return false;
		}
		if (this.getClass() != arg0.getClass()) {
			return false;
		}
		murStyle other = (murStyle) arg0;
		if (name.get().equals(other.getName())) {
			if (fontColor.get().equals(other.fontColor.get())) {
				if (this.backColor.get().equals(other.backColor.get())) {
					if (this.bottom.get().equals(other.bottom.get())) {
						if (this.top.get().equals(other.top.get())) {
							if (this.left.get().equals(other.left.get())) {
								if (this.right.get().equals(other.right.get())) {
									if (this.frontColor.get().equals(other.fontColor.get())) {
										if (this.font.get().equals(other.font.get()))
											return true;
									}
								}
							}
						}

					}
				}
			}
		}
		return false;
	}

	@Override
	public int compareTo(murStyle arg0) {
		 return this.equals(arg0)?1:0;
	}

	public String getPattern() {
		return pattern.get();
	}

	public void setPattern(String pattern) {
		this.pattern = Optional.ofNullable(pattern);
	}
}
