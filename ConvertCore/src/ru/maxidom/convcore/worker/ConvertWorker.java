package ru.maxidom.convcore.worker;


import ru.maxidom.convcore.algs.iAlgExecute;
import ru.maxidom.convcore.arbiter.BaseArbiter;
import ru.maxidom.convcore.arbiter.StringArbiter;


public class ConvertWorker {
	boolean isDebug;
	BaseArbiter strategyBroker = new StringArbiter();
	public ConvertWorker(boolean consoleLog) {
		isDebug = consoleLog;
	}
	public float doConvert(String inPar, String inFile, String otPar, String otFile,String algName) throws Exception {
		long start = System.currentTimeMillis();			
		strategyBroker.setParams(algName);
		iAlgExecute alg = strategyBroker.findStrategy();
		alg.execAlg(inPar,  inFile,  otPar, otFile,isDebug);
		long fin = System.currentTimeMillis();	
		return (fin-start)/1000.0f;
	}
}
