package ru.maxidom.convcore.filedata;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DataBase {
	private static String DBUrl;
	private static String DBName;
	private static String DBUser;
	private static String DBPassword;
	private Connection DBConnect;
	static {
		DBUrl = "jdbc:mysql://convert.m0.maxidom.ru:3306/convdb?autoReconnect=true&amp;useSSL=false;";
		DBName ="convdb";
		DBUser ="admin";
		DBPassword="Kee-K9si";
		}
   	public void closeConnect() throws Exception
	{
		DBConnect.close(); 
		DBConnect = null;
	}
   private void printMessage( boolean IsError, String strMsg )
    {
    	DateFormat dtForm = null;
    	Date curDate = null;
    	try
    	{
        	dtForm = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        	curDate = new Date();
        	
    		if( IsError == true)
    			System.out.println( dtForm.format(curDate) + " Error: " + strMsg );
    		else
    			System.out.println( dtForm.format(curDate) + " Info: " + strMsg );
    	}
    	catch( Exception e )
    	{
			System.out.println( " Error: " + e.getMessage() );
    	}
    	finally
    	{
    		dtForm = null;
    		curDate = null;
    	}
    }
   
   public Connection getConnection() throws Exception {
	   try {
		if(DBConnect==null || DBConnect.isClosed())
			   tryConnect();
	} catch (SQLException e) {
		printMessage(true,e.getMessage());
		throw new Exception("couldn't connect");
	}
	   return DBConnect;
   }
   
   /*подключение отдельное */
   public boolean connectToDataBase() {
		boolean Ret = false;
		try
		{
			DBConnect = DriverManager.getConnection( DBUrl,DBUser, DBPassword );
			printMessage( false, "Connect to database " + DBUrl );
			Ret = true;
		}
		catch( SQLException sqle)
		{
			printMessage( true, sqle.getMessage() );
			Ret = false;
		}
		catch( Exception e)
		{
			printMessage( true, e.getMessage() );
			Ret = false;
		}
		return true;
   } 
   /*спрашивает подключение если подклчен, то подключение*/
	 public boolean tryConnect()
	{
		boolean Ret = false;
		try
		{
			if( DBConnect != null  )
			{
				if( DBConnect.isClosed() == false ) return true;
				DBConnect = null;
			}
			DBConnect = DriverManager.getConnection( DBUrl,DBUser, DBPassword );
			Ret = true;
		}
		catch( SQLException sqle)
		{
			printMessage( true, sqle.getMessage() );
			Ret = false;
		}
		catch( Exception e)
		{
			printMessage( true, e.getMessage() );
			Ret = false;
		}
		return Ret;
	}
}
