package ru.maxidom.convcore.filedata;
import java.util.Map;
import java.util.Optional;

import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.Workbook;

import ru.maxidom.convcore.excel.murBook;

public class Document {
	/* документ содержит мета информацию о книге, */
	private Optional<String> mAuthor;
	private Optional<String> mDate;
	private murBook mBookData;
	private Map<DataValidation, Integer> vdmap;
	private Map<Integer, String> cfmap;
	private HSSFPalette pallette;
	private Map<Integer,CellStyle> mapst;
	private CreationHelper helper;
	public Document(int aSh, String author, String date) {
	//	this.setMapst(new HashMap<>());
	//	this.setCfmap(new HashMap<>());
		setD_book(new murBook(aSh));
		setD_author(author);
		setD_date(date);
	}
	
	public void clear() {
		mAuthor = null;
		mDate = null;
		mBookData.clear();
		mBookData= null;
		
	}
	
	public  void setHelper(Workbook w_wb) {
	 helper=w_wb.getCreationHelper();
	}

	public Map<Integer, String> getCfmap() {
		return cfmap;
	}
	public void setCfmap(Map<Integer, String> cfmap) {
		this.cfmap = cfmap;
	}
	public Map<DataValidation, Integer> getVdmap() {
		return vdmap;
	}
	public void setVdmap(Map<DataValidation, Integer> vdmap) {
		this.vdmap = vdmap;
	}
	public murBook getD_book() {
		return mBookData;
	}
	public void setD_book(murBook d_book) {
		this.mBookData = d_book;
	}
	public Map<Integer,CellStyle> getMapst() {
		return mapst;
	}
	public void setMapst(Map<Integer,CellStyle> mapst) {
		this.mapst = mapst;
	}
	public HSSFPalette getPallette() {
		return pallette;
	}
	public void setPallette(HSSFPalette pallette) {
		this.pallette = pallette;
	}
	public String getD_date() {
		return mDate.orElse("");
	}
	public void setD_date(String d_date) {
		this.mDate = Optional.ofNullable(d_date);
	}
	public String getD_author() {
		return mAuthor.orElse("");
	}
	public void setD_author(String d_author) {
		this.mAuthor = Optional.ofNullable(d_author);
	}
	public CreationHelper getHelper() {
		return helper;
	}
	public void setHelper(CreationHelper helper) {
		this.helper = helper;
	}
}
