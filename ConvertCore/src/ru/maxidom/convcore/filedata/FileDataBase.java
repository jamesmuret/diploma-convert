package ru.maxidom.convcore.filedata;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import ru.maxidom.convcore.parse.DocTags;

public  class FileDataBase {

	protected String inFile;
	protected String otFile;
	public  SimpleDateFormat m_df = new SimpleDateFormat(DocTags.DATEPATTERN);
	protected static boolean isLogged;
	public boolean isLogged() {
		return isLogged;
	}
	public static void setLogged(boolean isLogged) {
		FileDataBase.isLogged = isLogged;
	}
	public void setInFile(String inFile) {
		this.inFile = inFile;
	}
	public void setOtFile(String otFile) {
		this.otFile = otFile;
	}
	
	public static void printMessage( boolean IsError, String strMsg )
	    {	
	    	DateFormat dtForm = null;
	    	Date curDate = null;
	    	try
	    	{
	        	dtForm = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	        	curDate = new Date();
	    		if( IsError == true)
	    			System.out.println( dtForm.format(curDate) + " Error: " + strMsg );
	    		else if(isLogged)
	    			System.out.println( dtForm.format(curDate) + " Info: " + strMsg );
	    	}
	    	catch( Exception e )
	    	{
				System.out.println( " Error: " + e.getMessage() );
	    	}
	    	finally
	    	{
	    		dtForm = null;
	    		curDate = null;
	    	}
	    }
	    
	public SimpleDateFormat getM_df() {
		return m_df;
	}
	public String getInFile() {
		return inFile;
	}
	public String getOtFile() {
		return otFile;
	}
	public void close() {
		// TODO Auto-generated method stub
		
	}
}
