package ru.maxidom.convcore.filedata.test;

public interface iDumpable {
 public void dump(iDumpable dumper) throws Exception;
}
