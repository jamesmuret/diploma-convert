package ru.maxidom.convcore.filedata.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.filedata.TestExcelData;

public class TestDB {

	Connection DBSQLConnection;
	String DBConnection;
	String DBName;
	String DBUser;
	String DBPassword;
	String questTab;
	String topicTab;
	String answTab;
	String testTab;

	public void setDBConnection(String dBConnection) {
		DBConnection = dBConnection;
	}

	public void setDBName(String dBName) {
		DBName = dBName;
	}

	public void setDBUser(String dBUser) {
		DBUser = dBUser;
	}

	public void setDBPassword(String dBPassword) {
		DBPassword = dBPassword;
	}

	public boolean checkParams() {
		boolean ret = false;
		if (DBConnection != null && DBName != null && DBUser != null && DBPassword != null && questTab != null && topicTab != null
				&& answTab != null && testTab != null)
			ret = true;
		return ret;
	}

	public void setQuestTab(String questTab) {
		this.questTab = questTab;
	}

	public void setTopicTab(String topicTab) {
		this.topicTab = topicTab;
	}

	public void setAnswTab(String answTab) {
		this.answTab = answTab;
	}

	public void setTestTab(String testTab) {
		this.testTab = testTab;
	}

	public boolean isOpenConnect() {
		boolean ret = false;

		try {
			if (DBSQLConnection != null)
				ret = !DBSQLConnection.isClosed();
		} catch (Exception e) {

			FileDataBase.printMessage(true, e.getMessage());
		}

		return ret;
	}

	public boolean tryConnect() {

		if (isOpenConnect() == false) {
			if (openConnect() == false)
				return false;
		}
		return true;
	}

	public boolean openConnect() {
		closeConnect();
		try {
			DBSQLConnection = DriverManager.getConnection(DBConnection, DBUser, DBPassword);
		} catch (Exception e) {
			FileDataBase.printMessage(true, e.getMessage());
			return false;
		}
		return true;
	}

	public void closeConnect() {
		if (!isOpenConnect())
			return;
		try {
			DBSQLConnection.close();
		} catch (Exception e) {
		}
	}

	public boolean setAutoCommit(boolean IsAuto) {
		boolean Ret = false;

		try {
			if (tryConnect() == false)
				throw (new Exception("No database connection"));

			DBSQLConnection.setAutoCommit(IsAuto);
			Ret = true;
		} catch (SQLException sqle) {
			FileDataBase.printMessage(true, sqle.getMessage());
			Ret = false;
		} catch (Exception e) {
			FileDataBase.printMessage(true, e.getMessage());
			Ret = false;
		}

		return Ret;
	}

	public boolean commit() {
		boolean Ret = false;

		try {
			if (isOpenConnect() == false)
				throw (new Exception("No database connection"));

			DBSQLConnection.commit();
			Ret = true;
		} catch (SQLException sqle) {
			FileDataBase.printMessage(true, sqle.getMessage());
			Ret = false;
		} catch (Exception e) {
			FileDataBase.printMessage(true, e.getMessage());
			Ret = false;
		}

		return Ret;
	}

	public boolean rollBack() {
		boolean Ret = false;

		try {
			if (isOpenConnect() == false)
				throw (new Exception("No database connection"));
			DBSQLConnection.rollback();
			Ret = true;
		} catch (SQLException sqle) {
			FileDataBase.printMessage(true, sqle.getMessage());
			Ret = false;
		} catch (Exception e) {
			FileDataBase.printMessage(true, e.getMessage());
			Ret = false;
		}

		return Ret;
	}

	public boolean dumpTopic(TestNode tNode) {
		if (tNode == null) {
			FileDataBase.printMessage(true, "test data is null");
			return false;
		}
		String strQuery = "INSERT INTO " + DBName.trim() + ".test_tmp( id_task," + "topic_id," + "text) " + "VALUES("
				+ tNode.tData.getTaskId() + "," + tNode.tCount + ",'" + tNode.text + "')";
		if (tryConnect() == false) {
			FileDataBase.printMessage(true, "no database connection");
			return false;
		}
		try (Statement stmtCmd = DBSQLConnection.prepareStatement(strQuery)) {
			int res = stmtCmd.executeUpdate(strQuery);
			if (res == 0) {
				FileDataBase.printMessage(true, "couldn't insert answer");
				return false;
			}
		} catch (Exception ex) {
			FileDataBase.printMessage(true, ex.getMessage());
			return false;
		}
		return true;
	}

	public boolean dumpAnswer(TestNode tNode) {
		if (tNode == null) {
			FileDataBase.printMessage(true, "test data is null");
			return false;
		}
		String strQuery = null;
		try {
			strQuery = "INSERT INTO " + DBName.trim() + answTab.trim()
					+ "( id_task,id_topic,id_quest,answ_id,answ_type,text) " + "VALUES(" + tNode.tData.getTaskId() + ","
					+ tNode.tCount + "," + tNode.qCount + tNode.aCount + "," + Integer.parseInt(tNode.text) + ",'"
					+ tNode.answer + "')";
		} catch (Exception ex) {
			FileDataBase.printMessage(true, "answer parse error");
			return false;
		}
		if (tryConnect() == false) {
			FileDataBase.printMessage(true, "no database connection");
			return false;
		}
		try (Statement stmtCmd = DBSQLConnection.prepareStatement(strQuery)) {
			int res = stmtCmd.executeUpdate(strQuery);
			if (res == 0) {
				FileDataBase.printMessage(true, "couldn't insert answer");
				return false;
			}
		} catch (Exception ex) {
			FileDataBase.printMessage(true, ex.getMessage());
			return false;
		}
		return true;
	}

	public boolean findParams(Node doc) {
		String paramName;
		String paramValue;
		NodeList docList = doc.getChildNodes().item(0).getChildNodes();
		for (int i = 0; i < docList.getLength(); i++) {
			Node Node = docList.item(i);
			if (Node.getNodeName() == "database") {
				NodeList ParamsList = Node.getChildNodes();
				for (int j = 0; j < ParamsList.getLength(); j++) {
					Node Param = ParamsList.item(j);
					if (Param.getNodeName() == null || Param.hasChildNodes() == false
							|| Param.getFirstChild().getNodeValue() == null)
						continue;
					paramName = Param.getNodeName();
					paramValue = Param.getFirstChild().getNodeValue().toString().trim();
					FileDataBase.printMessage(false, "Find: " + paramName + " - " + paramValue);
					if (paramName == "DBConnection")
						setDBConnection(paramValue.trim());
					else if (paramName == "DBName")
						setDBName(paramValue.trim());
					else if (paramName == "DBUser")
						setDBUser(paramValue.trim());
					else if (paramName == "DBPassword")
						setDBPassword(paramValue.trim());
					else if (paramName == "TestTable")
						setTestTab(paramValue.trim());
					else if (paramName == "TopicTable")
						setTopicTab(paramValue.trim());
					else if (paramName == "QuestTable")
						setQuestTab(paramValue.trim());
					else if (paramName == "AnswTable")
						setAnswTab(paramValue.trim());
				}
			}
		}
		return checkParams();
	}
}
