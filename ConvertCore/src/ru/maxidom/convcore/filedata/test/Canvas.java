package ru.maxidom.convcore.filedata.test;

import java.util.Arrays;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ru.maxidom.convcore.filedata.FileDataBase;

public class Canvas {

	// данные о том, где расположены параметры шаблон
	// и еще узел отвечающий за дамп теста.
	int[] shops;
	int weightCol;
	int textCol;
	int answCol;

	int testTypeCol;
	int isExamCol;
	int isTimeLimitCol;
	int timeLimitCol;
	int isBackCol;
	int isShowsScoreCol;
	int isShowMessageCol;
	int decriptionCol;

	String radioStyle;
	String flagStyle;
	String replacePattern;
	String paramSheetName;
	String testSheetName;

	String topicMark;
	String testMark;
	String testIntroDefault;

	public String getTestIntroDefault() {
		return testIntroDefault;
	}

	public void setTestIntroDefault(String testIntroDefault) {
		this.testIntroDefault = testIntroDefault;
	}

	public String getTestMark() {
		return testMark;
	}

	public void setTestMark(String testMark) {
		this.testMark = testMark;
	}

	public String getTopicMark() {
		return topicMark;
	}

	public void setTopicMark(String topicMark) {
		this.topicMark = topicMark;
	}

	public int getAnswCol() {
		return answCol;
	}

	public void setAnswCol(int answCol) {
		this.answCol = answCol;
	}

	public boolean checkParams() {
		boolean ret = false;
		if (shops != null && answCol > 0 && weightCol > 0 && textCol > 0 && testTypeCol > 0 && isExamCol > 0
				&& isTimeLimitCol > 0 && timeLimitCol > 0 && isBackCol > 0 && isShowsScoreCol > 0
				&& isShowMessageCol > 0 && radioStyle != null && flagStyle != null && replacePattern != null
				&& paramSheetName != null && topicMark != null && testSheetName != null  && testMark!=null)
			ret = true;
		return ret;
	}

	public void setTestSheetName(String testSheetName) {
		this.testSheetName = testSheetName;
	}

	public String getParamSheetName() {
		return paramSheetName.toLowerCase();
	}

	public void setParamSheetName(String paramSheetName) {
		this.paramSheetName = paramSheetName;
	}

	public int[] getShops() {
		return shops;
	}

	public void setShops(int[] shops) {
		this.shops = shops;
	}

	public int getWeightCol() {
		return weightCol;
	}

	public void setWeightCol(int pointCol) {
		this.weightCol = pointCol;
	}

	public int getTextCol() {
		return textCol;
	}

	public void setTextCol(int textCol) {
		this.textCol = textCol;
	}

	public String getRadioStyle() {
		return radioStyle;
	}

	public void setRadioStyle(String radioStyle) {
		this.radioStyle = radioStyle;
	}

	public String getFlagStyle() {
		return flagStyle;
	}

	public void setFlagStyle(String flagStyle) {
		this.flagStyle = flagStyle;
	}

	public String getReplacePattern() {
		return replacePattern;
	}

	public void setReplacePattern(String replacePattern) {
		this.replacePattern = replacePattern;
	}

	public String getTestSheetName() {
		return testSheetName.toLowerCase();
	}

	public boolean findParams(Node doc) {
		String paramName;
		String paramValue;
		NodeList docList = doc.getChildNodes().item(0).getChildNodes();
		for (int i = 0; i < docList.getLength(); i++) {
			Node Node = docList.item(i);
			if (Node.getNodeName() == "canvas") {
				NodeList ParamsList = Node.getChildNodes();
				for (int j = 0; j < ParamsList.getLength(); j++) {
					Node Param = ParamsList.item(j);
					if (Param.getNodeName() == null || Param.hasChildNodes() == false
							|| Param.getFirstChild().getNodeValue() == null)
						continue;
					paramName = Param.getNodeName();
					paramValue = Param.getFirstChild().getNodeValue().toString().trim();
					FileDataBase.printMessage(false, "Find: " + paramName + " - " + paramValue);
					if (paramName == "Shops")
						setShops(Arrays.stream((paramValue.trim().split(","))).mapToInt(Integer::parseInt).toArray());
					else if (paramName == "WeigthCol")
						setWeightCol(Integer.parseInt(paramValue));
					else if (paramName == "TextCol")
						setTextCol(Integer.parseInt(paramValue));
					else if (paramName == "AnswCol")
						setAnswCol(Integer.parseInt(paramValue.trim()));
					else if (paramName == "TestTypeCol")
						setTestTypeCol(Integer.parseInt(paramValue.trim()));
					else if (paramName == "IsExamModeCol")
						setIsExamCol(Integer.parseInt(paramValue.trim()));
					else if (paramName == "IsTimeLimitCol")
						setIsTimeLimitCol(Integer.parseInt(paramValue.trim()));
					else if (paramName == "TimeLimitCol")
						setTimeLimitCol(Integer.parseInt(paramValue.trim()));
					else if (paramName == "IsBackCol")
						setIsBackCol(Integer.parseInt(paramValue.trim()));
					else if (paramName == "IsShowScoreCol")
						setIsShowsScoreCol(Integer.parseInt(paramValue.trim()));
					else if (paramName == "IsShowResultsMessageCol")
						setIsShowMessageCol(Integer.parseInt(paramValue.trim()));
					else if(paramName == "DescriptionCol")
						setDecriptionCol(Integer.parseInt(paramValue.trim()));
					else if (paramName == "ParamSheet")
						setParamSheetName(paramValue.trim());
					else if (paramName == "TestSheet")
						setTestSheetName(paramValue.trim());
					else if (paramName == "ReplacePattern")
						setReplacePattern(paramValue.trim());
					else if (paramName == "FlagButtonStyle")
						setFlagStyle(paramValue.trim());
					else if (paramName == "RadioButtonStyle")
						setRadioStyle(paramValue.trim());
					else if (paramName == "TopicMark")
						setTopicMark(paramValue.trim());
					else if(paramName =="TestMark")
						setTestMark(paramValue.trim());
					else if(paramName =="TestIntroDefault")
						setTestIntroDefault(paramValue.trim());
				}
			}
		}
		return checkParams();
	}

	public int getDecriptionCol() {
		return decriptionCol;
	}

	public void setDecriptionCol(int decriptionCol) {
		this.decriptionCol = decriptionCol;
	}

	public int getTestTypeCol() {
		return testTypeCol;
	}

	public void setTestTypeCol(int testTypeCol) {
		this.testTypeCol = testTypeCol;
	}

	public int getIsExamCol() {
		return isExamCol;
	}

	public void setIsExamCol(int isExamCol) {
		this.isExamCol = isExamCol;
	}

	public int getIsTimeLimitCol() {
		return isTimeLimitCol;
	}

	public void setIsTimeLimitCol(int isTimeLimitCol) {
		this.isTimeLimitCol = isTimeLimitCol;
	}

	public int getTimeLimitCol() {
		return timeLimitCol;
	}

	public void setTimeLimitCol(int timeLimitCol) {
		this.timeLimitCol = timeLimitCol;
	}

	public int getIsBackCol() {
		return isBackCol;
	}

	public void setIsBackCol(int isBackCol) {
		this.isBackCol = isBackCol;
	}

	public int getIsShowsScoreCol() {
		return isShowsScoreCol;
	}

	public void setIsShowsScoreCol(int isShowsScoreCol) {
		this.isShowsScoreCol = isShowsScoreCol;
	}

	public int getIsShowMessageCol() {
		return isShowMessageCol;
	}

	public void setIsShowMessageCol(int isShowMessageCol) {
		this.isShowMessageCol = isShowMessageCol;
	}


}
