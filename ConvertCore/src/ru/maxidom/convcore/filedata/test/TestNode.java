package ru.maxidom.convcore.filedata.test;

import java.util.HashMap;
import java.util.Optional;
import java.util.function.Consumer;

import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.filedata.TestExcelData;
import ru.maxidom.convcore.tab.readers.eventmodel.murSheetContentsHandlerTest.RW_TYPE;

public class TestNode implements iNodable, iDumpable {
	String text;
	String answer;
	StringBuilder exceptShops;
	Canvas cParams;
	int weight;
	boolean qFlag;
	boolean tFlag;
	int tCount;
	int qCount;
	int aCount;
	NodeState state = NodeState.EMPTY;;
	HashMap<Integer, Consumer<String>> nodeMethods;
	RW_TYPE rowType = RW_TYPE.FIRST_ROW;
	public TestExcelData tData;

	public Optional<HashMap<Integer, Consumer<String>>> getNodeMethods() {
		return Optional.of(nodeMethods);
	}

	public TestNode(Canvas params) {
		cParams = params;
		exceptShops = new StringBuilder("0");
		fillMethods();
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		if (text.toUpperCase().contains(cParams.getTopicMark()))
			tFlag = true;
	}

	public String getExceptShops() {
		return exceptShops.toString();
	}

	public void setExceptShops(String exceptShops) {
		if (!exceptShops.isEmpty()) {
			this.exceptShops.append(",");
			this.exceptShops.append(exceptShops);
		} else {
			this.exceptShops.setLength(0);
			this.exceptShops.append("0");
		}
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(String number) {
		try {
		this.weight = Integer.parseInt(number);
		}
		catch(Exception ex) {FileDataBase.printMessage(false, "weight is not digit");
		this.weight = 0;
		}
	}

	@Override
	public void dump(iDumpable dumper) throws Exception {

	}

	private void fillMethods() {
		nodeMethods = new HashMap<>();
		nodeMethods.put(cParams.answCol, this::setAnswer);
		nodeMethods.put(cParams.textCol, this::setText);
		nodeMethods.put(cParams.weightCol, this::setWeight);
		// включая магазины исключения
		for (int i = cParams.answCol + 1; i <= cParams.shops.length + cParams.answCol; i++) {
			nodeMethods.put(i, this::setExceptShops);
		}
	}

	@Override
	public void setRowType(RW_TYPE type) {
		this.rowType = type;
	}
	@Override
	public void setDataParams(TestExcelData tData) {
		this.tData = tData;
	}

	public void dump() throws Exception {
		state.dump(this);
		setExceptShops("");
		setText("");
		setWeight("0");
		setAnswer("");
	}
}
