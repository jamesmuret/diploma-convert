package ru.maxidom.convcore.filedata.test;

import ru.maxidom.convcore.filedata.FileDataBase;

public class TestAnswer {
	int answId;
	int questId;
	String answerText;
	boolean isTrue;
	int score;

	public TestAnswer(TestNode node) {
		this.answId = node.aCount;
		this.questId = node.qCount;
		this.answerText = node.answer;
		this.isTrue = checkAnswer(node.text);
		try {
		this.score =  Integer.parseInt(node.text);
				}
		catch(Exception ex) {
			FileDataBase.printMessage(true, "answer wrong Score"+node.aCount+node.qCount+node.tCount);
			score = isTrue?1:0;
		}
	}

	public int getIdAnsw() {
		return answId;
	}

	public void isTrue(Boolean score) {
		this.isTrue = score;
	}

	public int getIdQuest() {
		return questId;
	}

	public String getText() {
		return answerText;
	}

	public Boolean isTrue() {
		return isTrue;
	}

	public int getScore() {
		return score;
	}
	
	private boolean checkAnswer(String text) {
		boolean res = false;
		try {
			if(Integer.parseInt(text) > 0)
				res = true;
		}
		catch(Exception ex)
		{
			FileDataBase.printMessage(true, "answer score is a text");
		}
		return res;
	}
}
