package ru.maxidom.convcore.filedata.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import ru.maxidom.convcore.filedata.TestExcelData;
import ru.maxidom.convcore.parse.DocTags;
import ru.maxidom.convcore.tab.readers.eventmodel.murSheetContentsHandlerTest.RW_TYPE;

// класс, который включает в себя
public class GeneralNode implements iNodable, iDumpable {

	public String isExam() {
		return isExam;
	}

	public void setExam(String isExamCol) {
		this.isExam = isExamCol;
	}

	public void setTestType(String testTypeCol) {
		this.testType = testTypeCol;
	}

	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit;
	}

	public void setBackCol(String isBackCol) {
		this.isBackCol = isBackCol;
	}

	public void setShowsScore(String isShowsScoreCol) {
		this.isShowsScore = isShowsScoreCol;
	}

	public void setShowMessage(String isShowMessageCol) {
		this.isShowMessage = isShowMessageCol;
	}

	public void setIsTimeLimit(String isTimeLimit) {
		this.isTimeLimit = isTimeLimit;
	}

	String testType;
	String isExam;
	String isTimeLimit;
	String timeLimit;

	String isBackCol;
	String isShowsScore;
	String isShowMessage;
	String description;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	Canvas cParams;
	HashMap<Integer, Consumer<String>> nodeMethods = new HashMap<>();;
	NodeState state = NodeState.PARAM;
	RW_TYPE rowType = RW_TYPE.FIRST_ROW;
	TestExcelData data;
	

	public GeneralNode(Canvas params) {
		cParams = params;
		fillMethods();
	}

	private void fillMethods() {
		nodeMethods.put(cParams.testTypeCol, this::setTestType);
		nodeMethods.put(cParams.isExamCol, this::setExam);
		nodeMethods.put(cParams.isTimeLimitCol, this::setIsTimeLimit);
		nodeMethods.put(cParams.timeLimitCol, this::setTimeLimit);
		nodeMethods.put(cParams.isBackCol, this::setBackCol);
		nodeMethods.put(cParams.isShowsScoreCol, this::setShowsScore);
		nodeMethods.put(cParams.isShowMessageCol, this::setShowMessage);
		nodeMethods.put(cParams.decriptionCol,this::setDescription);
	}

	public Optional<HashMap<Integer, Consumer<String>>> getNodeMethods() {
		Optional<HashMap<Integer, Consumer<String>>> res = Optional.of(new HashMap<>());
		if(rowType==RW_TYPE.FIRST_ROW)
		 res = Optional.of(nodeMethods);
		return res;
	}

	public void setNodeMethods(HashMap<Integer, Consumer<String>> nodeMethods) {
		this.nodeMethods = nodeMethods;
	}

	@Override
	public void setRowType(RW_TYPE type) {
		rowType = type;
	}

	@Override
	public void setDataParams(TestExcelData tData) {
		data = tData;
	}
	@Override
	public void dump(iDumpable dumper) throws Exception {

	}

	@Override
	public void dump() throws Exception {
		state.dump(this);
	}

	public String getCustomDescript(String desc){
		String result = Arrays.stream(desc.split("\n")).map(p-> {
				if(p!=null){
				return cParams.getRadioStyle().replace(cParams.getReplacePattern(), p);
				}
				else {
				return DocTags.brake; 
				}
			}).collect(Collectors.joining());
		return result;	
		}
}
