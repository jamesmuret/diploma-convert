package ru.maxidom.convcore.filedata.test;

public class Test {

	boolean isExam;
	boolean isTimeLimit;
	String timeLimit;
	boolean isShowScore;
	boolean isShowResMes;
	boolean isBack;
	int testType;
	String testName;
	String description;
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Test() {
	
	}

	public boolean isExam() {
		return isExam;
	}

	public void setExam(boolean isExam) {
		this.isExam = isExam;
	}

	public boolean isTimeLimit() {
		return isTimeLimit;
	}

	public void setTimeLimit(boolean isTimeLimit) {
		this.isTimeLimit = isTimeLimit;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit;
	}

	public boolean isShowScore() {
		return isShowScore;
	}

	public void setShowScore(boolean isShowScore) {
		this.isShowScore = isShowScore;
	}

	public boolean isShowResMes() {
		return isShowResMes;
	}

	public void setShowResMes(boolean isShowResMes) {
		this.isShowResMes = isShowResMes;
	}

	public boolean isBack() {
		return isBack;
	}

	public void setBack(boolean isBack) {
		this.isBack = isBack;
	}

	public int getTestType() {
		return testType;
	}

	public void setTestType(int testType) {
		this.testType = testType;
	}

	public void close() {
		
		
	}
	
	
	
}
