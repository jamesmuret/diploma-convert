package ru.maxidom.convcore.filedata.test;

import java.util.Objects;
import java.util.stream.Stream;

public class TestQuestion {

	int idTopic;
	int idQuest;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	String text;
	String exceptShops;
	int weight;

	public TestQuestion(TestNode node) {
		idTopic = node.tCount;
		idQuest = node.qCount;
		text = node.text;
		weight =node.weight;
		exceptShops = node.getExceptShops();
	}
	
	public String getShops() {
		return exceptShops;
	}
	
	public boolean isShopInExcept (Integer shop) {
		
	boolean res = 	Stream.of(exceptShops.replaceAll("[^0-9.\\,]", "").split(",")).filter(Objects::nonNull).anyMatch(s->s.equals(shop.toString()));
		return res;
	}
	public Integer getIdTopic() {
		return idTopic;
	}
	public void setIdTopic(int idTopic) {
		this.idTopic = idTopic;
	}
	public int getIdQuest() {
		return idQuest;
	}
	public void setIdQuest(int idQuest) {
		this.idQuest = idQuest;
	}

	
	
}
