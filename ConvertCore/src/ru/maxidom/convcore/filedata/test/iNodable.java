package ru.maxidom.convcore.filedata.test;

import java.util.HashMap;
import java.util.Optional;
import java.util.function.Consumer;

import ru.maxidom.convcore.filedata.TestExcelData;
import ru.maxidom.convcore.tab.readers.eventmodel.murSheetContentsHandlerTest.RW_TYPE;

public interface iNodable {
	public Optional<HashMap<Integer, Consumer<String>>>  getNodeMethods();
	public void dump() throws Exception;
	public void setRowType(RW_TYPE type);
	public void setDataParams(TestExcelData tData);
}

