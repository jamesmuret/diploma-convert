package ru.maxidom.convcore.filedata.test;

public class TestTopic {
String text;
int topicId;
public TestTopic(TestNode node) {
	this.text = node.text;
	this.topicId = node.tCount;
}
public String getText() {
	return text;
}
public void setText(String text) {
	this.text = text;
}
public int getIdTopic() {
	return topicId;
}
public void setIdTopic(int topicId) {
	this.topicId = topicId;
}

}
