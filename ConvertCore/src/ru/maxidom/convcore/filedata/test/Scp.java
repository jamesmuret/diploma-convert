package ru.maxidom.convcore.filedata.test;

import java.nio.file.Files;
import java.nio.file.Paths;


import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import ru.maxidom.convcore.filedata.FileDataBase;


public class Scp {

	String SUser;
	String SPassword;
	String SHost;
	String SFUrl;
	int SPort;
	ChannelSftp sftpChannel;

	public void transferFiles(Iterable<String> files) throws Exception {
		for (String filename : files) {
			if(Files.exists(Paths.get(filename)))
			sftpChannel.put(filename, SFUrl + Paths.get(filename).getFileName());
		}
	}

	public void closeSftp() {

		if (sftpChannel != null ) {
			try {
				sftpChannel.getSession().disconnect();
				sftpChannel.disconnect();
				sftpChannel.exit();
			} catch (JSchException e) {
				FileDataBase.printMessage(true,e.getLocalizedMessage());
			}
			
		}
	}

	public void connectSftp() throws Exception {
		JSch jsch = new JSch();
		Session session = jsch.getSession(SUser, SHost);
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.setPassword(SPassword);
		session.connect();

		sftpChannel = (ChannelSftp) session.openChannel("sftp");
		sftpChannel.connect();
	}

	public boolean checkParams() {
		boolean ret = false;
		if (SUser != null && SPassword != null && SHost != null)
			ret = true;
		return ret;
	}

	public String getSUser() {
		return SUser;
	}

	public void setSUser(String sUser) {
		SUser = sUser;
	}

	public String getSPassword() {
		return SPassword;
	}

	public void setSPassword(String sPassword) {
		SPassword = sPassword;
	}

	public String getSHost() {
		return SHost;
	}

	public void setSHost(String sHost) {
		SHost = sHost;
	}

	public int getSPort() {
		return SPort;
	}

	public void setSPort(int sPort) {
		SPort = sPort;
	}

	public boolean findParams(Node doc) {
		String paramName;
		String paramValue;
		NodeList docList = doc.getChildNodes().item(0).getChildNodes();
		for (int i = 0; i < docList.getLength(); i++) {
			Node Node = docList.item(i);
			if (Node.getNodeName() == "sunrav") {
				NodeList ParamsList = Node.getChildNodes();
				for (int j = 0; j < ParamsList.getLength(); j++) {
					Node Param = ParamsList.item(j);
					if (Param.getNodeName() == null || Param.hasChildNodes() == false
							|| Param.getFirstChild().getNodeValue() == null)
						continue;
					paramName = Param.getNodeName();
					paramValue = Param.getFirstChild().getNodeValue().toString().trim();
					FileDataBase.printMessage(false, "Find: " + paramName + " - " + paramValue);
					if (paramName == "SUser")
						setSUser(paramValue.trim());
					else if (paramName == "SPort") {
						try {
							setSPort(Integer.parseInt(paramValue.trim()));
						} catch (Exception e) {
							FileDataBase.printMessage(true, "SPort:" + paramName + " - " + paramValue);
							setSPort(22);
						}
					} else if (paramName == "SHost")
						setSHost(paramValue.trim());
					else if (paramName == "SPassword")
						setSPassword(paramValue.trim());
					else if (paramName == "SFUrl")
						setSFUrl(paramValue.trim());
				}
			}
		}
		return checkParams();
	}

	public String getSFUrl() {
		return SFUrl;
	}

	public void setSFUrl(String sFUrl) {
		SFUrl = sFUrl;
	}

}
