package ru.maxidom.convcore.filedata.test;

import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.tab.readers.eventmodel.murSheetContentsHandlerTest.RW_TYPE;

enum NodeState implements iDumpable {
    PARAM {
	@Override
	public void dump(iDumpable dumpee) throws Exception {
	    GeneralNode node = (GeneralNode) dumpee;
	    if (!validate(node))
		return;
	    if (!uploadDB(node))
		throw new Exception("ошибка чтения параметров");
	}

	@Override
	protected void changeState(iDumpable node) {

	}

	@Override
	protected boolean validate(iDumpable dumpee) {
	    GeneralNode node = (GeneralNode) dumpee;
	    if (node.rowType != RW_TYPE.FIRST_ROW)
		return false;
	    return true;
	}

	@Override
	protected boolean uploadDB(iDumpable dumpee) {
	    GeneralNode node = (GeneralNode) dumpee;
	    try {
		node.data.test.isBack = Boolean.parseBoolean(node.isBackCol);
		node.data.test.isExam = Boolean.parseBoolean(node.isExam);
		node.data.test.isShowResMes = Boolean.parseBoolean(node.isShowMessage);
		node.data.test.isShowScore = Boolean.parseBoolean(node.isShowsScore);
		node.data.test.isTimeLimit = Boolean.parseBoolean(node.isTimeLimit);
		node.data.test.timeLimit = node.timeLimit;
		node.data.test.testType = Integer.parseInt(node.testType);
		node.data.test.description = node.description == null
			? node.data.getCanvasParams().getTestIntroDefault()
			: node.getCustomDescript(node.description);
	    } catch (Exception ex) {
		FileDataBase.printMessage(true, ex.getMessage());
		return false;
	    }
	    return true;
	}
    },

    EMPTY {
	@Override
	public void dump(iDumpable dumpee) throws Exception {
	    TestNode node = (TestNode) dumpee;
	    if (!validate(node))
		throw new Exception("ошибка валидации теста");
	    ;
	    changeState(node);
	    uploadDB(node);
	}

	@Override
	protected void changeState(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    node.state = TEST;
	    node.tCount = -1;
	    node.qCount = -1;
	    node.aCount = -1;
	}

	@Override
	protected boolean validate(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    if (node.text == null)
		return false;
	    return true;
	}

	@Override
	protected boolean uploadDB(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    node.tData.test.testName = node.text;
	    return false;
	}
    },
    TEST {
	@Override
	public void dump(iDumpable dumpee) throws Exception {
	    TestNode node = (TestNode) dumpee;
	    if (!validate(node))
		throw new Exception("ошибка валидации темы");
	    changeState(node);
	    uploadDB(node);

	}

	@Override
	protected void changeState(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    node.state = TOPIC;
	    node.tCount++;
	    node.aCount = -1;
	    node.tFlag = false;
	}

	@Override
	protected boolean validate(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    if (node.text == null)
		return false;
	    return true;
	}

	@Override
	protected boolean uploadDB(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    TestTopic tmpTopic = new TestTopic(node);
	    node.tData.topics.put(tmpTopic.getIdTopic(), tmpTopic);
	    return true;
	}
    },
    TOPIC {
	@Override
	public void dump(iDumpable dumpee) throws Exception {
	    TestNode node = (TestNode) dumpee;
	    if (!validate(node))
		throw new Exception("ошибка валидации вопроса");
	    changeState(node);
	    uploadDB(node);
	}

	@Override
	protected void changeState(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    node.state = QUESTION;
	    node.qCount++;
	    node.aCount = -1;
	    node.qFlag = false;
	}

	@Override
	protected boolean validate(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    if (node.text == null)
		return false;
	    return true;
	}

	@Override
	protected boolean uploadDB(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    TestQuestion tmpQ = new TestQuestion(node);
	    node.tData.questions.add(tmpQ);
	    return true;
	}
    },
    QUESTION {
	@Override
	public void dump(iDumpable dumpee) throws Exception {
	    TestNode node = (TestNode) dumpee;
	    if (!validate(node))
		throw new Exception("ошибка валидации ответа");
	    ;
	    changeState(node);
	    uploadDB(node);
	}

	@Override
	protected void changeState(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    node.state = ANSWER;
	    node.aCount++;
	    node.qFlag = false;
	}

	@Override
	protected boolean validate(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    if (node.answer == null || node.text == null)
		return false;
	    return true;
	}

	@Override
	protected boolean uploadDB(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    TestAnswer tmpA = new TestAnswer(node);
	    node.tData.answers.add(tmpA);
	    return true;
	}
    },
    ANSWER {
	@Override
	public void dump(iDumpable dumpee) throws Exception {
	    TestNode node = (TestNode) dumpee;
	    if (!validate(node))
		return;
	    changeState(node);
	    node.dump();
	}

	@Override
	protected void changeState(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    if (node.tFlag)
		node.state = TEST;
	    else {
		if (node.answer.isEmpty())
		    node.state = TOPIC;
		else
		    node.state = QUESTION;

	    }
	}

	@Override
	protected boolean validate(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    return true;
	}

	@Override
	protected boolean uploadDB(iDumpable dumpee) {
	    TestNode node = (TestNode) dumpee;
	    return false;
	}
    };

    protected abstract void changeState(iDumpable node);

    protected abstract boolean validate(iDumpable node);

    protected abstract boolean uploadDB(iDumpable node);
}
