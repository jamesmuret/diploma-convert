package ru.maxidom.convcore.filedata;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Consumer;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import ru.maxidom.convcore.filedata.test.Canvas;

import ru.maxidom.convcore.filedata.test.Scp;
import ru.maxidom.convcore.filedata.test.Test;
import ru.maxidom.convcore.filedata.test.TestAnswer;

import ru.maxidom.convcore.filedata.test.TestQuestion;
import ru.maxidom.convcore.filedata.test.TestTopic;
import ru.maxidom.convcore.filedata.test.iNodable;

public class TestExcelData extends FileDataBase {
	long taskId;
	iNodable currNode;
	Canvas cParams;
	Scp sParams;
	public Test test;
	public LinkedList<TestQuestion> questions;
	public LinkedList<TestAnswer> answers;
	public HashMap<Integer, TestTopic> topics;

	public iNodable getCurrNode() {
		return currNode;
	}

	public void setCurrNode(iNodable currNode) {
		this.currNode = currNode;
		currNode.setDataParams(this);
	}

	public Scp getsParams() {
		return sParams;
	}

	public void setsParams(Scp sParams) {
		this.sParams = sParams;
	}

	public boolean addQuestion(TestQuestion question) {
		if (question != null) {
			questions.add(question);
			return true;
		}
		return false;
	}

	public Canvas getCanvasParams() {
		return this.cParams;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public Scp getSParams() {
		return sParams;
	}

	public boolean addAnswer(TestAnswer answer) {
		if (answer != null) {
			answers.add(answer);
			return true;
		}
		return false;
	}

	public boolean addTopic(TestTopic topic) {
		if (topic != null) {
			topics.put(topic.getIdTopic(), topic);
			return true;
		}
		return false;
	}

	private void initTestTree() {
		test = new Test();
		questions = new LinkedList<>();
		answers = new LinkedList<>();
		topics = new HashMap<>();
	}

	public TestExcelData() throws Exception {
		cParams = new Canvas();
		sParams = new Scp();
		initTestTree();
		if (!loadParamsFromFile())
			throw new Exception("Couldn't find param file");
	}

	public boolean loadParamsFromFile() {
		Boolean res = false;
		try {
			ClassLoader classLoader = this.getClass().getClassLoader();
			DocumentBuilderFactory DocFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder DocBuilder = DocFactory.newDocumentBuilder();
			Document doc = DocBuilder.parse((classLoader.getResource(Dictionary.getTestFilePath()).toExternalForm()));
			res = cParams.findParams(doc);
			res = sParams.findParams(doc);
		} catch (Exception e) {
			FileDataBase.printMessage(true, e.getMessage());
			return res;
		}
		return res;
	}

	@Override
	public void setInFile(String inFile) {
		super.setInFile(inFile);
		parseTaskId();
	}

	private boolean parseTaskId() {
		String fileName = super.getInFile();
		int ft_st, id_st, id_en;
		boolean rs = true;
		ft_st = fileName.indexOf('_', 2);
		id_st = fileName.indexOf('_', ft_st + 1);
		id_en = fileName.indexOf('_', id_st + 1);
		try {
			taskId = Long.parseLong(fileName.substring(id_st + 1, id_en));
		} catch (NumberFormatException ex) {
			printMessage(true, "task id found error" + ex.getMessage());
			rs = false;
		}
		return rs;
	}

	@Override
	public String getInFile() {
		return super.getInFile();
	}

	@Override
	public String getOtFile() {
		return super.getOtFile();
	}

	public Optional<Consumer<String>> resolveMethod(int cell) throws Exception {
		Consumer<String> method;
		method = currNode.getNodeMethods().orElseGet(() -> null).get(cell);
		return Optional.ofNullable(method);
	}

	@Override
	public void close() {
		super.close();
		sParams.closeSftp();
		if (questions != null)
			questions.clear();
		if (topics != null)
			topics.clear();
		if (answers != null)
			answers.clear();
		FileDataBase.printMessage(false, "test data closed");
	}
}
