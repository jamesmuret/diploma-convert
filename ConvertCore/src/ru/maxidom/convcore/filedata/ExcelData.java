package ru.maxidom.convcore.filedata;
import java.text.SimpleDateFormat;

public class ExcelData extends FileDataBase {
	private Document mDoc;

	public Document getM_doc() {
		return mDoc;
	}

	public void clear() {
		if(mDoc!=null) {
		mDoc.clear();
		mDoc=null;
		}
	}
	
	
	public void setM_doc(Document m_doc) {
		mDoc = m_doc;
	}

	public  SimpleDateFormat getM_df() {
		return m_df;
	}


	public String getInFile() {
		return inFile;
	}
	public String getOtFile() {
		return otFile;
	}
}
