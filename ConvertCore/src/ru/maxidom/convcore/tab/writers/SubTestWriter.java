package ru.maxidom.convcore.tab.writers;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import ru.maxidom.convcore.filedata.TestExcelData;
import ru.maxidom.convcore.filedata.test.TestAnswer;
import ru.maxidom.convcore.filedata.test.TestQuestion;
import ru.maxidom.convcore.parse.DocTags;

public class SubTestWriter implements iSubWrite {

	TestExcelData testData;
	HashMap<Integer, Integer> topicLook;
	Integer shop;
	String filename;
	XMLStreamWriter writer;

	public SubTestWriter(String filename, int shop, TestExcelData testData) {
		this.filename = filename;
		this.shop = shop;
		this.testData = testData;
		this.topicLook = new HashMap<>();
	}

	private long countQuestions() {
		return testData.questions.stream().filter(s -> !s.isShopInExcept(shop)).count();
	}

	private void writeTopics() throws Exception {
		writer.writeStartElement(DocTags.THEMES);
		writer.writeStartElement(DocTags.Quantity);
		long topicCount = countThemes();
		writer.writeCharacters(String.valueOf(topicCount));
		writer.writeEndElement();

		int[] topicIdList = testData.questions.stream().filter(s -> !s.isShopInExcept(shop))
				.mapToInt(s -> s.getIdTopic()).distinct().toArray();
		for (int i = 0; i < topicCount; i++) {
			int topicId = topicIdList[i];
			topicLook.put(topicId, i);
			writer.writeStartElement(DocTags.THEME + i);
			writer.writeStartElement(DocTags.Caption);
			writer.writeCharacters(testData.topics.get(topicId).getText());
			writer.writeEndElement();
			writer.writeStartElement(DocTags.NumExam);
			writer.writeCharacters(String.valueOf(testData.questions.stream().filter(s -> s.getIdTopic() == topicId)
					.filter(s -> !s.isShopInExcept(shop)).distinct().count()));
			writer.writeEndElement();
			writer.writeStartElement(DocTags.ShowInResults);
			writer.writeCharacters(DocTags.ShowInResultsBody);
			writer.writeEndElement();
			writer.writeEndElement();
		}
		writer.writeEndElement();
	}

	private void writeQuestions() throws Exception {
		List<TestQuestion> questions = testData.questions.parallelStream().filter(s -> !s.isShopInExcept(shop)).collect(Collectors.toList());
		int i = 0;
		boolean multi;
		writer.writeStartElement(DocTags.QUESTIONS);
		for (TestQuestion question : questions) {
			multi = checkMulti(question.getIdQuest());
			writer.writeStartElement(DocTags.QUESTION + i++);
			writer.writeStartElement(DocTags.Weight);
			writer.writeCharacters(String.valueOf(question.getWeight()));
			writer.writeEndElement();
			writer.writeStartElement(DocTags.ThemeID);
			writer.writeCharacters(String.valueOf(topicLook.get(question.getIdTopic())));
			writer.writeEndElement();
			writer.writeStartElement(DocTags.Type);
			writer.writeCharacters(String.valueOf(multi?1:0));
			writer.writeEndElement();
			writer.writeStartElement(DocTags.TEXT_HTML);
			String questText = testData.getCanvasParams().getRadioStyle()
					.replace(testData.getCanvasParams().getReplacePattern(), question.getText());
			writer.writeCharacters(questText);
			writer.writeEndElement();
			if (!multi)
				writeAnswers(question.getIdQuest());
			else
				writeAnswersMulti(question.getIdQuest());
			writer.writeEndElement();
		}

		writer.writeEndElement();
	}

	private boolean checkMulti(int questionId) {
		boolean res = false;
		int resInt = testData.answers.stream().filter(s -> s.getIdQuest() == questionId)
				.mapToInt(s -> s.isTrue() ? 1 : 0).reduce((x, y) -> x + y).getAsInt();
		if (resInt > 1)
			res = true;
		return res;
	}

	private void writeAnswersMulti(int questionId) throws Exception {

		writer.writeStartElement(DocTags.ANSWERS);
		List<TestAnswer> answers = testData.answers.parallelStream().filter(s -> s.getIdQuest() == questionId).collect(Collectors.toList());
		int j = 0;
		for (TestAnswer answer : answers) {
			writer.writeStartElement(DocTags.ANSWER_ + answer.getIdAnsw());
			writer.writeStartElement(DocTags.TEXT);
			writer.writeCharacters(answer.getText());
			writer.writeEndElement();
			writer.writeStartElement(DocTags.TEXT_HTML);
			String answText = testData.getCanvasParams().getFlagStyle()
					.replace(testData.getCanvasParams().getReplacePattern(), answer.getText());
			writer.writeCharacters(answText);
			writer.writeEndElement();
			writer.writeStartElement(DocTags.SCORE);
			writer.writeCharacters(String.valueOf(answer.getScore()));
			writer.writeEndElement();
			writer.writeStartElement(DocTags.ISRIGHT);
			writer.writeCharacters(String.valueOf(answer.isTrue()).toUpperCase());
			writer.writeEndElement();
			writer.writeEndElement();
			j++;
		}

		writer.writeEndElement();
		writer.writeStartElement(DocTags.AQuantity);
		writer.writeCharacters(String.valueOf(j));
		writer.writeEndElement();

	}

	private void writeAnswers(int questionId) throws Exception {

		writer.writeStartElement(DocTags.ANSWERS);
		List<TestAnswer> answers = testData.answers.parallelStream().filter(s -> s.getIdQuest() == questionId)
				.collect(Collectors.toList());
		int j = 0;
		for (TestAnswer answer : answers) {
			writer.writeStartElement(DocTags.ANSWER_ + answer.getIdAnsw());
			writer.writeStartElement(DocTags.TEXT_HTML);
			String answText = testData.getCanvasParams().getRadioStyle()
					.replace(testData.getCanvasParams().getReplacePattern(), answer.getText());
			writer.writeCharacters(answText);
			writer.writeEndElement();
			writer.writeStartElement(DocTags.ISRIGHT);
			writer.writeCharacters(String.valueOf(answer.isTrue()).toUpperCase());
			writer.writeEndElement();

			writer.writeEndElement();
			j++;
		}

		writer.writeEndElement();

		writer.writeStartElement(DocTags.AQuantity);
		writer.writeCharacters(String.valueOf(j));
		writer.writeEndElement();
	}

	private long countThemes() {
		return testData.questions.stream().filter(s -> !s.isShopInExcept(shop)).mapToInt(s -> s.getIdTopic()).distinct()
				.count();
	}

	private void writeTest(long questCount) throws Exception {
		writer.writeStartElement(DocTags.TEST);
		writer.writeStartElement(DocTags.INFO);
		writer.writeStartElement(DocTags.Name);
		writer.writeCharacters(testData.test.getTestName().replace(testData.getCanvasParams().getTestMark(), "").trim() + " M" + shop);
		writer.writeEndElement();

		writer.writeStartElement(DocTags.Description);
		writer.writeCharacters(testData.test.getDescription());
		writer.writeEndElement();
		
		
		writer.writeStartElement(DocTags.QuestionsCount);
		writer.writeCharacters(String.valueOf(questCount));
		writer.writeEndElement();

		writer.writeStartElement(DocTags.TestType);
		writer.writeCharacters(String.valueOf(testData.test.getTestType()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.IsExamMode);
		writer.writeCharacters(String.valueOf(testData.test.isExam()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.IsTimeLimit);
		writer.writeCharacters(String.valueOf(testData.test.isTimeLimit()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.IsBack);
		writer.writeCharacters(String.valueOf(testData.test.isBack()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.IsShowScore);
		writer.writeCharacters(String.valueOf(testData.test.isShowScore()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.IsShowResultsMessage);
		writer.writeCharacters(String.valueOf(testData.test.isShowResMes()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.TimeLimit);
		writer.writeCharacters(String.valueOf(testData.test.getTimeLimit()));
		writer.writeEndElement();
		/* это относится к Doc.INFO */
		writer.writeEndElement();

	}

	public void writeShopFile() throws Exception {

		long questCount = countQuestions();
		if (questCount == 0)
			return;
		XMLOutputFactory saxFac = XMLOutputFactory.newFactory();
		try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filename))) {
			writer = saxFac.createXMLStreamWriter(bos, DocTags.ENCODE);
			String version = "1.0";
			writer.writeStartDocument(DocTags.ENCODE, version);
			writeTest(questCount);
			writeTopics();
			writeQuestions();
			writer.writeEndElement();
			writer.writeEndDocument();
			writer.flush();
			writer.close();
		}
	}

	@Override
	public String getWrittenFile() {
		return filename;
	}
}
