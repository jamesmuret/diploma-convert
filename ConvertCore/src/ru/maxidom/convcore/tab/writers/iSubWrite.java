package ru.maxidom.convcore.tab.writers;

public interface iSubWrite {

	public void writeShopFile() throws Exception;
	public String getWrittenFile();
}
