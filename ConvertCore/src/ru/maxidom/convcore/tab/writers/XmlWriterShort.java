package ru.maxidom.convcore.tab.writers;

import java.io.BufferedOutputStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map.Entry;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murCellFormat;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.excel.murStyle;
import ru.maxidom.convcore.filedata.ExcelData;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.DocTags;
import ru.maxidom.convcore.parse.iWrite;

public class XmlWriterShort implements iWrite {
	ExcelData e_data;
	XMLStreamWriter writer;

	@Override
	public void writeData() throws Exception {
		writeToXml(e_data.getOtFile());
		e_data.clear();
		e_data = null;
	}

	@Override
	public void setFileData(FileDataBase m_data) throws Exception {
		e_data = (ExcelData) m_data;

	}

	private void writeMeta(XMLStreamWriter writer, String newLine) throws XMLStreamException {
		String endocing = "UTF-8";
		String version = "1.0";
		writer.writeStartDocument(endocing, version);
		writer.writeStartElement(DocTags.DATA);
		writer.writeAttribute(DocTags.AUTHOR, e_data.getM_doc().getD_author());
		writer.writeAttribute(DocTags.DATE, e_data.getM_doc().getD_date());
	}


	private void writeSheets(XMLStreamWriter writer, String newLine) throws XMLStreamException {
		for (murSheet w_sheet : e_data.getM_doc().getD_book().sheets) {
			writer.writeStartElement(DocTags.WORKSHEET);
			writer.writeAttribute(DocTags.NAME, w_sheet.getName());
			writer.writeStartElement(DocTags.ROWS);
			for (murRow row : w_sheet.rows) {
				if (!row.cells.isEmpty()) {
					writer.writeStartElement(DocTags.ROW);
					writer.writeAttribute(DocTags.NUMBER, String.valueOf(row.getName()));
					// writer.writeAttribute(DocTags.height, row.getHeight());
					for (murCell w_cell : row.cells) {
						writer.writeStartElement(DocTags.CELL);
						writer.writeAttribute(DocTags.NAME, w_cell.getName());
						if (w_cell.getNumber() != -1)
							writer.writeAttribute(DocTags.NUMBER, String.valueOf(w_cell.getNumber()));
						writer.writeAttribute(DocTags.DATATYPE, w_cell.getDatatype());
						// writer.writeAttribute(DocTags.format, w_cell.getFormat());
						// writer.writeAttribute(DocTags.style, w_cell.getStyle());
						// if(!w_cell.getValidation().isEmpty())
						// writer.writeAttribute(DocTags.validation, w_cell.getValidation());
						// if(!w_cell.getWidth().isEmpty())
						// writer.writeAttribute(DocTags.width, w_cell.getWidth());
						// writer.writeCharacters(newLine);
						writer.writeStartElement(DocTags.VALUE);
						writer.writeCharacters(w_cell.getValue());
						writer.writeEndElement();

						if (!w_cell.getFormula().isEmpty()) {
							writer.writeStartElement(DocTags.FORMULA);
							writer.writeCharacters(w_cell.getFormula());
							writer.writeEndElement();
						}
						writer.writeEndElement();
						w_cell.clear();
						w_cell = null;
					}
					writer.writeEndElement();
					row.clear();
					row = null;
				}
			}
			w_sheet.clear();
			w_sheet = null;
			writer.writeEndElement();
		}
	}

	public void writeToXml(String filename) throws XMLStreamException, IOException {
		XMLOutputFactory saxFac = XMLOutputFactory.newFactory();
		writer = saxFac.createXMLStreamWriter(new BufferedOutputStream(new FileOutputStream(filename)), "UTF-8");
		String newLine = "\n";
		writeMeta(writer, newLine);
		writer.writeCharacters(newLine);
		writer.writeStartElement(DocTags.WORKSHEETS);
		writeSheets(writer, newLine);
		writer.writeEndElement();
		/* root element -data */
		writer.writeEndElement();
		writer.writeEndDocument();
		writer.flush();
		writer.close();
}

	@Override
	public void close() throws Exception {
	if(writer!=null)
		writer.close();
		
	}

	@Override
	public void setOutParameter(String outParameter) {
		// TODO Auto-generated method stub
		
	}
}