package ru.maxidom.convcore.tab.writers;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murColumn;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.filedata.ExcelData;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.DocTags;
import ru.maxidom.convcore.parse.iDirectWrite;

public class BigXmlWriter implements iDirectWrite {
	ExcelData e_data;
	XMLStreamWriter streamWriter;
	String newLine = "\n";

	@Override
	public void writeData() throws Exception {
		startDoc();
	}

	private void writeMeta(String newLine) throws XMLStreamException {
		String endocing = "UTF-8";
		String version = "1.0";
		streamWriter.writeStartDocument(endocing, version);
		streamWriter.writeCharacters(newLine);
		streamWriter.writeStartElement(DocTags.DATA);
	}

	@Override
	public void startCell(murCell cell) throws Exception {
		streamWriter.writeStartElement(DocTags.CELL);
		streamWriter.writeAttribute(DocTags.NUMBER, String.valueOf(cell.getNumber()));
		streamWriter.writeAttribute(DocTags.DATATYPE, cell.getDatatype());
		streamWriter.writeStartElement(DocTags.VALUE);
		streamWriter.writeCharacters(cell.getValue());
		streamWriter.writeEndElement();
		streamWriter.writeCharacters(newLine);
		if (!cell.getFormula().isEmpty()) {
			streamWriter.writeStartElement(DocTags.FORMULA);
			streamWriter.writeCharacters(cell.getFormula());
			streamWriter.writeEndElement();
		}
		cell.clear();
		cell = null;
	}

	@Override
	public void endCell() throws Exception {
		streamWriter.writeEndElement();
	}

	@Override
	public void startRow(murRow row) throws Exception {
		// здесь должны начаться cells
		streamWriter.writeStartElement(DocTags.ROW);
		streamWriter.writeAttribute(DocTags.NUMBER, String.valueOf(row.getName()));
		row.clear();
		row = null;
	}

	@Override
	public void endRow() throws Exception {
		// здесь закончатся cells
//		streamWriter.writeEndElement();
		streamWriter.writeCharacters(newLine);
		// здесь заканчивается row
		streamWriter.writeEndElement();
	}

	@Override
	public void startSheet(murSheet sheet) throws Exception {
		// TODO Auto-generated method stub
		streamWriter.writeStartElement(DocTags.WORKSHEET);
		streamWriter.writeAttribute(DocTags.NAME, sheet.getName());
		streamWriter.writeAttribute(DocTags.NUMBER, String.valueOf(sheet.getNumber()));
		streamWriter.writeCharacters(newLine);
		// здесь начинаются rows
		streamWriter.writeStartElement(DocTags.ROWS);
	}

	@Override
	public void endSheet() throws Exception {
		// здесь заканчиваются rows
		streamWriter.writeEndElement();
		// здесь заканчивается sheet
		streamWriter.writeEndElement();
	}

	@Override
	public void startDoc() throws Exception {
		// здесь открываю чтеца и все что нужно
		setStreamWriter(e_data.getOtFile());
		writeMeta(newLine);
		streamWriter.writeCharacters(newLine);
		streamWriter.writeStartElement(DocTags.WORKSHEETS);
		streamWriter.writeCharacters(newLine);
	}

	private void setStreamWriter(String filename) throws XMLStreamException, IOException {
		XMLOutputFactory saxFac = XMLOutputFactory.newFactory();
		streamWriter = saxFac.createXMLStreamWriter(new BufferedOutputStream(new FileOutputStream(filename)), "UTF-8");
		saxFac = null;
	}

	@Override
	public void closeDoc() throws Exception {
		/* заканчиваются worksheets */
		streamWriter.writeEndElement();
		/* root element -data */
		streamWriter.writeEndElement();
		streamWriter.writeCharacters(newLine);
		streamWriter.writeEndDocument();
		streamWriter.flush();
		streamWriter.close();
	}

	@Override
	public void close() throws Exception {
		if (streamWriter != null)
			streamWriter.close();
		finalizeData();
	}

	@Override
	public void finalizeData() throws Exception {
		if (e_data != null)
			e_data.clear();
		e_data = null;
	}

	@Override
	public void setFileData(FileDataBase m_data) throws Exception {
		e_data = (ExcelData) m_data;
	}

	@Override
	public void startColumn(murSheet sheet, murColumn col) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setOutParameter(String outParameter) {
		// TODO Auto-generated method stub
		
	}
}
