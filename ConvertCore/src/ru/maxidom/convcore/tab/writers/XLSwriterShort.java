package ru.maxidom.convcore.tab.writers;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murColumn;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.filedata.ExcelData;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.iWrite;
import ru.maxidom.convcore.parse.parseHelperXml;

public class XLSwriterShort implements iWrite {
	ExcelData e_data;
	CellStyle[] styles;
	@Override
	public void setFileData(FileDataBase m_data) throws Exception {
		e_data =(ExcelData)m_data;
		
	}
	@Override
	public void writeData() throws Exception {
		styles = new CellStyle[4];
	writeToXLSTab(e_data.getOtFile());
	}

	private void writeToXLSTab(String filename) throws Exception {
		/* Куда записывать в папку где лежат файлы */
		try (OutputStream fileOut = new FileOutputStream(filename)) {
			createBook().write(fileOut);
		}
	}
/*
	private void fillFormat(murCell d_cell, Cell w_cell) {
		murCellFormat tmp_f = e_data.getM_doc().getD_book().formats.get(d_cell.getFormat());
		e_data.logConsole(tmp_f.getType());
		e_data.logConsole(tmp_f.getName());
		short df = e_data.getM_doc().getHelper().createDataFormat().getFormat(tmp_f.getType());
		e_data.logConsole(String.valueOf(df));
		w_cell.getCellStyle().setDataFormat(e_data.getM_doc().getHelper().createDataFormat().getFormat(tmp_f.getType()));
	}
*/

	private void createStyles(Workbook w_wb) {
		for(int i=0;i<4;i++) {
		styles[i] = w_wb.createCellStyle();}
		styles[0].setDataFormat(e_data.getM_doc().getHelper().createDataFormat().getFormat("m/d/yy"));// date
		styles[1].setDataFormat(e_data.getM_doc().getHelper().createDataFormat().getFormat("0"));// numeric
		styles[2].setDataFormat(e_data.getM_doc().getHelper().createDataFormat().getFormat("@"));// Text
		styles[3].setDataFormat(e_data.getM_doc().getHelper().createDataFormat().getFormat("General"));// general
	}
	
	private void initDefaultCols(Sheet w_sheet, murSheet d_sheet) throws Exception {
		for (murColumn p_col: d_sheet.cols) {
			int min = p_col.getMin();
			int max = p_col.getMax();
			CellStyle defColStyle=this.styles[3];	
			for(int i= min;i<max;i++) {
				switch(p_col.getDatatype().toLowerCase().trim().charAt(0)) {
				case 'd' :defColStyle = this.styles[0];
					break;
				case 's':defColStyle = this.styles[1];
					break;
				case 'n': defColStyle = this.styles[2];
					break;
				case 'g':defColStyle = this.styles[3];	
				default:
					throw new Exception("unknown datatype");
				}
			w_sheet.setDefaultColumnStyle(i, defColStyle);	
			}
			d_sheet.cols.clear();
		}
//		w_sheet.setDefaultColumnWidth(15);*/
	}
	
	private void createSheets(Workbook w_wb) throws Exception {
		for (murSheet d_sheet : e_data.getM_doc().getD_book().sheets) {
			Sheet w_sheet = w_wb.createSheet(d_sheet.getName());
			initDefaultCols(w_sheet,d_sheet);
			createRows(w_sheet, d_sheet);
			// createValidations(w_sheet);
			d_sheet.clear();
			d_sheet=null;
		}
	}

	private void createRows(Sheet w_sheet, murSheet d_sheet) throws Exception {
		for (murRow d_row : d_sheet.rows) {
			int rowIndex = Integer.valueOf(d_row.getName()) - 1;
			Row w_row = w_sheet.createRow(rowIndex);
			 if(d_row.getHeight()!=-1)
				 w_row.setHeight(d_row.getHeight().shortValue());
				createCells(w_row, d_row, w_sheet,rowIndex);
		}
	}

	private void createCells(Row w_row, murRow d_row, Sheet w_sheet,int rowIndex) throws Exception {
		for (murCell d_cell : d_row.cells) {
			CellType ctype = parseHelperXml.parseCellType(d_cell.getDatatype().toUpperCase(), !d_cell.getFormula().isEmpty());
			Integer cellIndex;
			if (d_cell.getNumber()!=-1) {
				cellIndex = Integer.valueOf(d_cell.getNumber()) - 1;
			} else {
				cellIndex = CellReference.convertColStringToIndex(d_cell.getName());
			}
			Cell w_cell = w_row.createCell(cellIndex, ctype);
			fillCell(w_cell, d_cell,w_sheet,cellIndex,rowIndex);
		}
	}

	private void fillCell(Cell w_cell, murCell d_cell,Sheet w_sheet, int rowIndex,int cellIndex) throws Exception {
		if (!d_cell.getFormula().trim().isEmpty()) {
			// некоторые формулы не поддерживаются старым экселем
			try {
				w_cell.setCellFormula(d_cell.getFormula());
			} catch (Exception ex) {
				e_data.printMessage(true,"unsupported formula");
			}
		}
		CellStyle defColStyle=this.styles[2];	
		if(!d_cell.getValue().trim().isEmpty()) {
		switch (d_cell.getDatatype().trim().toUpperCase()) {
		case "s":
			w_cell.setCellValue(d_cell.getValue());
			defColStyle = this.styles[2];
			break;
		case "n":
			w_cell.setCellValue(Double.valueOf(d_cell.getValue().replaceAll(",", "\\.")));
			defColStyle = this.styles[1];
			break;
		case "d":
			try {
			w_cell.setCellValue(e_data.getM_df().parse(d_cell.getValue()));
			defColStyle = this.styles[0];
			} catch (ParseException e) {
				e_data.printMessage(true,e.getMessage());
			}
			break;
		case "e":
			w_cell.setCellErrorValue(Byte.valueOf(d_cell.getValue()));
			defColStyle = this.styles[2];
			break;
		default:
			throw new Exception("unreq cell Datatype"+d_cell.getName());
		}
		w_cell.setCellStyle(defColStyle);
		}
	}
	private Workbook createBook() throws Exception {
		/* создается новая книга, с числом листов и названием и автором */
		Workbook w_wb = new HSSFWorkbook();
		e_data.getM_doc().setHelper(w_wb);
		// fillDocMeta(w_wb);
		createStyles(w_wb);
		createSheets(w_wb);
		return w_wb;
	}
	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setOutParameter(String outParameter) {
		// TODO Auto-generated method stub
		
	}
}
