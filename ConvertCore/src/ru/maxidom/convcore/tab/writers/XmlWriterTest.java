package ru.maxidom.convcore.tab.writers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.filedata.TestExcelData;
import ru.maxidom.convcore.parse.DocTags;
import ru.maxidom.convcore.parse.DocTypes;
import ru.maxidom.convcore.parse.iWrite;

public class XmlWriterTest implements iWrite {
	TestExcelData testData;
	int shops[];
	boolean isZipped;
	String shopFileName;
	ArrayList<iSubWrite> subwriters;

	@Override
	public void writeData() throws Exception {
		initSubWriters();
		for (iSubWrite s : subwriters) {
			s.writeShopFile();
			FileDataBase.printMessage(false, "Writen file" + s.getWrittenFile());
		}
		if (isZipped) {
			zipMultipleFiles();
		} else {
			transferFiles();
			writeFileList(testData.getOtFile());
		}
	}

	private void writeFileList(String fileListName) throws Exception {
		XMLOutputFactory saxFac = XMLOutputFactory.newFactory();
		try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fileListName))) {
			XMLStreamWriter writer = saxFac.createXMLStreamWriter(bos, DocTags.ENCODE);
			String version = "1.0";
			writer.writeStartDocument(DocTags.ENCODE, version);
			writer.writeStartElement(DocTags.LIST);
			for (Path p : subwriters.stream().map(s -> Paths.get(s.getWrittenFile())).filter(Files::exists)
					.collect(Collectors.toList())) {
					writer.writeStartElement(DocTags.Filename);
					writer.writeCharacters(p.getFileName().toString());
					writer.writeEndElement();
			}
			writer.writeStartElement(DocTags.LIST);
			writer.writeEndDocument();
			writer.flush();
			writer.close();
		}
	}

	private void zipMultipleFiles() throws Exception {
		String dirPath = Paths.get(testData.getOtFile()).getParent().toString();
		String fileListPath = dirPath + File.separator + testData.getTaskId() +"_filelist.xml";
		try (FileOutputStream fos = new FileOutputStream(testData.getOtFile());
				ZipOutputStream zipOut = new ZipOutputStream(fos);) {
			for (Path xmlFilePath : subwriters.stream().map(s -> Paths.get(s.getWrittenFile())).filter(Files::exists)
					.collect(Collectors.toList())) {
				FileDataBase.printMessage(false, "zipping file" + xmlFilePath.getFileName());
				zipFile(zipOut,xmlFilePath);
		}
			writeFileList(fileListPath);
			zipFile(zipOut,Paths.get(fileListPath));
	}
	}
	
	private void zipFile(ZipOutputStream zipOut,Path filePath) throws IOException    {
		try (InputStream fis = Files.newInputStream(filePath)) {
			ZipEntry zipEntry = new ZipEntry(filePath.getFileName().toString());
			zipOut.putNextEntry(zipEntry);
			byte[] bytes = new byte[1024];
			int length;
			while ((length = fis.read(bytes)) >= 0) {
				zipOut.write(bytes, 0, length);
			}
		}
	}

	
	
	

	private void deleteFiles() throws Exception {
		if (subwriters != null) {
			for (iSubWrite s : subwriters) {
				File file = new File(s.getWrittenFile());
				if (file.exists()) {
					file.delete();
					FileDataBase.printMessage(false, "deleted file" + s.getWrittenFile());
				}
			}
			subwriters.clear();
		}
	}

	private void transferFiles() throws Exception {
		testData.getSParams().connectSftp();
		testData.getSParams()
				.transferFiles(subwriters.stream().map(s -> s.getWrittenFile()).collect(Collectors.toList()));
	}

	@Override
	public void setFileData(FileDataBase m_data) throws Exception {
		testData = (TestExcelData) m_data;
		shops = testData.getCanvasParams().getShops();
		subwriters = new ArrayList<>(shops.length);

	}

	public void initSubWriters() throws Exception {
		String dirPath = Paths.get(testData.getOtFile()).getParent().toString();
		String outFilePath = dirPath + File.separator + testData.getTaskId();
		String shopFile;
		calculateExceptShops();
		for (int i = 0; i < shops.length; i++) {
			shopFile = outFilePath + ""  + "_M" + shops[i] + ".xml";
			subwriters.add(new SubTestWriter(shopFile, shops[i], testData));
		}
		shopFile = outFilePath +  ".xml";
		subwriters.add(new SubTestWriterGeneral(shopFile, 0, testData));
	}

	private void calculateExceptShops() {
		Set<Integer> allShops = Arrays.stream(shops).boxed().collect(Collectors.toSet());
		shops = testData.questions.stream()
				.flatMap(s -> Arrays.stream((s.getShops().replaceAll("[^0-9.\\,]", "").split(","))))
				.filter(Objects::nonNull).mapToInt(s -> Integer.parseInt(s)).distinct()
				.filter(s -> allShops.contains(s)).toArray();
	}

	public void writeToXml(String filename) throws XMLStreamException, IOException {

	}

	@Override
	public void close() throws Exception {
		deleteFiles();
		if (testData != null) {
			testData.close();
		}
	}

	@Override
	public void setOutParameter(String OutParameter) {
		try {
			if (OutParameter.trim().split("#")[1].equalsIgnoreCase(("Z")));
				isZipped = true;
		} catch (Exception ex) {
			FileDataBase.printMessage(false, "not zipped");
		}

	}
}