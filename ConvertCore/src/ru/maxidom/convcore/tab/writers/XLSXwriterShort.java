package ru.maxidom.convcore.tab.writers;

import java.io.FileOutputStream;

import java.io.OutputStream;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murColumn;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.filedata.ExcelData;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.iWrite;

public class XLSXwriterShort implements iWrite {
	ExcelData e_data;
	CellStyle[] styles;

	@Override
	public void writeData() throws Exception {
	styles = new CellStyle[4];
	writeToXLSTab(e_data.getOtFile());
	e_data.clear();
	e_data=null;
	}
	@Override
	public void setFileData(FileDataBase m_data) throws Exception {
		e_data =(ExcelData)m_data;
		
	}
	private void writeToXLSTab(String filename) throws Exception {
		try (OutputStream fileOut = new FileOutputStream(filename)) {
		Workbook wb = createBook();
		wb.write(fileOut);
		((SXSSFWorkbook)wb).dispose();
		}
	}

	private void createSheets(Workbook w_wb) throws Exception {
		for (murSheet d_sheet : e_data.getM_doc().getD_book().sheets) {
			Sheet w_sheet = w_wb.createSheet(d_sheet.getName());
			initDefaultCols(w_sheet, d_sheet);
			createRows(w_sheet, d_sheet);
			d_sheet.clear();
			d_sheet=null;
		}
		styles=null;
	}

	private void initDefaultCols(Sheet w_sheet, murSheet d_sheet) throws Exception {
		for (murColumn p_col : d_sheet.cols) {
			int min = p_col.getMin()-1;
			int max = p_col.getMax()+1;
			CellStyle defColStyle = this.styles[3];
			for (int i = min; i < max; i++) {
				switch (p_col.getDatatype().toLowerCase().trim().charAt(0)) {
				case 'd':
					defColStyle = this.styles[0];
					break;
				case 'n':
					defColStyle = this.styles[1];
					break;
				case 's':
					defColStyle = this.styles[2];
					break;
				default:
					throw new Exception("unknown datatype");
				}
				w_sheet.setDefaultColumnStyle(i, defColStyle);
			}
		}
	}

	private void createStyles(Workbook w_wb) {
		for (int i = 0; i < 4; i++) {
			styles[i] = w_wb.createCellStyle();
		}
		styles[0].setDataFormat(e_data.getM_doc().getHelper().createDataFormat().getFormat("m/d/yy"));// date
		styles[1].setDataFormat(e_data.getM_doc().getHelper().createDataFormat().getFormat("0"));// numeric
		styles[2].setDataFormat(e_data.getM_doc().getHelper().createDataFormat().getFormat("@"));// text
		styles[3].setDataFormat(e_data.getM_doc().getHelper().createDataFormat().getFormat("General"));// general
	}

	private void createRows(Sheet w_sheet, murSheet d_sheet) throws Exception {
		for (murRow d_row : d_sheet.rows) {
			int rowIndex = Integer.valueOf(d_row.getName()) - 1;
			Row w_row = w_sheet.createRow(rowIndex);
			createCells(w_row, d_row, w_sheet, rowIndex);
		}
		d_sheet.clear();
		d_sheet=null;
	}

	private void createCells(Row w_row, murRow d_row, Sheet w_sheet, int rowIndex) throws Exception {
		for (murCell d_cell : d_row.cells) {
			Integer cellIndex;
			if (d_cell.getNumber()!=-1) {
				cellIndex = Integer.valueOf(d_cell.getNumber()) - 1;
			} else {
				cellIndex = CellReference.convertColStringToIndex(d_cell.getName());
			}
			Cell w_cell = w_row.createCell(cellIndex);
			fillCell(w_cell, d_cell, w_sheet, cellIndex, rowIndex);
		}
		d_row.clear();
		d_row = null;
	}

	private void fillCell(Cell w_cell, murCell d_cell, Sheet w_sheet, int rowIndex, int cellIndex) throws Exception {
		if (!d_cell.getFormula().trim().isEmpty()) {
			// некоторые формулы не поддерживаются старым экселем
			try {
				w_cell.setCellFormula(d_cell.getFormula());
			} catch (Exception ex) {
				e_data.printMessage(true,"unsupported Formula");
			}
		}
		CellStyle defCellStyle = this.styles[3];
		if (!d_cell.getValue().trim().isEmpty()) {
			try {
			switch (d_cell.getDatatype().trim().toLowerCase().charAt(0)) {
			case 's':
				w_cell.setCellValue(d_cell.getValue());
				defCellStyle = this.styles[2];
				break;
			case 'n':
				w_cell.setCellValue(Double.valueOf(d_cell.getValue().replaceAll(",", "\\.")));
				defCellStyle = this.styles[1];
				break;
			case 'd':
					w_cell.setCellValue(e_data.getM_df().parse(d_cell.getValue()));
					defCellStyle = this.styles[0];
				break;
			case 'e':
				w_cell.setCellErrorValue(Byte.valueOf(d_cell.getValue()));
				defCellStyle = this.styles[2];
				break;
			case 'b':
				w_cell.setCellValue(d_cell.getValue());
				defCellStyle = this.styles[2];
				break;
			default:
				throw new Exception("unreq cell Datatype" + d_cell.getName());
			}
		
			}
			catch(Exception ex) {
				w_cell.setCellValue(d_cell.getValue());
			}
			w_cell.setCellStyle(defCellStyle);
		}
		d_cell.clear();
		d_cell=null;
	}
	private Workbook createBook() throws Exception {
		/* создается новая книга, с числом листов и названием и автором */
		Workbook w_wb = new SXSSFWorkbook();
		e_data.getM_doc().setHelper(w_wb);
		createStyles(w_wb);
		createSheets(w_wb);
		e_data.getM_doc().getD_book().clear();
		return w_wb;
	}
	@Override
	public void close() throws Exception {
		
	}
	@Override
	public void setOutParameter(String outParameter) {
		// TODO Auto-generated method stub
		
	}

}
