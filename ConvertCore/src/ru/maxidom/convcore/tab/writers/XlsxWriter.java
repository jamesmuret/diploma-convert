package ru.maxidom.convcore.tab.writers;

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.Map.Entry;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murCellFormat;
import ru.maxidom.convcore.excel.murFont;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.excel.murStyle;
import ru.maxidom.convcore.filedata.ExcelData;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.iWrite;
import ru.maxidom.convcore.parse.parseHelperXml;

public class XlsxWriter implements iWrite {
	ExcelData e_data;

	@Override
	public void writeData() throws Exception {
		writeToXLSTab(e_data.getOtFile());
	}
	@Override
	public void setFileData(FileDataBase m_data) throws Exception {
		e_data =(ExcelData)m_data;
		
	}
	private void writeToXLSTab(String filename) throws Exception {
		/* Куда записывать в папку где лежат файлы */
		try (OutputStream fileOut = new FileOutputStream(filename)) {
			createBook().write(fileOut);
		}
	}

	@SuppressWarnings("deprecation")
	private XSSFColor getColorPattern(String col) {
		String colors[] = col.split(":");
		Color clr = new java.awt.Color(Integer.valueOf(colors[0]), Integer.valueOf(colors[1]),
				Integer.valueOf(colors[2]));
		XSSFColor res_color = new XSSFColor(clr);
		return res_color;
	}

	private void fillFormat(murCell d_cell, Cell w_cell) {
		murCellFormat tmp_f = e_data.getM_doc().getD_book().formats.get(d_cell.getFormat());
		e_data.printMessage(false,tmp_f.getType());
		e_data.printMessage(false,String.valueOf(tmp_f.getName()));
		short df = e_data.getM_doc().getHelper().createDataFormat().getFormat(tmp_f.getType());
		e_data.printMessage(false,String.valueOf(df));
		w_cell.getCellStyle()
				.setDataFormat(e_data.getM_doc().getHelper().createDataFormat().getFormat(tmp_f.getType()));
	}

	private void createStyles(Workbook w_wb) {
		if (!e_data.getM_doc().getD_book().styles.isEmpty()) {
			for (Entry<Integer, murStyle> d_style : e_data.getM_doc().getD_book().styles.entrySet()) {
//			this.logConsole("/***********************/");
				XSSFCellStyle w_style = (XSSFCellStyle) w_wb.createCellStyle();
				XSSFFont w_font = (XSSFFont) w_wb.createFont();
				murFont d_font = d_style.getValue().getFont().get();
				Integer r_styleInt = d_style.getValue().getName();
//			w_style.setFillBackgroundColor(getColorPattern(d_style.getValue().getBackColor()));
				w_style.setFillForegroundColor(getColorPattern(d_style.getValue().getFrontColor()));
				w_font.setColor(getColorPattern(d_style.getValue().getFontColor()));
				w_font.setFontName(d_font.getName());
				w_font.setFontHeightInPoints(Short.valueOf(d_font.getSize()));
				w_font.setBold(d_font.getBold());
				w_font.setItalic(d_font.getItalic());
				w_font.setStrikeout(d_font.getStrike());
				w_font.setUnderline(d_font.getUnderlined() ? XSSFFont.U_SINGLE : XSSFFont.U_NONE);
				w_style.setBorderLeft(BorderStyle.valueOf(d_style.getValue().getLeft()));
				w_style.setBorderBottom(BorderStyle.valueOf(d_style.getValue().getBottom()));
				w_style.setBorderRight(BorderStyle.valueOf(d_style.getValue().getRight()));
				w_style.setBorderTop(BorderStyle.valueOf(d_style.getValue().getTop()));
				w_style.setFont(w_font);
				w_style.setAlignment(HorizontalAlignment.valueOf(d_font.getHorizontal()));
				w_style.setVerticalAlignment(VerticalAlignment.valueOf(d_font.getVertical()));
				w_style.setFillPattern(FillPatternType.valueOf(d_style.getValue().getPattern()));
				e_data.getM_doc().getMapst().put(r_styleInt, w_style);
			}
		}
	}

	private void createSheets(Workbook w_wb) throws Exception {
		for (murSheet d_sheet : e_data.getM_doc().getD_book().sheets) {
			Sheet w_sheet = w_wb.createSheet(d_sheet.getName());
			createRows(w_sheet, d_sheet);
			createValidations(w_sheet);
		}
	}

	private void createRows(Sheet w_sheet, murSheet d_sheet) throws Exception {
		for (murRow d_row : d_sheet.rows) {
			Row w_row = w_sheet.createRow(Integer.valueOf(d_row.getName()) - 1);
			/*
			 * if(!d_row.getHeight().isEmpty())
			 * w_row.setHeight(Short.valueOf(d_row.getHeight()));
			 */
			createCells(w_row, d_row, w_sheet);
		}
	}

	private void createCells(Row w_row, murRow d_row, Sheet w_sheet) throws Exception {
		for (murCell d_cell : d_row.cells) {
			CellType ctype = parseHelperXml.parseCellType(d_cell.getDatatype().toUpperCase(), d_cell.getFormula().isEmpty());
			/*    */
			Integer cellIndex;
			if (d_cell.getNumber()!=-1) {
				cellIndex = Integer.valueOf(d_cell.getNumber()) - 1;
			} else {
				cellIndex = CellReference.convertColStringToIndex(d_cell.getName());
			}
			Cell w_cell = w_row.createCell(cellIndex, ctype);
			// optional
			if (d_cell.getWidth()!=-1)
				w_sheet.setColumnWidth(w_cell.getColumnIndex(), d_cell.getWidth());
			fillCell(w_cell, d_cell);
		}
	}

	private void fillCell(Cell w_cell, murCell d_cell) throws Exception {
		if (!d_cell.getFormula().trim().isEmpty()) {
			// некоторые формулы не поддерживаются старым экселем
			try {
				w_cell.setCellFormula(d_cell.getFormula());
			} catch (Exception ex) {
				e_data.printMessage(true,ex.getMessage());
			}
		}
		int style_id;
		CellStyle cw_style;
		if (d_cell.getStyle()!=-1) {
			style_id = d_cell.getStyle();
			cw_style = e_data.getM_doc().getMapst().get(style_id);
			e_data.printMessage(false,cw_style.getFillForegroundColor() + "");
			w_cell.setCellStyle(cw_style);
			fillFormat(d_cell, w_cell);
		}
		if(!d_cell.getValue().trim().isEmpty()) {
		switch (d_cell.getDatatype().trim().toUpperCase()) {
		case "STRING":
			w_cell.setCellValue(d_cell.getValue());
			break;
		case "NUMERIC":
			w_cell.setCellValue(Double.valueOf(d_cell.getValue().replaceAll(",", "\\.")));
			break;
		case "DATE":
			try {
				w_cell.setCellValue(e_data.getM_df().parse(d_cell.getValue()));
			} catch (ParseException e) {
				e_data.printMessage(true,e.getMessage());
			}
			break;
		case "ERROR":
			w_cell.setCellErrorValue(Byte.valueOf(d_cell.getValue()));
			break;
		default:
			throw new Exception("unreq cell Datatype"+d_cell.getName());
		}
		}
	}

	private void createValidations(Sheet sheet) {
	//	DataValidationHelper dvh = sheet.getDataValidationHelper();
	}

	private Workbook createBook() throws Exception {
		/* создается новая книга, с числом листов и названием и автором */
		Workbook w_wb = new XSSFWorkbook();
		e_data.getM_doc().setHelper(w_wb);
//		fillDocMeta(w_wb);
		createStyles(w_wb);
		createSheets(w_wb);
		return w_wb;
	}
	/*
	private void fillDocMeta(Workbook w_wb) throws ParseException {
		
		 * POIXMLProperties xmlProps = ((XSSFWorkbook) w_wb).getProperties();
		 * CoreProperties coreProps = xmlProps.getCoreProperties();
		 * coreProps.setCreator(e_data.getM_doc().getD_author()); DateFormat
		 * cdate_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
		 * coreProps.setCreated(cdate_format.format(e_data.getM_df().parse(e_data.
		 * getM_doc().getD_date())));
		
	}
 */
	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setOutParameter(String outParameter) {
		// TODO Auto-generated method stub
		
	}
}
