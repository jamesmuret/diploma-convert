package ru.maxidom.convcore.tab.writers;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map.Entry;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murCellFormat;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.excel.murStyle;
import ru.maxidom.convcore.filedata.ExcelData;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.DocTags;
import ru.maxidom.convcore.parse.iWrite;

public class XmlWriter implements iWrite {
	ExcelData e_data;
	XMLStreamWriter writer;
	@Override
	public void writeData() throws Exception {
		writeToXml(e_data.getOtFile());
	}
	@Override
	public void setFileData(FileDataBase m_data) throws Exception {
		e_data =(ExcelData)m_data;
		
	}
	private void writeMeta(XMLStreamWriter writer, String newLine) throws XMLStreamException {
		String endocing = "UTF-8";
		String version = "1.0";
		writer.writeStartDocument(endocing, version);
		
		writer.writeStartElement(DocTags.DATA);
		writer.writeAttribute(DocTags.AUTHOR, e_data.getM_doc().getD_author());
		writer.writeAttribute(DocTags.DATE, e_data.getM_doc().getD_date());
	}

	private void writeStyles(XMLStreamWriter writer, String newLine) throws XMLStreamException {
		for (Entry<Integer, murStyle> w_style : e_data.getM_doc().getD_book().styles.entrySet()) {

			writer.writeStartElement(DocTags.STYLE);
			writer.writeAttribute(DocTags.NAME, String.valueOf(w_style.getKey()));
			/* элемент шрифт */
			writer.writeStartElement(DocTags.FONT);
			writer.writeAttribute(DocTags.NAME, w_style.getValue().getFont().get().getName());
			writer.writeAttribute(DocTags.SIZE, w_style.getValue().getFont().get().getSize());
			writer.writeAttribute(DocTags.ITALIC, w_style.getValue().getFont().get().getItalic() == true ? "1" : "0");
			writer.writeAttribute(DocTags.BOLD, w_style.getValue().getFont().get().getBold() == true ? "1" : "0");
			writer.writeAttribute(DocTags.STRIKE, w_style.getValue().getFont().get().getStrike() == true ? "1" : "0");
			writer.writeAttribute(DocTags.UNDERLINED,
					w_style.getValue().getFont().get().getUnderlined() == true ? "1" : "0");
			writer.writeEndElement();
			/* элемент граница */
			writer.writeStartElement(DocTags.BORDER);
			writer.writeAttribute(DocTags.LEFT, w_style.getValue().getLeft());
			writer.writeAttribute(DocTags.TOP, w_style.getValue().getTop());
			writer.writeAttribute(DocTags.RIGHT, w_style.getValue().getRight());
			writer.writeAttribute(DocTags.BOTTOM, w_style.getValue().getBottom());
			writer.writeEndElement();
			/* элемент выравнивание */
			writer.writeStartElement(DocTags.ALIGN);
			writer.writeAttribute(DocTags.VERTICAL, w_style.getValue().getFont().get().getVertical());
			writer.writeAttribute(DocTags.HORIZONTAL, w_style.getValue().getFont().get().getHorizontal());
			writer.writeEndElement();
			/* шрифт */
			/* элемент цвет */
			writer.writeStartElement(DocTags.COLOR);
			writer.writeAttribute(DocTags.FONT, w_style.getValue().getFontColor());
			writer.writeAttribute(DocTags.FRONT, w_style.getValue().getFrontColor());
			writer.writeAttribute(DocTags.BACK, w_style.getValue().getBackColor());
			writer.writeCharacters(w_style.getValue().getPattern());
			writer.writeEndElement();
			/* /цвет */
			/* /стиль */
			writer.writeEndElement();
		}
	}

	private void writeFormats(XMLStreamWriter writer, String newLine) throws XMLStreamException {
		writer.writeStartElement(DocTags.FORMATS);
		/* форматы ячеек */
		for (Entry<Integer, murCellFormat> w_format : e_data.getM_doc().getD_book().formats.entrySet()) {
			writer.writeStartElement(DocTags.CELLFORMAT);
			writer.writeAttribute(DocTags.NAME, String.valueOf(w_format.getKey()));
			writer.writeAttribute(DocTags.TYPE, w_format.getValue().getType());
			writer.writeEndElement();
		}
		writer.writeEndElement();
	}
	private void writeSheets(XMLStreamWriter writer, String newLine) throws XMLStreamException {
		for (murSheet w_sheet : e_data.getM_doc().getD_book().sheets) {
			writer.writeStartElement(DocTags.WORKSHEET);
			writer.writeAttribute(DocTags.NAME, w_sheet.getName());
			writer.writeStartElement(DocTags.ROWS);
			for (murRow row : w_sheet.rows) {
				writer.writeStartElement(DocTags.ROW);
				writer.writeAttribute(DocTags.NUMBER, String.valueOf(row.getName()));
				writer.writeAttribute(DocTags.HEIGHT, String.valueOf(row.getHeight()));
				for (murCell w_cell : row.cells) {
					writer.writeStartElement(DocTags.CELL);
					writer.writeAttribute(DocTags.NAME, w_cell.getName());
					if(w_cell.getNumber()!=-1)
					writer.writeAttribute(DocTags.NUMBER, String.valueOf(w_cell.getNumber()));
					writer.writeAttribute(DocTags.DATATYPE, String.valueOf(w_cell.getDatatype()));
					writer.writeAttribute(DocTags.FORMAT, String.valueOf(w_cell.getFormat()));
					writer.writeAttribute(DocTags.STYLE, String.valueOf(w_cell.getStyle()));
					if(w_cell.getValidation()!=-1)
					writer.writeAttribute(DocTags.VALIDATION, String.valueOf(w_cell.getValidation()));
					if(w_cell.getWidth()!=-1)
					writer.writeAttribute(DocTags.WIDTH, String.valueOf(w_cell.getWidth()));
					writer.writeStartElement(DocTags.VALUE);
					writer.writeCharacters(w_cell.getValue());
					writer.writeEndElement();
					writer.writeStartElement(DocTags.FORMULA);
					writer.writeCharacters(w_cell.getFormula());
					writer.writeEndElement();
					writer.writeEndElement();
				}
				writer.writeEndElement();
			}
			writer.writeEndElement();
		}
	}
	private void writeToXml(String filename) throws FileNotFoundException, XMLStreamException {
		/* Sax фабрика создается по факту */
		XMLOutputFactory saxFac = XMLOutputFactory.newFactory();
		writer = saxFac.createXMLStreamWriter(new FileOutputStream(filename));
		String newLine = "\n";
		/*
		 * шапка
		 */
		writeMeta(writer, newLine);
		/*
		 * стили
		 */
		writeStyles(writer, newLine);
		writer.writeEndElement();
		writeFormats(writer, newLine);
		/*
		 * листы
		 */
		writer.writeStartElement(DocTags.WORKSHEETS);
		writeSheets(writer, newLine);
		writer.writeEndElement();
		/* root element -data */
		writer.writeEndElement();
		writer.writeEndDocument();
		writer.flush();
		writer.close();
	}
	@Override
	public void close() throws Exception {
		if(writer!=null)
			writer.close();
	}
	@Override
	public void setOutParameter(String outParameter) {
		// TODO Auto-generated method stub
		
	}
}
