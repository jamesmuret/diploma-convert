package ru.maxidom.convcore.tab.writers;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import ru.maxidom.convcore.filedata.TestExcelData;
import ru.maxidom.convcore.filedata.test.TestAnswer;
import ru.maxidom.convcore.filedata.test.TestQuestion;
import ru.maxidom.convcore.filedata.test.TestTopic;
import ru.maxidom.convcore.parse.DocTags;

public class SubTestWriterGeneral implements iSubWrite {

	TestExcelData testData;
	String filename;
	XMLStreamWriter writer;
	public SubTestWriterGeneral(String filename, int shop, TestExcelData testData) {
		this.filename = filename;
		this.testData = testData;
	}

	private void writeTopics() throws Exception {
		writer.writeStartElement(DocTags.THEMES);
		writer.writeStartElement(DocTags.Quantity);
		long topicCount = testData.topics.size();
		writer.writeCharacters(String.valueOf(topicCount));
		writer.writeEndElement();
		for (Entry<Integer, TestTopic> topic : testData.topics.entrySet()) {
			int topicId = topic.getKey();
			writer.writeStartElement(DocTags.THEME + topicId);
			writer.writeStartElement(DocTags.Caption);
			writer.writeCharacters(topic.getValue().getText().replace(testData.getCanvasParams().getTopicMark(), "").trim());
			writer.writeEndElement();
			writer.writeStartElement(DocTags.NumExam);
			writer.writeCharacters(String.valueOf(testData.questions.stream().filter(s -> s.getIdTopic() == topic.getKey()).distinct().count()));
			writer.writeEndElement();
			writer.writeStartElement(DocTags.ShowInResults);
			writer.writeCharacters(DocTags.ShowInResultsBody);
			writer.writeEndElement();
			writer.writeEndElement();
		}
		writer.writeEndElement();
	}

	private void writeQuestions() throws Exception {
		int answersCount = 0;
		boolean multi;
		writer.writeStartElement(DocTags.QUESTIONS);
		for (TestQuestion question : testData.questions) {
			multi = checkMulti(question.getIdQuest());
			writer.writeStartElement(DocTags.QUESTION + question.getIdQuest());
			writer.writeStartElement(DocTags.Weight);
			writer.writeCharacters(String.valueOf(question.getWeight()));
			writer.writeEndElement();
			writer.writeStartElement(DocTags.ThemeID);
			writer.writeCharacters(String.valueOf(question.getIdTopic()));
			writer.writeEndElement();
			writer.writeStartElement(DocTags.Type);
			writer.writeCharacters(String.valueOf(multi?1:0));
			writer.writeEndElement();
			writer.writeStartElement(DocTags.TEXT_HTML);
			String questText = testData.getCanvasParams().getRadioStyle().replace(testData.getCanvasParams().getReplacePattern(), question.getText());
			writer.writeCharacters(questText);
			writer.writeEndElement();
			if (!multi)
				answersCount = writeAnswers(question.getIdQuest());
			else
				answersCount = writeAnswersMulti(question.getIdQuest());
			writer.writeStartElement(DocTags.AQuantity);
			writer.writeCharacters(String.valueOf(answersCount));
			writer.writeEndElement();

			writer.writeEndElement();
		}

		writer.writeEndElement();
	}

	private boolean checkMulti(int questionId) {
		boolean res = false;
		int resInt = testData.answers.stream().filter(s -> s.getIdQuest() == questionId)
				.mapToInt(s -> s.isTrue() ? 1 : 0).reduce((x, y) -> x + y).getAsInt();
		if (resInt > 1)
			res = true;
		return res;
	}

	private int writeAnswersMulti(int questionId) throws Exception {

		writer.writeStartElement(DocTags.ANSWERS);
		List<TestAnswer> answers = testData.answers.stream().filter(s -> s.getIdQuest() == questionId).collect(Collectors.toList());
		int j = 0;
		for (TestAnswer answer : answers) {
			writer.writeStartElement(DocTags.ANSWER_ + answer.getIdAnsw());
			writer.writeStartElement(DocTags.TEXT);
			writer.writeCharacters(answer.getText());
			writer.writeEndElement();
			writer.writeStartElement(DocTags.TEXT_HTML);
			String answText = testData.getCanvasParams().getFlagStyle()
					.replace(testData.getCanvasParams().getReplacePattern(), answer.getText());
			writer.writeCharacters(answText);
			writer.writeEndElement();
			writer.writeStartElement(DocTags.SCORE);
			writer.writeCharacters(String.valueOf(answer.getScore()).toUpperCase());
			writer.writeEndElement();
			
			writer.writeStartElement(DocTags.ISRIGHT);
			writer.writeCharacters(String.valueOf(answer.isTrue()).toUpperCase());
			writer.writeEndElement();
			writer.writeEndElement();
			j++;
		}
		writer.writeEndElement();
		return j;
	}

	private int writeAnswers(int questionId) throws Exception {

		writer.writeStartElement(DocTags.ANSWERS);
		List<TestAnswer> answers = testData.answers.stream().filter(s -> s.getIdQuest() == questionId).collect(Collectors.toList());
		int j = 0;
		for (TestAnswer answer : answers) {
			writer.writeStartElement(DocTags.ANSWER_ + answer.getIdAnsw());
			writer.writeStartElement(DocTags.TEXT_HTML);
			String answText = testData.getCanvasParams().getRadioStyle()
					.replace(testData.getCanvasParams().getReplacePattern(), answer.getText());
			writer.writeCharacters(answText);
			writer.writeEndElement();
			writer.writeStartElement(DocTags.ISRIGHT);
			writer.writeCharacters(String.valueOf(answer.isTrue()).toUpperCase());
			writer.writeEndElement();
			writer.writeEndElement();
			j++;
		}
		writer.writeEndElement();
		return j;
	}

	private void writeTest(long questCount) throws Exception {
		writer.writeStartElement(DocTags.TEST);
		writer.writeStartElement(DocTags.INFO);
		writer.writeStartElement(DocTags.Name);
		writer.writeCharacters(testData.test.getTestName().replace(testData.getCanvasParams().getTestMark(), "").trim());
		writer.writeEndElement();

		
		writer.writeStartElement(DocTags.Description);
		writer.writeCharacters(testData.test.getDescription());
		writer.writeEndElement();
		
		
		writer.writeStartElement(DocTags.QuestionsCount);
		writer.writeCharacters(String.valueOf(questCount));
		writer.writeEndElement();

		writer.writeStartElement(DocTags.TestType);
		writer.writeCharacters(String.valueOf(testData.test.getTestType()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.IsExamMode);
		writer.writeCharacters(String.valueOf(testData.test.isExam()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.IsTimeLimit);
		writer.writeCharacters(String.valueOf(testData.test.isTimeLimit()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.IsBack);
		writer.writeCharacters(String.valueOf(testData.test.isBack()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.IsShowScore);
		writer.writeCharacters(String.valueOf(testData.test.isShowScore()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.IsShowResultsMessage);
		writer.writeCharacters(String.valueOf(testData.test.isShowResMes()).toUpperCase());
		writer.writeEndElement();

		writer.writeStartElement(DocTags.TimeLimit);
		writer.writeCharacters(String.valueOf(testData.test.getTimeLimit()));
		writer.writeEndElement();
		/* это относится к Doc.INFO */
		writer.writeEndElement();

	}

	public void writeShopFile() throws Exception {
		
		XMLOutputFactory saxFac = XMLOutputFactory.newFactory();
		try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filename))) {
			writer = saxFac.createXMLStreamWriter(bos, DocTags.ENCODE);
			String version = "1.0";
			int questionCount = testData.questions.size();
			writer.writeStartDocument(DocTags.ENCODE, version);
			writeTest(questionCount);
			writeTopics();
			writeQuestions();
			writer.writeEndElement();
			writer.writeEndDocument();
			writer.flush();
			writer.close();
		}
	}

	@Override
	public String getWrittenFile() {

		return filename;
	}
}
