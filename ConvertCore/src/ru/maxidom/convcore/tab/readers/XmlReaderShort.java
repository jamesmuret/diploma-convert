package ru.maxidom.convcore.tab.readers;

import java.io.File;
import java.util.Optional;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murColumn;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.filedata.ExcelData;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.DocTags;
import ru.maxidom.convcore.parse.iRead;
import ru.maxidom.convcore.parse.parseHelperXml;

public class XmlReaderShort implements iRead {
	ExcelData e_data;
	@Override
	public void readData() throws Exception {
		readFromXml(e_data.getInFile());
	}
	public void readFromXml(String filename) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		builder = factory.newDocumentBuilder();
		Document x_document = null;
		try {
		x_document = builder.parse(new File(filename));
		}
		catch(SAXParseException ex) {
			Exception e = new Exception("XML read validation fail see log");
			ex.setStackTrace(ex.getStackTrace());
			throw e; 
		}
		parseXmldocument(x_document);
	}

	private void parseXmlcols(NodeList n_cols,murSheet p_sheet) throws Exception {
		for(int i=0; i<n_cols.getLength()-1;i++) {
			murColumn p_column = parseHelperXml.readColumn(n_cols.item(i));
			if(p_column.getDatatype().trim().isEmpty()) throw new Exception("Empty format value");
			e_data.printMessage(false,p_column.toString());
			p_sheet.cols.add(p_column);
		}
		
	}
	
	private void parseXmldocument(Document x_document) throws Exception {
		Node root = x_document.getDocumentElement();
		NamedNodeMap docHead = root.getAttributes();
		Optional<Node> n_docAutor = Optional.ofNullable(docHead.getNamedItem(DocTags.AUTHOR));
//		Optional<Node> n_filename = Optional.ofNullable(docHead.getNamedItem(DocTags.DOCNAME));
		Optional<Node> n_docDate = Optional.ofNullable(docHead.getNamedItem(DocTags.DATE));
		NodeList x_sheets = x_document.getElementsByTagName(DocTags.WORKSHEET);
//		NodeList x_formats = x_document.getElementsByTagName(DocTags.cellFormat)
		e_data.setM_doc(new ru.maxidom.convcore.filedata.Document(x_sheets.getLength(), n_docAutor.orElse(x_document.createTextNode(DocTags.AUTHOR)).getNodeValue(), n_docDate.orElse(x_document.createTextNode(DocTags.DATE)).getNodeValue()));
//		parseXmlcellFormats(x_formats);
		parseXmlsheets(x_sheets);
	}

	private void parseXmlsheets(NodeList x_sheets) throws Exception {
		murSheet p_sheet = new murSheet(1);
		for (int i = 0; i < x_sheets.getLength(); i++) {
			Node n_sheet = x_sheets.item(i);
			String sheetName =parseHelperXml.readtSingleAttVal(n_sheet, DocTags.NAME);
			if(sheetName==null) {
				throw new Exception("sheetName missing");
			}
			p_sheet.setName(sheetName);
			p_sheet.setNumber(i+1);
			NodeList n_rows = parseHelperXml.readListElements((Element) n_sheet, DocTags.ROW);
			NodeList n_cols = parseHelperXml.readListElements((Element) n_sheet, DocTags.COLUMN);
			parseXmlrows(n_rows, p_sheet);
			parseXmlcols(n_cols,p_sheet);
			e_data.getM_doc().getD_book().sheets.add(p_sheet);
//			parseXmlcols(x_sheets,p_sheet);
		}
	}
	private void parseXmlrowCells(NodeList cells, murRow p_row) throws Exception {
		for (int i = 0; i < cells.getLength(); i++) {
			Node cell = cells.item(i);
			murCell p_cell = parseHelperXml.readCell(cell);
			p_cell.setStyle(-1);
			p_cell.setValidation(-1);
			p_cell.setFormat(-1);
			p_cell.setWidth(-1);
			p_row.cells.add(p_cell);
			e_data.printMessage(false,p_cell.toString());
		}
	}
	private void parseXmlrows(NodeList rows, murSheet p_sheet) throws Exception {
		for (int i = 0; i < rows.getLength(); i++) {
			Node n_row = rows.item(i);
			murRow p_row = new murRow(1);
			String rowName =parseHelperXml.readtSingleAttVal(n_row, DocTags.NUMBER);
			if(rowName==null) {throw new Exception("rowNumber att missing"+p_sheet.getName());}
			p_row.setName(Integer.valueOf(rowName));
//			p_row.setHeight(Integer.valueOf(parseHelperXml.readtSingleAttVal(n_row, DocTags.HEIGHT)));
			NodeList n_cells = parseHelperXml.readListElements((Element) n_row, DocTags.CELL);
			parseXmlrowCells(n_cells, p_row);
			p_sheet.rows.add(p_row);
			e_data.printMessage(false,String.valueOf(p_row.getName()));
		}
	}

	@Override
	public void setFileData(FileDataBase m_data) throws Exception {
		e_data = (ExcelData) m_data;
	}
	@Override
	public void close() throws Exception {
	
	}
}
