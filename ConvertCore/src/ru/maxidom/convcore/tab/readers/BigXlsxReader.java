package ru.maxidom.convcore.tab.readers;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.poi.ooxml.util.SAXHelper;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.StylesTable;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.iDirectRead;
import ru.maxidom.convcore.parse.iDirectWrite;

import ru.maxidom.convcore.tab.readers.eventmodel.MyXSSFSheetXMLHandlerShortFast;
import ru.maxidom.convcore.tab.readers.eventmodel.murSheetContentsHandlerShortBig;

public class BigXlsxReader implements iDirectRead {
	FileDataBase e_data;
	iDirectWrite e_writer;

	@Override
	public void setWriter(iDirectWrite m_writer) throws Exception {
		e_writer = m_writer;
	}

	private void parseXMLtable(String filename) throws Exception {
		e_data.printMessage(false, "Start parse bigExcel" + filename);
		try (OPCPackage container = OPCPackage.open(filename)) {
			ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(container);
			XSSFReader xssfReader = new XSSFReader(container);
			StylesTable styles = xssfReader.getStylesTable();
			XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator) xssfReader.getSheetsData();
			int i = 1;
			e_writer.writeData();
			while (iter.hasNext()) {
				try (InputStream stream = iter.next()) {
					e_data.printMessage(false,
							"Get sheet: " + iter.getSheetName() + " : " + styles.getNumDataFormats());
					murSheet sheet = new murSheet();
					sheet.setName(iter.getSheetName());
					sheet.setNumber(i++);
					e_writer.startSheet(sheet);
					sheet = null;
					processSheet(styles, strings, stream);
					e_writer.endSheet();

				}
				e_data.printMessage(false, "End parse big" + filename);
			}
			styles = null;
			xssfReader = null;
			strings = null;
		} 
	}

	private void processSheet(StylesTable styles, ReadOnlySharedStringsTable strings, InputStream sheetInputStream)
			throws IOException, SAXException {
		DataFormatter formatter = new DataFormatter();
		InputSource sheetSource = new InputSource(sheetInputStream);
		try {
			XMLReader sheetParser = SAXHelper.newXMLReader();
			murSheetContentsHandlerShortBig sheetHandler = new murSheetContentsHandlerShortBig();
			sheetHandler.setH_writer(e_writer);
			sheetHandler.setH_data(e_data);
			ContentHandler handler = new MyXSSFSheetXMLHandlerShortFast(styles, strings, sheetHandler, formatter,
					false);
			sheetParser.setContentHandler(handler);
			e_data.printMessage(false, "Start parse sheet");
			sheetParser.parse(sheetSource);
			sheetParser = null;
			sheetHandler = null;
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
		}
	}

	@Override
	public void readData() throws Exception {
		parseXMLtable(e_data.getInFile());
	}

	@Override
	public void setFileData(FileDataBase m_date) throws Exception {
		e_data = m_date;
	}

	@Override
	public void close() throws Exception {
		if (e_writer != null)
			e_writer.close();
	}
}
