package ru.maxidom.convcore.tab.readers;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.function.Consumer;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.ooxml.util.SAXHelper;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.StylesTable;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.filedata.TestExcelData;
import ru.maxidom.convcore.filedata.test.Canvas;
import ru.maxidom.convcore.filedata.test.GeneralNode;
import ru.maxidom.convcore.filedata.test.TestDB;
import ru.maxidom.convcore.filedata.test.TestNode;
import ru.maxidom.convcore.parse.iRead;
import ru.maxidom.convcore.tab.readers.eventmodel.MyXSSFSheetXMLHandlerShort;
import ru.maxidom.convcore.tab.readers.eventmodel.MyXSSFSheetXMLHandlerShort.SheetContentsHandler;
import ru.maxidom.convcore.tab.readers.eventmodel.MyXSSFSheetXMLHandlerTest;
import ru.maxidom.convcore.tab.readers.eventmodel.murSheetContentsHandlerShort;
import ru.maxidom.convcore.tab.readers.eventmodel.murSheetContentsHandlerTest;

public class TestXlsxReader implements iRead {
	TestExcelData testData;
	TestNode tNode;
	GeneralNode gNode;

	@Override
	public void setFileData(FileDataBase m_data) throws Exception {
		testData = (TestExcelData) m_data;
		tNode = new TestNode(testData.getCanvasParams());
		gNode = new GeneralNode(testData.getCanvasParams());
	}

	@Override
	public void close() throws Exception {
	}

	@Override
	public void readData() throws Exception {
		if (!parseXMLtable(testData.getInFile()))
			throw new Exception("template doesn't match");
	}

	private boolean parseXMLtable(String filename) throws IOException, SAXException, OpenXML4JException {
		FileDataBase.printMessage(false, "Start parse bigExcel" + filename);
		boolean res = false;
		try (OPCPackage container = OPCPackage.open(filename)) {
			ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(container);
			XSSFReader xssfReader = new XSSFReader(container);
			StylesTable styles = xssfReader.getStylesTable();
			XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator) xssfReader.getSheetsData();
			// проверяются только листы с телом теста и общими параметрами
			while (iter.hasNext()) {
				try (InputStream stream = iter.next()) {
					FileDataBase.printMessage(false,"Get sheet: " + iter.getSheetName() + " : " + styles.getNumDataFormats());
					if (checkSheet(iter)) {
						processSheet(styles, strings, stream);
						res = true;
					}
				}
			}
			FileDataBase.printMessage(false, "End parse big" + filename);
		}

		return res;
	}

	private boolean checkSheet(XSSFReader.SheetIterator iterator) {
		boolean res = true;
		if (iterator.getSheetName().trim().equalsIgnoreCase(testData.getCanvasParams().getParamSheetName())) {
			testData.setCurrNode(gNode);
		} else if ((iterator.getSheetName().trim().equalsIgnoreCase(testData.getCanvasParams().getTestSheetName()))) {
			testData.setCurrNode(tNode);
		} else
			res = false;
		return res;
	}

	private void processSheet(StylesTable styles, ReadOnlySharedStringsTable strings, InputStream sheetInputStream)
			throws IOException, SAXException {
		DataFormatter formatter = new DataFormatter();
		InputSource sheetSource = new InputSource(sheetInputStream);
		try {
			XMLReader sheetParser = SAXHelper.newXMLReader();
			murSheetContentsHandlerTest sheetHandler = new murSheetContentsHandlerTest();
			sheetHandler.setData(testData);
			ContentHandler handler = new MyXSSFSheetXMLHandlerTest(styles, strings, sheetHandler, formatter, false);
			sheetParser.setContentHandler(handler);
			sheetParser.parse(sheetSource);
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
		}
	}

}
