package ru.maxidom.convcore.tab.readers.eventmodel;

import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFComment;

import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.filedata.TestExcelData;

public class murSheetContentsHandlerTest implements SheetContentsHandler {

	public enum RW_TYPE {
		FIRST_ROW, OTHER_ROW
	}

	private TestExcelData testData;
	private boolean isEmptyRow;
	public void setData(TestExcelData data) {
		testData = data;
	}
	private int currRow;
	private int currCell;
	
	@Override
	public void headerFooter(String text, boolean isHeader, String tagName) {
		FileDataBase.printMessage(false, "Run headerFooter: " + text + " : " + isHeader + " : " + tagName);
	}

	@Override
	public void cell(String cellReference, String formattedValue, XSSFComment comment) {

		FileDataBase.printMessage(false, "Run cell: " + cellReference + " : " + formattedValue + " : " + comment);
	}

	public void cell(String cellReference, String formattedValue, String formula, XSSFCellStyle cellStyle) {
		isEmptyRow = false;
		currCell = CellReference.convertColStringToIndex(cellReference.replaceAll("\\d+", ""));
		FileDataBase.printMessage(false, formattedValue+ " "+currCell);
		try {
			testData.resolveMethod(currCell).ifPresent(s -> s.accept(formattedValue));
		} catch (Exception e) {
			FileDataBase.printMessage(true, "in row:"+currRow+"col:"+currCell + " " + e.getLocalizedMessage());
			throw new RuntimeException("стр:"+currRow+"стл:"+currCell + " " + e.getLocalizedMessage());
		}
	}

	@Override
	public void endRow(int rowNum) {
		FileDataBase.printMessage(false, "Run endRow: " + rowNum);
	}

	public void endRow() {
		try {
			if(!isEmptyRow)
				testData.getCurrNode().dump();
			isEmptyRow = true;
		} catch (Exception e) {
			FileDataBase.printMessage(true, "in row:"+currRow+"col:"+currCell + " " + e.getLocalizedMessage());
			throw new RuntimeException("in row:"+currRow+"col:"+currCell + " " + e.getLocalizedMessage());
		}
	}

	@Override
	public void startRow(int rowNum) {
		currRow = rowNum;
		if (rowNum > 1)
			testData.getCurrNode().setRowType(RW_TYPE.OTHER_ROW);
		FileDataBase.printMessage(false, "Run startRow: " + rowNum);
	}

}
