package ru.maxidom.convcore.tab.readers.eventmodel;


import java.text.SimpleDateFormat;

import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFComment;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murCellFormat;
import ru.maxidom.convcore.excel.murColumn;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.DocTags;
import ru.maxidom.convcore.parse.iDirectWrite;

public class maxiSheetContentsHandler implements SheetContentsHandler {


	public murCellFormat getF_format() {
		return f_format;
	}

	public void setF_format(murCellFormat f_format) {
		this.f_format = f_format;
	}


	public String getF_datatype() {
		return f_datatype;
	}

	public void setF_datatype(String f_datatype) {
		this.f_datatype = f_datatype;
	}

	public String getH_rowHt() {
		return f_rowHt;
	}

	public void setH_rowHt(String h_rowHt) {
		this.f_rowHt = h_rowHt;
	}

	public murSheet getH_sheet() {
		return f_sheet;
	}

	public void setH_sheet(murSheet h_sheet) {
		this.f_sheet = h_sheet;
	}

	private murSheet f_sheet;
	private murRow f_row;
	private murCell f_cell;
	private String f_rowHt;
	private String f_datatype;
	private FileDataBase f_data;
	private murCellFormat f_format;
	private iDirectWrite f_writer;
	private StringBuffer sbval = new StringBuffer();
	private StringBuffer sbcell = new StringBuffer();
	
	public iDirectWrite getH_writer() {
		return f_writer;
	}

	public void setH_writer(iDirectWrite f_writer) {
		this.f_writer = f_writer;
	}

	public void setH_data(FileDataBase h_data) {
		this.f_data = h_data;
	}
	@Override
	public void headerFooter(String text, boolean isHeader, String tagName) {
		f_data.printMessage(false,"Run headerFooter: " + text + " : " + isHeader + " : " + tagName);
	}
	@Override
	public void cell(String cellReference, String formattedValue, XSSFComment comment) {
		f_data.printMessage(false,"Run cell: " + cellReference + " : " + formattedValue + " : " + comment);
	}

	public void startSheet(String name,Integer number) throws Exception {
		// создает непустой список с форматами столбцов
		murSheet sheet = new murSheet();
		f_sheet=sheet;
		sheet.setName(name);
		sheet.setNumber(number);
		f_writer.startSheet(sheet);
	}
	public void endSheet( ) {
			f_sheet.clear();
			f_sheet = null;
			try {
				f_writer.endSheet();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	/*
	public void column(String name,Integer max,Integer min,String format,String width,String type) throws Exception {
		murColumn col = new murColumn();
		if (format.isEmpty()) throw new Exception("Empty col format");
			col.setFormat(Integer.valueOf(format));
		col.setMin(Integer.valueOf(min));
		col.setMax(Integer.valueOf(max));
		col.setDatatype(type);	
		f_writer.column(col,this.getH_sheet());
		col = null;
	}
	*/
	
	public void cell(String cellReference, String formattedValue, String formula, XSSFCellStyle cellStyle) {
		sbval.append(formattedValue.replace(".", ","));
		sbval.append(cellReference.replaceAll("\\d+", ""));
		sbcell.append(cellReference);
		if(f_datatype.equals("d")) {
		SimpleDateFormat dtForm = new SimpleDateFormat(DocTags.DATEPATTERN);
		formattedValue = dtForm.format(DateUtil.getJavaDate(Double.parseDouble(formattedValue)));
		dtForm = null;
		}
		else if(f_datatype.equals("n"))
		{
			formattedValue = sbval.toString().replaceAll("([^\\d-,])|([\\.,](0)+\\z)", "");

		}	
		f_cell = new murCell();
		f_cell.setFormat(-1);
		f_cell.setStyle(-1);
		f_cell.setDatatype(f_datatype);
		f_cell.setValue(formattedValue);
		f_cell.setName(sbcell.toString());
		f_cell.setFormula(formula);
		f_cell.setWidth(-1);
		int number = CellReference.convertColStringToIndex(sbcell.toString()) +1;
		f_cell.setNumber(number);
		f_cell.setValidation(-1);
		sbval.setLength(0);
		sbcell.setLength(0);
		cellReference=null;
		formattedValue=null;
		formula=null;
		try {
			f_writer.startCell(f_cell);
			f_writer.endCell();
		} catch (Exception e) {
		}
		f_cell =null;
	}
	@Override
	public void endRow(int rowNum) {
		try {
			f_writer.endRow();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
			}
		f_data.printMessage(false,"Run endRow: " + rowNum);
	}
	public void endRow() {
		try {
			f_writer.endRow();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
		f_data.printMessage(false,"Run endRow: ");

	}
	@Override
	public void startRow(int rowNum) {
		f_row = new murRow();
		f_row.setName(rowNum);
		f_row.setHeight(-1);
		f_data.printMessage(false,"Run startRow: " + rowNum);
		try {
			f_writer.startRow(f_row);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
		f_row =null;
	}

}
