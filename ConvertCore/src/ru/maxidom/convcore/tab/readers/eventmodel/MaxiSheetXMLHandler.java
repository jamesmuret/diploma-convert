package ru.maxidom.convcore.tab.readers.eventmodel;

import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;




public class MaxiSheetXMLHandler extends DefaultHandler {

	/**
	 * These are the different kinds of cells we support. We keep track of the
	 * current one between the start and end.
	 */
	enum xssfDataType {
	BOOLEAN, ERROR, FORMULA, INLINE_STRING, SST_STRING, NUMBER,
	}

	/**
	 * Table with the styles used for formatting
	 */
	private StylesTable stylesTable;

	private ReadOnlySharedStringsTable sharedStringsTable;

	/**
	 * Where our text is going
	 */
	private final murSheetContentsHandlerShort output;

	// Set when V start element is seen
	private boolean vIsOpen;
	// Set when F start element is seen
	private boolean fIsOpen;
	// Set when an Inline String "is" is seen
	private boolean isIsOpen;
	// Set when a header/footer element is seen
	private boolean hfIsOpen;

	// Set when cell start element is seen;
	// used when cell close element is seen.
	private xssfDataType nextDataType;

	// Used to format numeric cell values.
	private short formatIndex;
	private String formatString;
	private final DataFormatter formatter;
	private String cellRef;
	private boolean formulasNotResults;
	private XSSFCellStyle cellStyle;

	// Gathers characters as they are seen.
	private StringBuffer value = new StringBuffer();
	private StringBuffer formula = new StringBuffer();
	private StringBuffer headerFooter = new StringBuffer();

	/**
	 * Accepts objects needed while parsing.
	 *
	 * @param styles  Table of styles
	 * @param strings Table of shared strings
	 */
	public MaxiSheetXMLHandler(StylesTable styles, ReadOnlySharedStringsTable strings,
			murSheetContentsHandlerShort sheetContentsHandler, DataFormatter dataFormatter, boolean formulasNotResults) {
		this.stylesTable = styles;
		this.sharedStringsTable = strings;
		this.output = sheetContentsHandler;
		this.formulasNotResults = formulasNotResults;
		this.nextDataType = xssfDataType.NUMBER;
		this.formatter = dataFormatter;
	}

	/**
	 * Accepts objects needed while parsing.
	 *
	 * @param styles  Table of styles
	 * @param strings Table of shared strings
	 */
	public MaxiSheetXMLHandler(StylesTable styles, ReadOnlySharedStringsTable strings,
			murSheetContentsHandlerShort sheetContentsHandler, boolean formulasNotResults) {
		this(styles, strings, sheetContentsHandler, new DataFormatter(), formulasNotResults);
	}

	private boolean isTextTag(String name) {
		if ("v".equals(name)) {
			// Easy, normal v text tag
			return true;
		}
		if ("inlineStr".equals(name)) {
			// Easy inline string
			return true;
		}
		if ("t".equals(name) && isIsOpen) {
			// Inline string <is><t>...</t></is> pair
			return true;
		}
		// It isn't a text tag
		return false;
	}

	public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {

		if (isTextTag(name)) {
			vIsOpen = true;
			// Clear contents cache
			value.setLength(0);
		} else if ("is".equals(name)) {
			// Inline string outer tag
			isIsOpen = true;
		} else if ("f".equals(name)) {
			// Clear contents cache
			formula.setLength(0);

			// Mark us as being a formula if not already
			if (nextDataType == xssfDataType.NUMBER) {
				nextDataType = xssfDataType.FORMULA;
			}
			String type = attributes.getValue("t");
			if (type != null && type.equals("shared")) {
				String ref = attributes.getValue("ref");
				if (ref != null) {
					fIsOpen = true;
				} else {
					if (formulasNotResults) {
						System.err.println("Warning - shared formulas not yet supported!");
					} else {
					}
				}
			} else {
				fIsOpen = true;
			}
		}
		else if ("row".equals(name)) {
			int rowNum = Integer.parseInt(attributes.getValue("r"));
			output.startRow(rowNum);
		}
		else if ("c".equals(name)) {
			// Set up defaults.
			this.nextDataType = xssfDataType.NUMBER;
			this.formatIndex = -1;
			this.formatString = null;
			this.cellStyle = null;
			cellRef = attributes.getValue("r");
			// по дефолту написано, что дефолтное number
			String cellType = attributes.getValue("t");
			String cellStyleStr = attributes.getValue("s");
			int styleIndex = 0;
			if (cellType == null) {
				nextDataType = xssfDataType.NUMBER;
			}
			if (cellStyleStr != null) {
				styleIndex = Integer.parseInt(cellStyleStr);
			} else {
				styleIndex = 0;
			}
			this.cellStyle = stylesTable.getStyleAt(styleIndex);
			this.formatIndex = this.cellStyle.getDataFormat();
			this.formatString = this.cellStyle.getDataFormatString();
			if (this.formatString == null) {
				this.formatString = BuiltinFormats.getBuiltinFormat(this.formatIndex);
				this.formatIndex = -1;
			}
			if ("b".equals(cellType))
				nextDataType = xssfDataType.BOOLEAN;
			else if ("e".equals(cellType))
				nextDataType = xssfDataType.ERROR;
			else if ("inlineStr".equals(cellType))
				nextDataType = xssfDataType.INLINE_STRING;
			else if ("s".equals(cellType))
				nextDataType = xssfDataType.SST_STRING;
			else if ("str".equals(cellType))
				nextDataType = xssfDataType.FORMULA;
			else if (cellStyleStr != null) {
				this.formatIndex = this.cellStyle.getDataFormat();
				this.formatString = this.cellStyle.getDataFormatString();

				if (this.formatString == null) {
					this.formatString = BuiltinFormats.getBuiltinFormat(this.formatIndex);
					this.formatIndex = -1;
				}
			}
		}
	}

	public void endElement(String uri, String localName, String name) throws SAXException {
		String thisStr = null;
		// v => contents of a cell данные в ячейке
		if (isTextTag(name)) {
			vIsOpen = false;
			// Process the value contents as required, now we have it all
			switch (nextDataType) {
			case BOOLEAN:
				char first = value.charAt(0);
				thisStr = first == '0' ? "FALSE" : "TRUE";
				output.setF_datatype("b");
				break;
			case ERROR:
				thisStr = "ERROR:" + value.toString();
				output.setF_datatype("e");
				break;
			case FORMULA:
				if (formulasNotResults) {
					thisStr = formula.toString();
					output.setF_datatype("s");
				} else {
					String fv = value.toString();
					if (this.formatString != null) {
						try {
							// Try to use the value as a formattable number
							double d = Double.parseDouble(fv);
							if (DateUtil.isADateFormat(this.formatIndex, this.formatString)) {
								output.setF_datatype("d");
								thisStr = String.valueOf(d);
							} else {
								System.out.println(this.formatString);
								thisStr = formatter.formatRawCellContents(d, this.formatIndex, this.formatString);
								output.setF_datatype("n");
							}
						} catch (NumberFormatException e) {
							// Formula is a String result not a Numeric one
							thisStr = fv;
						}
					} else {
						// No formating applied, just do raw value in all cases
						thisStr = fv;
					}
				}
				break;
			case INLINE_STRING:
				// TODO: Can these ever have formatting on them?
				XSSFRichTextString rtsi = new XSSFRichTextString(value.toString());
				thisStr = rtsi.toString();
				if(thisStr.matches("^,\\d+")) {
					thisStr = "0"+thisStr;
					output.setF_datatype("n");
					break;
				}
				try {
					thisStr =  String.valueOf(Integer.parseInt(thisStr));
				output.setF_datatype("n");
				break;
				}
				catch(Exception ex) {
					try {
					thisStr = String.valueOf(Double.parseDouble(thisStr));
					output.setF_datatype("n");	
					}
					catch(Exception exx) {
						output.setF_datatype("s");	
					}
				}
				break;
			case SST_STRING:
				String sstIndex = value.toString();
				try {
					int idx = Integer.parseInt(sstIndex);
					XSSFRichTextString rtss = new XSSFRichTextString(sharedStringsTable.getEntryAt(idx));
					thisStr = rtss.toString();
					if(thisStr.matches("^,\\d+")) {
						thisStr = "0"+thisStr;
						output.setF_datatype("n");
						break;
					}
					try {
						thisStr =  String.valueOf(Integer.parseInt(thisStr));
					output.setF_datatype("n");
					break;
					}
					catch(Exception ex) {
						try {
						thisStr = String.valueOf(Double.parseDouble(thisStr));
						output.setF_datatype("n");	
						}
						catch(Exception exx) {
							output.setF_datatype("s");	
						}
					}
					
				} catch (NumberFormatException ex) {
					throw new SAXException("Failed to parse SST index '" + sstIndex + "': " + ex.toString());
				//	System.err.println("Failed to parse SST index '" + sstIndex + "': " + ex.toString());
				}
				break;
			case NUMBER:
				String n = value.toString();
				// Try to use the value as a formattable number
				if (DateUtil.isADateFormat(formatIndex, formatString)) {
					thisStr = n;
					output.setF_datatype("d");
					break;
				}
				if (this.formatString != null & !this.formatString.equals("General"))
					thisStr = formatter.formatRawCellContents(Double.parseDouble(n), this.formatIndex,
							this.formatString);
				else
					thisStr = n;
				output.setF_datatype("n");
				break;
			default:
				thisStr = "(TODO: Unexpected type: " + nextDataType + ")";
				break;
			}
			thisStr = thisStr.trim();
			if(thisStr.isEmpty()) {
				return;
			}
			output.cell(cellRef,thisStr, formula.toString(), this.cellStyle);
			formula.setLength(0);
		} else if ("f".equals(name)) {
			fIsOpen = false;
			// formula.setLength(0);
		} else if ("is".equals(name)) {
			isIsOpen = false;
		} else if ("row".equals(name)) {
			output.endRow();
		} else if ("oddHeader".equals(name) || "evenHeader".equals(name) || "firstHeader".equals(name)) {
			hfIsOpen = false;
			output.headerFooter(headerFooter.toString(), true, name);
		} else if ("oddFooter".equals(name) || "evenFooter".equals(name) || "firstFooter".equals(name)) {
			hfIsOpen = false;
			output.headerFooter(headerFooter.toString(), false, name);
		}
	}

	/**
	 * Captures characters only if a suitable element is open. Originally was just
	 * "v"; extended for inlineStr also.
	 */
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (vIsOpen) {
			value.append(ch, start, length);
		}
		if (fIsOpen) {
			formula.append(ch, start, length);
		}
		if (hfIsOpen) {
			headerFooter.append(ch, start, length);
		}
	}

	/**
	 * You need to implement this to handle the results of the sheet parsing.
	 */
	public interface SheetContentsHandler {
		/** A row with the (zero based) row number has started */
		public void startRow(int rowNum);

		/** A row with the (zero based) row number has ended */
		public void endRow();

		/** A cell, with the given formatted value, was encountered */
		public void cell(String cellReference, String formattedValue);

		/** A header or footer has been encountered */
		public void headerFooter(String text, boolean isHeader, String tagName);
	}
}
