package ru.maxidom.convcore.tab.readers.eventmodel;

import java.util.Optional;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.apache.poi.xssf.usermodel.XSSFFont;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murCellFormat;
import ru.maxidom.convcore.excel.murFont;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.excel.murStyle;
import ru.maxidom.convcore.filedata.ExcelData;

public class murSheetContentsHandler implements SheetContentsHandler {


	public murCellFormat getF_format() {
		return f_format;
	}

	public void setF_format(murCellFormat f_format) {
		this.f_format = f_format;
	}

	public int getStyle_id() {
		return style_id;
	}

	public void setStyle_id(int style_id) {
		this.style_id = style_id;
	}

	public String getF_datatype() {
		return f_datatype;
	}

	public void setF_datatype(String f_datatype) {
		this.f_datatype = f_datatype;
	}

	public Integer getH_rowHt() {
		return f_rowHt;
	}

	public void setH_rowHt(Integer h_rowHt) {
		this.f_rowHt = h_rowHt;
	}

	public murSheet getH_sheet() {
		return f_sheet;
	}

	public void setH_sheet(murSheet h_sheet) {
		this.f_sheet = h_sheet;
	}

	private murSheet f_sheet;
	private murRow f_row;
	private murCell f_cell;
	private Integer f_rowHt;
	private String f_datatype;
	private ExcelData f_data;
	private murCellFormat f_format;
	private int style_id;
	
	public void setH_data(ExcelData h_data) {
		this.f_data = h_data;
	}

	@Override
	public void headerFooter(String text, boolean isHeader, String tagName) {
	//	f_data.logConsole("Run headerFooter: " + text + " : " + isHeader + " : " + tagName);
	}
	@Override
	public void cell(String cellReference, String formattedValue, XSSFComment comment) {
		// TODO Auto-generated method stub
  //	f_data.logConsole("Run cell: " + cellReference + " : " + formattedValue + " : " + comment);
	}

	public void cell(String cellReference, String formattedValue, String formula, XSSFCellStyle cellStyle) {
		if(f_datatype.equals("DATE")) {
		formattedValue = f_data.getM_df().format(DateUtil.getJavaDate(Double.parseDouble(formattedValue)));
		}
		else if(f_datatype.equals("NUMERIC"))
		{
			formattedValue = formattedValue.replaceAll("[^\\d-,]", "").replaceAll("\\.", ".");
		}	
		if(!f_data.getM_doc().getD_book().formats.containsKey(f_format.getName())) {
			f_data.getM_doc().getD_book().formats.put(this.f_format.getName(), f_format);
		}
		f_cell = new murCell();
		f_cell.setFormat(f_format.getName());
		f_cell.setDatatype(f_datatype);
		f_cell.setValue(formattedValue);
		f_cell.setName(cellReference.replaceAll("\\d+", ""));
		f_cell.setFormula(formula);
		f_cell.setWidth(-1);
		int index =CellReference.convertColStringToIndex(cellReference.replaceAll("\\d+", ""));
		f_cell.setNumber(index);
		f_cell.setValidation(-1);
		/* и нужно добавить стиль, если только стиль еще не найден */
		if (cellStyle != null) {
			/* обработка данных о клетке находится тут */
			/* пока без цветов и стилей, только значения и формулы */
			murStyle f_style = new murStyle();
			murFont f_font = new murFont();
			f_cell.setStyle(getStyle_id());
			// стиль для ячейки
			XSSFFont tmp_font = cellStyle.getFont();
			f_font.setBold(tmp_font.getBold());
			f_font.setVertical(cellStyle.getVerticalAlignmentEnum().toString());
			f_font.setItalic(tmp_font.getItalic());
			f_font.setName(tmp_font.getFontName());
			f_font.setSize(String.valueOf(tmp_font.getFontHeightInPoints()));
			f_font.setStrike(tmp_font.getStrikeout());
			f_font.setUnderlined(false);
			f_font.setHorizontal(cellStyle.getAlignmentEnum().toString());
			f_style.setFont(Optional.of(f_font));
			// заглушки лол
			f_style.setFontColor("0:0:0");
			f_style.setBackColor("0:0:0");
			f_style.setFrontColor("0:0:0");
			f_style.setPattern(cellStyle.getFillPatternEnum().toString());
			/* установка стиля */
//			f_style.setBackColor();
//			f_style.setFrontColor(frontColor);
//			расписать 
			f_style.setName(style_id);
			f_style.setLeft(cellStyle.getBorderLeftEnum().toString());
			f_style.setRight(cellStyle.getBorderRightEnum().toString());
			f_style.setTop(cellStyle.getBorderTopEnum().toString());
			f_style.setBottom(cellStyle.getBorderBottomEnum().toString());
			// getColorPattern( (XSSFColor) cellStyle.getFillForegroundColorColor() );
			// getColorPattern( (XSSFColor) cellStyle.getFillBackgroundColorColor() );
			// getColorPattern( (XSSFColor) cellStyle.getFont().getXSSFColor() );
			// добавление стиля ячейки
			if(!f_data.getM_doc().getD_book().styles.containsKey(f_style.getName())) {
				f_data.getM_doc().getD_book().styles.put(f_style.getName(), f_style);
			}
			f_data.printMessage(false,("\tFont name: " + cellStyle.getFont().getFontName()));
		}
		/*
		 * System.out.println("Run cell: " + cellReference + " : " + formattedValue +
		 * " : " + formula); if (cellStyle != null) { // getColorPattern( (XSSFColor)
		 * cellStyle.getFillForegroundColorColor() ); // getColorPattern( (XSSFColor)
		 * cellStyle.getFillBackgroundColorColor() ); // getColorPattern( (XSSFColor)
		 * cellStyle.getFont().getXSSFColor() ); System.out.println("\tFont name: " +
		 * cellStyle.getFont().getFontName()); }
		 */
		f_row.cells.add(f_cell);
	}

	@Override
	public void endRow(int rowNum) {
		// TODO Auto-generated method stub
		f_data.printMessage(false,"Run endRow: " + rowNum);
	}

	public void endRow() {
		/* вот тут добавляется строка в листе */
		f_sheet.rows.add(f_row);
		// TODO Auto-generated method stub
		f_data.printMessage(false,"Run endRow: ");

	}

	@Override
	public void startRow(int rowNum) {
		f_row = new murRow();
		f_row.setName(rowNum);
		f_row.setHeight(f_rowHt);
		f_data.printMessage(false,"Run startRow: " + rowNum);
	}

}
