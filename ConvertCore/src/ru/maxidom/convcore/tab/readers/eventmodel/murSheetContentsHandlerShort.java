package ru.maxidom.convcore.tab.readers.eventmodel;


import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFComment;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murCellFormat;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.filedata.ExcelData;

public class murSheetContentsHandlerShort implements SheetContentsHandler {


	public murCellFormat getF_format() {
		return f_format;
	}

	public void setF_format(murCellFormat f_format) {
		this.f_format = f_format;
	}


	public String getF_datatype() {
		return f_datatype;
	}

	public void setF_datatype(String f_datatype) {
		this.f_datatype = f_datatype;
	}

	public String getH_rowHt() {
		return f_rowHt;
	}

	public void setH_rowHt(String h_rowHt) {
		this.f_rowHt = h_rowHt;
	}

	public murSheet getH_sheet() {
		return f_sheet;
	}

	public void setH_sheet(murSheet h_sheet) {
		this.f_sheet = h_sheet;
	}

	private murSheet f_sheet;
	private murRow f_row;
	private murCell f_cell;
	private String f_rowHt;
	private String f_datatype;
	private ExcelData f_data;
	private murCellFormat f_format;
	
	public void setH_data(ExcelData h_data) {
		this.f_data = h_data;
	}

	@Override
	public void headerFooter(String text, boolean isHeader, String tagName) {
		f_data.printMessage(false,"Run headerFooter: " + text + " : " + isHeader + " : " + tagName);
	}
	@Override
	public void cell(String cellReference, String formattedValue, XSSFComment comment) {
		// TODO Auto-generated method stub
		f_data.printMessage(false,"Run cell: " + cellReference + " : " + formattedValue + " : " + comment);
	}

	public void cell(String cellReference, String formattedValue, String formula, XSSFCellStyle cellStyle) {
		if(f_datatype.equals("d")) {
		formattedValue = f_data.getM_df().format(DateUtil.getJavaDate(Double.parseDouble(formattedValue)));
		}
		else if(f_datatype.equals("n"))
		{
			formattedValue = formattedValue.replace(".", ",").replaceAll("([^\\d-,])|([\\.,](0)+\\z)", "");//.replaceAll(",(0)+\\z", "");

		}	
/*		
		if(!f_data.getM_doc().getD_book().formats.containsKey(f_format.getName())) {
			f_data.getM_doc().getD_book().formats.put(this.f_format.getName(), f_format);
		}*/
		f_cell = new murCell();
		f_cell.setFormat(-1);
		f_cell.setStyle(-1);
		f_cell.setDatatype(f_datatype);
		f_cell.setValue(formattedValue);
		f_cell.setName(cellReference.replaceAll("\\d+", ""));
		f_cell.setFormula(formula);
		f_cell.setWidth(-1);
		int number = CellReference.convertColStringToIndex(cellReference.replaceAll("\\d+", "")) +1;
		f_cell.setNumber(number);
		f_cell.setValidation(-1);
		f_row.cells.add(f_cell);
	}

	@Override
	public void endRow(int rowNum) {
		// TODO Auto-generated method stub
		f_data.printMessage(false,"Run endRow: " + rowNum);
	}

	public void endRow() {
		/* вот тут добавляется строка в листе */
		f_sheet.rows.add(f_row);
		// TODO Auto-generated method stub
		f_data.printMessage(false,"Run endRow: ");

	}
	@Override
	public void startRow(int rowNum) {
		f_row = new murRow(1);
		f_row.setName(rowNum);
		f_row.setHeight(-1);
		f_data.printMessage(false,"Run startRow: " + rowNum);
	}

}
