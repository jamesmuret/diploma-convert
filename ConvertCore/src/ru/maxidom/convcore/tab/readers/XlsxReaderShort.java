package ru.maxidom.convcore.tab.readers;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.poi.ooxml.util.SAXHelper;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.StylesTable;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.filedata.Document;
import ru.maxidom.convcore.filedata.ExcelData;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.iRead;
import ru.maxidom.convcore.tab.readers.eventmodel.MyXSSFSheetXMLHandlerShort;
import ru.maxidom.convcore.tab.readers.eventmodel.murSheetContentsHandlerShort;

public class XlsxReaderShort implements iRead {
	ExcelData e_data;

	@Override
	public void readData() throws Exception {
		parseXMLtable(e_data.getInFile());

	}

	private void parseXMLtable(String filename) throws IOException, SAXException, OpenXML4JException {
		e_data.printMessage(false,"Start parse bigExcel" + filename);
		try (OPCPackage container = OPCPackage.open(filename)) {
			ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(container);
			XSSFReader xssfReader = new XSSFReader(container);
			StylesTable styles = xssfReader.getStylesTable();
			XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator) xssfReader.getSheetsData();
			// автора и дату, надо брать из файла без открытия книги.
			e_data.setM_doc(new Document(1, "athr", ""));
			while (iter.hasNext()) {
				try (InputStream stream = iter.next()) {
					e_data.printMessage(false,"Get sheet: " + iter.getSheetName() + " : " + styles.getNumDataFormats());
					murSheet f_sheet = processSheet(styles, strings, stream);
					f_sheet.setName(iter.getSheetName());
					e_data.getM_doc().getD_book().sheets.add(f_sheet);
				}
				e_data.printMessage(false,"End parse big" + filename);
			}
		}
	}
	private murSheet processSheet(StylesTable styles, ReadOnlySharedStringsTable strings, InputStream sheetInputStream)
			throws IOException, SAXException {
		DataFormatter formatter = new DataFormatter();
		InputSource sheetSource = new InputSource(sheetInputStream);
		try {
			XMLReader sheetParser = SAXHelper.newXMLReader();
			murSheetContentsHandlerShort sheetHandler = new murSheetContentsHandlerShort();
			murSheet p_sheet = new murSheet(1);
			sheetHandler.setH_sheet(p_sheet);
			sheetHandler.setH_data(e_data);
			ContentHandler handler = new MyXSSFSheetXMLHandlerShort(styles, strings, sheetHandler, formatter, false);
			sheetParser.setContentHandler(handler);
			e_data.printMessage(false,"Start parse sheet");
			sheetParser.parse(sheetSource);
			return p_sheet;
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
		}
	}

	@Override
	public void setFileData(FileDataBase m_data) throws Exception {
		e_data=(ExcelData)m_data;
	}

	@Override
	public void close() throws Exception {
	}
}
