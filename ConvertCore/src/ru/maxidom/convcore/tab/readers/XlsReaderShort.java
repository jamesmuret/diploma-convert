package ru.maxidom.convcore.tab.readers;

import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormatter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.filedata.Document;
import ru.maxidom.convcore.filedata.ExcelData;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.parse.DocTags;
import ru.maxidom.convcore.parse.iRead;

public class XlsReaderShort implements iRead {
	
	ExcelData e_data;

	@Override
	public void readData() throws Exception {
/*		Workbook wb = WorkbookFactory.create(new File(FileIN));
		keepSheets(wb);
		parseSheets(wb);*/
	}

	private Font parseFont(CellStyle cs, Workbook wb) {
		Font res_font = null;
		res_font = ((HSSFCellStyle) cs).getFont(wb);
/*
		case XLSX:
			res_font = ((XSSFCellStyle) cs).getFont();
			break;
		case XLS:
			res_font = ((HSSFCellStyle) cs).getFont(wb);
			break;
		default:
			throw new RuntimeException("unsupported type");
		}*/
		return res_font;
	}
/*
	private String parseStyle(CellStyle c_style, Workbook wb) {
		// номер стиля
		int styleId = (int) c_style.getIndex();
		boolean underl;
		murStyle p_style = new murStyle();
		murFont p_font = new murFont();
		HSSFCellStyle x_style = (HSSFCellStyle) c_style;
		x_style.getFont(wb);
		Font tmp_font = parseFont(c_style, wb);
		switch (tmp_font.getUnderline()) {
		case Font.U_NONE:
			underl = false;
			break;
		default:
			underl = true;
			break;
		}
		p_font.setUnderlined(underl);
		p_font.setName(tmp_font.getFontName());
		p_font.setItalic(tmp_font.getItalic());
		p_font.setBold(tmp_font.getBold());
		p_font.setStrike(tmp_font.getStrikeout());
		p_font.setSize(String.valueOf(tmp_font.getFontHeightInPoints()));
		p_font.setHorizontal(String.valueOf(c_style.getAlignment()));
		p_font.setVertical(String.valueOf(c_style.getVerticalAlignment()));
		e_data.logConsole(p_font.toString());
		p_style.setFont(Optional.of(p_font));
		p_style.setName("s" + String.valueOf(styleId));
		p_style.setLeft(String.valueOf(c_style.getBorderLeft()));
		p_style.setRight(String.valueOf(c_style.getBorderRight()));
		p_style.setTop(String.valueOf(c_style.getBorderTop()));
		p_style.setBottom(String.valueOf(c_style.getBorderBottom()));
		String colors[] = getColors(c_style, tmp_font);
		p_style.setFrontColor(colors[0]);
		p_style.setBackColor(colors[1]);
		p_style.setFontColor(colors[2]);
	    String fillPat = x_style.getFillPatternEnum().toString();
		p_style.setPattern(fillPat);
		e_data.logConsole(p_style.toString());
		// если ключ есть то добавляю по ключу String,murStyle
		// если же есть то я возвращу идентификатор первого похожего стиля
		if (!e_data.getM_doc().getD_book().styles.containsKey(p_style.getName()))
			e_data.getM_doc().getD_book().styles.put(p_style.getName(), p_style);
		return p_style.getName();
	}
*//*
	private String parseValRegions(DataValidation dv) {
		StringBuilder res_regions = new StringBuilder();
		int t_size = dv.getRegions().getCellRangeAddresses().length;
		for (int i = 0; i < t_size; i++) {
			String tmp_res = dv.getRegions().getCellRangeAddress(i).formatAsString();
			res_regions.append(tmp_res);
			if (i != (t_size - 1))
				res_regions.append(";");
		}
		return res_regions.toString();
	}
*/
	private void keepSheets(Workbook wb) throws IOException {
		HSSFWorkbook hwb = (HSSFWorkbook) wb;
//		SummaryInformation sI = hwb.getSummaryInformation();
		e_data.setM_doc(new Document(hwb.getNumberOfSheets(), "robot", "01.01.2019"));
		e_data.getM_doc().setPallette(hwb.getCustomPalette());
		/*
		 * записал стили в память for (int i = 0; i < hwb.getNumCellStyles(); i++) {
		 * m_doc.stmap.put(hwb.getCellStyleAt(i), i); }
		 */
		/* записал листы в память */
		/*for (Sheet sht : wb) {
			e_data.getM_doc().getSheets().add(sht);
		}*/
		/*
		 * записал шрифты в документ for (int i = 0; i <= hwb.getNumberOfFontsAsInt();
		 * i++) { m_doc.fmap.put(i, hwb.getFontAt(i)); }
		 */
		/* Закрыл документ */
//		hwb.close();
	}
	private void parseSheets(Workbook wb) throws IOException {
		murSheet p_sheet = null;
/*		murValidation p_validation;
		int vname = 0;
		String valType = null;
		String opType;
		String form1 = null;
		String form2 = null;
		Date f1;
		Date f2;*/
		String cType = null;
		String cValue = null;
		String cForm = null;
		int vald = -1;
	//	DataFormatter m_dform = new DataFormatter();
		HSSFDataFormatter hform = new HSSFDataFormatter();
		for (Sheet sheet : wb) {
			if (sheet.getPhysicalNumberOfRows() == 0)
				continue;
			p_sheet = new murSheet(1);
			p_sheet.setName(sheet.getSheetName());
			/* Ограничения список */

			/*
			try {
				for (DataValidation dv : sheet.getDataValidations()) {
					DataValidationConstraint vc = dv.getValidationConstraint();
					switch (dv.getValidationConstraint().getOperator()) {
					case DataValidationConstraint.OperatorType.BETWEEN:
						opType = "BETWEEN";
						break;
					case DataValidationConstraint.OperatorType.EQUAL:
						opType = "EQUAL";
						break;
					case DataValidationConstraint.OperatorType.GREATER_OR_EQUAL:
						opType = "GREATER_OR_EQUAL";
						break;
					case DataValidationConstraint.OperatorType.GREATER_THAN:
						opType = "GREATER_THAN";
						break;
					case DataValidationConstraint.OperatorType.LESS_OR_EQUAL:
						opType = "LESS_OR_EQUAL";
						break;
					case DataValidationConstraint.OperatorType.LESS_THAN:
						opType = "LESS_THAN";
						break;
					case DataValidationConstraint.OperatorType.NOT_BETWEEN:
						opType = "NOT_BETWEEN";
						break;
					case DataValidationConstraint.OperatorType.NOT_EQUAL:
						opType = "NOT_EQUAL";
						break;
					default:
						opType = "NULL";
					}

					try {
						form1 = dv.getValidationConstraint().getFormula1();
						form2 = dv.getValidationConstraint().getFormula2();
						if (form1 != null && form2 != null
								&& (form1.matches(".*RowOffset.*") || form2.matches(".*RowOffset.*")))
							continue;
						switch (vc.getValidationType()) {
						case ValidationType.ANY:
							valType = "ANY";
							break;
						case ValidationType.DATE:
							valType = "DATE";
							if (form1 != null) {
								f1 = DateUtil.getJavaDate((double) Integer.parseInt(form1));
								form1 = e_data.getM_df().format(f1);
							}
							if (form2 != null) {
								f2 = DateUtil.getJavaDate((double) Integer.parseInt(form2));
								form2 = e_data.getM_df().format(f2);
							}
							break;
						case ValidationType.DECIMAL:
							valType = "DECIMAL";
							break;
						case ValidationType.FORMULA:
							valType = "FORMULA";
							break;
						case ValidationType.INTEGER:
							valType = "INTEGER";
							break;
						case ValidationType.LIST:
							valType = "LIST";
							if (vc.getExplicitListValues() != null)
								form1 = String.join(";", vc.getExplicitListValues());
							form2 = "";
							break;
						case ValidationType.TEXT_LENGTH:
							valType = "TEXT_LENGTH";
							break;
						case ValidationType.TIME:
							valType = "TIME";
							break;
						default:
							valType = "NULL";
						}
					} catch (NullPointerException ex) {
	
					}
					p_validation = new murValidation();
					p_validation.setRegions(parseValRegions(dv));
					p_validation.setDatatype(valType);
					p_validation.setMax(form2);
					p_validation.setMin(form1);
					p_validation.setName("v" + String.valueOf(vname));
					p_validation.setOperator(opType);
					vname++;
//					e_data.getM_doc().getVdmap().put(dv, vname);
					p_sheet.vals.put(p_validation.getName(), p_validation);
				}
			} catch (IllegalStateException il) {
				e_data.logConsole("Empty validate table");
			}
			*/
			for (Row row : sheet) {
				if (row.getPhysicalNumberOfCells() == 0) {
					continue;
				}
				murRow p_row = new murRow(1);
				p_row.setName(row.getRowNum() + 1);
				p_row.setHeight(-1);
				//p_row.setHeight(String.valueOf(row.getHeight()));
				for (Cell cell : row) {
					murCell p_cell = new murCell();
					int fmt = cell.getCellStyle().getDataFormat();
					String fms = cell.getCellStyle().getDataFormatString();
					e_data.getM_doc().getCfmap().put(fmt, fms);
					switch (cell.getCellType()) {
					case STRING:
						cType = CellType.STRING.toString();
						cValue = cell.getRichStringCellValue().getString();
						cForm = "";
						break;
					case NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							cType = "DATE";
							cValue = e_data.getM_df().format(cell.getDateCellValue());
						} else {
							cType = CellType.NUMERIC.toString();
							cValue =getNumericFormatted(cell, hform);
							cForm = "";
						}
						break;
					case BOOLEAN:
						cType = CellType.BOOLEAN.toString();
						cValue = String.valueOf(cell.getBooleanCellValue());
						cForm = "";
						break;
					case FORMULA:
						cForm = cell.getCellFormula();
						switch (cell.getCachedFormulaResultType()) {
						case STRING:
							cType = CellType.STRING.toString();
							cValue = cell.getRichStringCellValue().getString();
							break;
						case NUMERIC:
							if (DateUtil.isCellDateFormatted(cell)) {
								cType = "DATE";
								cValue = e_data.getM_df().format(cell.getDateCellValue());
								break;
							} else {
								cType = CellType.NUMERIC.toString();
								cValue = String.valueOf(cell.getNumericCellValue()).replaceAll("\\.", ",");
							}
							break;
						case BOOLEAN:
							cType = CellType.BOOLEAN.toString();
							cValue = String.valueOf(cell.getBooleanCellValue());
							break;
						default:
							break;
						}
						break;
					case ERROR:
						cType = cell.getCellType().toString();
						cForm = "";
						cValue = String.valueOf(cell.getErrorCellValue());
						break;
					case BLANK:
						continue;
					/*
					 * cType = "EMPTY"; cValue = ""; cForm = ""; break;
					 */
					case _NONE:
						continue;
					/*
					 * cType = "EMPTY"; cValue = ""; cForm = ""; break;
					 */
					default:
						continue;
					/* break; */
					}
				//	CellStyle c_style = cell.getCellStyle();
					int cId = cell.getColumnIndex();
			//		int cWid = sheet.getColumnWidth(cId);
					String colLet = CellReference.convertNumToColString(cId);
					/* привязка ячейки к ограничению */
				/*
					try {

						for (DataValidation dv : e_data.getM_doc().getVdmap().keySet()) {
							for (CellRangeAddress adr : dv.getRegions().getCellRangeAddresses()) {
								if (adr.containsRow(row.getRowNum()) && adr.containsColumn(cId)) {
									vald = e_data.getM_doc().getVdmap().get(dv);
								} else
									continue;
							}
						}
					} catch (Exception ex) {
						e_data.logConsole("empty limitation");
					}
					*/
					p_cell.setWidth(-1);
					p_cell.setName(colLet);
					p_cell.setNumber(cId + 1);
					p_cell.setDatatype(cType);
					p_cell.setStyle(-1);
					p_cell.setValue(cValue);
					p_cell.setValidation(vald);
					p_cell.setFormula(cForm);
					p_cell.setFormat(-1);
					e_data.printMessage(false,p_cell.toString());
					p_row.cells.add(p_cell);
				}
				e_data.printMessage(false,(DocTags.ROW + p_row.getName()));
				p_sheet.rows.add(p_row);
			}
		/*	for (Entry<Integer, String> cellFormat : e_data.getM_doc().getCfmap().entrySet()) {
				murCellFormat p_format = new murCellFormat();
				p_format.setName("f" + String.valueOf(cellFormat.getKey()));
				p_format.setType(cellFormat.getValue());
				e_data.logConsole(p_format.toString());
				e_data.getM_doc().getD_book().formats.put(p_format.getName(), p_format);
			}*/
			e_data.getM_doc().getD_book().sheets.add(p_sheet);
		}
		wb.close();
	}

	private String getNumericFormatted(Cell cell, HSSFDataFormatter hm_df)  {
		return hm_df.formatCellValue(cell).replaceAll("[^\\d\\,\\-]", "");
		/*switch (m_docType) {
			return m_df.formatCellValue(cell).replaceAll("[^\\d\\,\\-]", "");
		}*/
	}
/*
	private String[] getColors(CellStyle cstyle, Font font) {
		short ColorInd;
		String colors[] = new String[3];

		ColorInd = cstyle.getFillForegroundColor();
		// а что мне делать, для того, чтобы поймать несуществующий цвет
		if (IndexedColors.fromInt(ColorInd) != IndexedColors.AUTOMATIC)
			colors[0] = getColorPattern(e_data.getM_doc().getPallette().getColor(ColorInd));
		else
			colors[0] = "0:0:0";
		ColorInd = cstyle.getFillBackgroundColor();
		if (IndexedColors.fromInt(ColorInd) != IndexedColors.AUTOMATIC)
			colors[1] = getColorPattern(e_data.getM_doc().getPallette().getColor(ColorInd));
		else
			colors[1] = "0:0:0";
		ColorInd = font.getColor();
		if (ColorInd != 32767)
			colors[2] = getColorPattern(e_data.getM_doc().getPallette().getColor(ColorInd));
		else
			colors[2] = "0:0:0";
		/* sCoolorFor-colors[0]; sColorBack-colors[1];sColorFont-colors[2]; 
		switch (m_docType) {
		case XLS:
			ColorInd = cstyle.getFillForegroundColor();
			// а что мне делать, для того, чтобы поймать несуществующий цвет
			if (IndexedColors.fromInt(ColorInd) != IndexedColors.AUTOMATIC)
				colors[0] = getColorPattern(getM_doc().pallette.getColor(ColorInd));
			else
				colors[0] = "0:0:0";
			ColorInd = cstyle.getFillBackgroundColor();
			if (IndexedColors.fromInt(ColorInd) != IndexedColors.AUTOMATIC)
				colors[1] = getColorPattern(getM_doc().pallette.getColor(ColorInd));
			else
				colors[1] = "0:0:0";
			ColorInd = font.getColor();
			if (ColorInd != 32767)
				colors[2] = getColorPattern(getM_doc().pallette.getColor(ColorInd));
			else
				colors[2] = "0:0:0";
			break;
		case XLSX:
			colors[0] = getColorPattern(((XSSFCellStyle) cstyle).getFillForegroundXSSFColor());
			colors[1] = getColorPattern(((XSSFCellStyle) cstyle).getFillBackgroundXSSFColor());
			colors[2] = getColorPattern(((XSSFFont) font).getXSSFColor());
			break;
		default:
			break;
		}
		return colors;
	}
*/
	/*
	 * private String getCellColor(Color color) { HSSFColor hCol = null; XSSFColor
	 * xCol = null; if (color == null) return ""; switch (this.m_docType) { case
	 * XLSX: xCol = XSSFColor.toXSSFColor(color); short[] srgbColor = null; byte[]
	 * brgbColor = null; byte[] resb = xCol.getRGB(); if (resb != null) return
	 * String.join(":", String.valueOf(resb[0]), String.valueOf(resb[1]),
	 * String.valueOf(resb[2])); return "0:0:0"; case XLS: hCol =
	 * HSSFColor.toHSSFColor(color); short[] ress = hCol.getTriplet(); return
	 * String.join(":", String.valueOf(ress[0]), String.valueOf(ress[1]),
	 * String.valueOf(ress[2])); default: return ""; } }
	 */
/*
	private String getColorPattern(Color color) {
		return new String("murmur");
	}
	private String getColorPattern(Color color) {
		short[] srgbColor = null;
		String outStr = "";
		if (color != null) {
			srgbColor = ((HSSFColor)color).getTriplet();
			if (srgbColor != null)
				outStr = srgbColor[0] + ":" + srgbColor[1] + ":" + srgbColor[2];
		}
		return outStr;
	}
	/*
	private String getColorPattern(XSSFColor xColor) {
		short[] srgbColor = null;
		byte[] brgbColor = null;
		String outStr = "";
		if (xColor != null) {
			brgbColor = xColor.getRGB();
			if (brgbColor != null) {
				srgbColor = new short[3];

				for (int i = 0; i < 3; i++)
					srgbColor[i] = (short) ((short) brgbColor[i] & 0x00FF);

				outStr = srgbColor[0] + ":" + srgbColor[1] + ":" + srgbColor[2];
			}
		}
		return outStr;
	}
*/
	@Override
	public void setFileData(FileDataBase m_data) throws Exception {
		e_data =(ExcelData)m_data;
	}

	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	/*
	@SuppressWarnings("unused")
	private String getFontColor(Font font) {
		String retColor = " ";
		HSSFFont hfCol = null;
		XSSFFont xfCol = null;
		
		try {
			if (font == null)
				throw (new Exception(""));

			if (font == null)
				throw (new Exception(""));

			switch (m_docType) {
			case XLSX:
				xfCol = (XSSFFont) font;
				byte[] resb = xfCol.getXSSFColor().getRGB();
				return String.join(":", String.valueOf(resb[0]), String.valueOf(resb[1]), String.valueOf(resb[2]));
			case XLS:
				hfCol = (HSSFFont) font;
				short[] ress = getM_doc().pallette.getColor((hfCol.getColor())).getTriplet();
				return String.join(":", String.valueOf(ress[0]), String.valueOf(ress[1]), String.valueOf(ress[2]));
			default:
				return "";
			}
		} catch (Exception ex) {
			retColor = " ";
		}
		return retColor;
	}*/
}
