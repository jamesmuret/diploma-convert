package ru.maxidom.convcore.arbiter;

import ru.maxidom.convcore.algs.AlgBase;
import ru.maxidom.convcore.algs.iAlgExecute;

public class StringArbiter extends BaseArbiter {

	String algFullName;

	@Override
	public void setParams(String... strings) {
		if(strings.length==1)
			algFullName = strings[0];
	}

	public iAlgExecute findStrategy() throws Exception {
		AlgBase alg = null;
		try {
			alg = (AlgBase) (Class.forName(algFullName).newInstance());
		} catch (Exception ex) {
			throw new Exception(ex + "unknown strategy");
		}
		return alg;
	}

}
