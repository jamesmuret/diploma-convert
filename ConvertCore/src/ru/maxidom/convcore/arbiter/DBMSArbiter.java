package ru.maxidom.convcore.arbiter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import ru.maxidom.convcore.algs.AlgBase;
import ru.maxidom.convcore.algs.iAlgExecute;

public class DBMSArbiter implements iArbite {
	String inType;
	String outType;
	String algFcn;
	Connection conn;
	static String algProc = "{? = call convdb.getAlgName(?,?)}";
	
	@Override
	public iAlgExecute findStrategy() throws Exception {
		doArbition();
		AlgBase alg = null;
		try {
			alg = (AlgBase) (Class.forName(algFcn).newInstance());
		} catch (Exception ex) {
			throw new Exception(ex + "alg name not found");
		}
		return alg;
	}


	private void doArbition() throws Exception {
		try (CallableStatement stmt = conn.prepareCall(algProc);) {
			stmt.registerOutParameter(1, Types.VARCHAR);
			stmt.setString(2, inType);
			stmt.setString(3, outType);
			stmt.execute();
			algFcn = stmt.getString(1).trim();
		} catch (SQLException ex) {
			throw new Exception( ex.getMessage() +"alg search DBMS exception");
		}
	}
	}
