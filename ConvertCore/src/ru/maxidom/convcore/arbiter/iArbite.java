package ru.maxidom.convcore.arbiter;

import ru.maxidom.convcore.algs.iAlgExecute;

public interface iArbite {
	public iAlgExecute findStrategy() throws Exception;
}
