package ru.maxidom.convcore.parse;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public interface iFromXML {

	public void readFromXML(String filename) throws ParserConfigurationException, SAXException, IOException;
	public void writeToTab(String filename) throws FileNotFoundException, IOException, ParseException;
}
