package ru.maxidom.convcore.parse;

public interface iWrite extends iData {
	public void writeData() throws Exception;
	public void setOutParameter(String OutParameter);
}
