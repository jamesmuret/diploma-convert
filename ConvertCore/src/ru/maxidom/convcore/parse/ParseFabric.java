package ru.maxidom.convcore.parse;

import ru.maxidom.convcore.bashexecutors.EdsExecutor;
import ru.maxidom.convcore.bashexecutors.PdfExec;
import ru.maxidom.convcore.bashexecutors.SignMessage;
import ru.maxidom.convcore.filedata.ExcelData;
import ru.maxidom.convcore.filedata.FileDataBase;
import ru.maxidom.convcore.filedata.TestExcelData;
import ru.maxidom.convcore.tab.readers.BigXlsxReader;
import ru.maxidom.convcore.tab.readers.TestXlsxReader;
import ru.maxidom.convcore.tab.readers.XlsReaderShort;
import ru.maxidom.convcore.tab.readers.XlsxReaderShort;
import ru.maxidom.convcore.tab.readers.XmlReaderShort;
import ru.maxidom.convcore.tab.writers.BigXmlWriter;
import ru.maxidom.convcore.tab.writers.UxslsxWriterShort;
import ru.maxidom.convcore.tab.writers.XLSXwriterShort;
import ru.maxidom.convcore.tab.writers.XLSwriterShort;
import ru.maxidom.convcore.tab.writers.XlsxWriter;
import ru.maxidom.convcore.tab.writers.XmlWriter;
import ru.maxidom.convcore.tab.writers.XmlWriterShort;
import ru.maxidom.convcore.tab.writers.XmlWriterTest;

public class ParseFabric {
	public static EdsExecutor getBashExecutor(String otType) throws Exception{
		EdsExecutor resExecutor =null;
		switch (DocTypes.valueOf(otType.trim().toUpperCase().split("#")[0])) {
		case EDM:
			resExecutor = new SignMessage();
			break;
		case PDF:
			resExecutor = new PdfExec();
			break;	
		default:
			throw new Exception("unknown Type bash executor");
		}
		return resExecutor;
	}
	public static FileDataBase getFileData(String inType,String otType) throws Exception {
		FileDataBase res =null;
		StringBuilder io = new StringBuilder(inType);
		io.append(otType);
		if((io.toString().toLowerCase().contains("t")))
			res = new TestExcelData();
		else if (io.toString().toLowerCase().contains("xls"))
			res = new ExcelData();
		return res;
	}
	
	public static iRead getReader(String inType) throws Exception {
		iRead resReader = null;
		switch (DocTypes.valueOf(inType.trim().toUpperCase())) {
		case FXLS:
			resReader = new XlsReaderShort();
			break;
		case FXLSX:
			resReader = new XlsxReaderShort();
			break;
		case FXML:
			resReader = new XmlReaderShort();
			break;
		case BXLSX:
			resReader = (iRead) new BigXlsxReader();
			break;
		case TXLSX:
			resReader = new TestXlsxReader();
			break;
		default:
			throw new Exception("unknown Type reader");
		}
		return resReader;
	}
	public static iWrite getWriter(String otType) throws Exception  {
		iWrite resWriter = null;
		switch (DocTypes.valueOf(otType.trim().toUpperCase().split("#")[0])) {
		case XLSX:
			resWriter = new XlsxWriter();
			break;
		case XML:
			resWriter = new XmlWriter();
			break;
		case FXLS:
			resWriter = new XLSwriterShort();
			break;
		case FXLSX:
			resWriter = new XLSXwriterShort();
			break;
		case FXML:
			resWriter = new XmlWriterShort();
			break;
		case UXLSX:
			resWriter = new UxslsxWriterShort();
			break;
		case BXML:
			resWriter = (iWrite) new BigXmlWriter();
			break;
		case TXML:
			resWriter = new XmlWriterTest();
			break;
		default:
			throw new Exception("unknown Type writer");
		}
		resWriter.setOutParameter(otType);
		return resWriter;
	}
}
