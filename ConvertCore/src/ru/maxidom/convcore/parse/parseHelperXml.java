package ru.maxidom.convcore.parse;

import java.util.Optional;

import org.apache.poi.ss.usermodel.CellType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murCellFormat;
import ru.maxidom.convcore.excel.murColumn;
import ru.maxidom.convcore.excel.murFont;
import ru.maxidom.convcore.excel.murStyle;
import ru.maxidom.convcore.excel.murValidation;

public class parseHelperXml {
	public static String readtSingleAttVal(Node node, String attName) {
		Node tmpNode = node.getAttributes().getNamedItem(attName);
		return (tmpNode != null) ? tmpNode.getNodeValue() : "";
	}

	public static murColumn readColumn(Node colNode) throws Exception {
		murColumn resColumn = new murColumn();
		String width = readtSingleAttVal(colNode, DocTags.WIDTH);
		String format = readtSingleAttVal(colNode, DocTags.FORMAT);
		String min = readtSingleAttVal(colNode, DocTags.MIN);
		String max = readtSingleAttVal(colNode, DocTags.MAX);
		if (!width.isEmpty())
			resColumn.setWidth(Integer.valueOf(width));
		if (!format.isEmpty())
			resColumn.setFormat(Integer.valueOf(format));
			resColumn.setMin(Integer.valueOf(min));
			resColumn.setMax(Integer.valueOf(max));
		resColumn.setDatatype(readtSingleAttVal(colNode, DocTags.DATATYPE));
		return resColumn;
	}
	public static Node readSingleElement(Element element, String e_name) {
		return element.getElementsByTagName(e_name).item(0);
	}

	public static NodeList readListElements(Element element, String e_name) {
		return element.getElementsByTagName(e_name);
	}

	public static murFont readFont(NodeList fontNode, NodeList align) throws Exception {
		murFont resFont = new murFont();
		resFont.setBold(readtSingleAttVal(fontNode.item(0), DocTags.BOLD).equals("1"));
		resFont.setHorizontal(readtSingleAttVal(align.item(0), DocTags.HORIZONTAL));
		resFont.setItalic(readtSingleAttVal(fontNode.item(0), DocTags.ITALIC).equals("1"));
		resFont.setName(readtSingleAttVal(fontNode.item(0), DocTags.NAME));
		resFont.setSize(readtSingleAttVal(fontNode.item(0), DocTags.SIZE));
		resFont.setStrike(readtSingleAttVal(fontNode.item(0), DocTags.STRIKE).equals("1"));
		resFont.setUnderlined(readtSingleAttVal(fontNode.item(0), DocTags.UNDERLINED).equals("1"));
		resFont.setVertical(readtSingleAttVal(align.item(0), DocTags.VERTICAL));
		return resFont;
	}

	public static murCell readCell(Node cell) throws Exception {
		murCell resCell = new murCell();
		String fmt = readtSingleAttVal(cell, DocTags.FORMAT);
		String stl = readtSingleAttVal(cell, DocTags.STYLE);
		String val = readtSingleAttVal(cell, DocTags.VALIDATION);
		String wi = readtSingleAttVal(cell, DocTags.WIDTH);
		Integer format = Integer.valueOf(fmt.isEmpty() ? "-1" : fmt);
		Integer style = Integer.valueOf(stl.isEmpty() ? "-1" : stl);
		Integer validation = Integer.valueOf(val.isEmpty() ? "-1" : val);
		Integer width = Integer.valueOf(wi.isEmpty() ? "-1" : wi);
		stl = val = fmt = wi = null;
		String datatype = readtSingleAttVal(cell, DocTags.DATATYPE);
		String name = readtSingleAttVal(cell, DocTags.NAME);
		Integer number = Integer.valueOf(readtSingleAttVal(cell, DocTags.NUMBER));
		/* способ сказать о том, что недостаточно атрибутов для чтения */
		if (datatype == null || name == null) {
			throw new Exception("wrong Cell attributes");
		}
		/* нельзя чтобы отсутствовал только стиль без формата */
		if ((format == null && style != null) || (format != null && style == null)) {
			throw new Exception("format&style at Cell" + name);
		}
		resCell.setDatatype(datatype);
		if (format != -1)
			resCell.setFormat(format);
		if (style != -1)
			resCell.setStyle(style);
		resCell.setName(name);
		resCell.setNumber(number);
		if (validation != -1)
			resCell.setValidation(validation);
		if (width != -1)
			resCell.setWidth(width);
		// -=========================--------------========================
		Node n_form = readSingleElement((Element) cell, DocTags.FORMULA);
		Node n_value = readSingleElement((Element) cell, DocTags.VALUE);
		resCell.setFormula(n_form!=null?n_form.getTextContent():"");
		resCell.setValue(n_value.getTextContent());
		return resCell;
	}

	public static murCellFormat readFormat(Node format) throws Exception {
		murCellFormat resFormat = new murCellFormat();
		resFormat.setName(Integer.valueOf(readtSingleAttVal(format, DocTags.NAME)));
		resFormat.setType(readtSingleAttVal(format, DocTags.TYPE));
		return resFormat;
	}

	public static murValidation readVal(Node validation) throws Exception {
		murValidation resVal = new murValidation();
		resVal.setName(readtSingleAttVal(validation, DocTags.NAME));
		resVal.setMin((readtSingleAttVal(validation, DocTags.MIN)));
		resVal.setMax(readtSingleAttVal(validation, DocTags.MAX));
		resVal.setDatatype((readtSingleAttVal(validation, DocTags.DATATYPE)));
		resVal.setOperator((readtSingleAttVal(validation, DocTags.OPERATOR)));
		resVal.setRegions(validation.getTextContent());
		return resVal;

	}

	public static CellType parseCellType(String c_type, boolean formula) throws Exception {
		CellType res_type;
		if (formula == true) {
			return CellType.FORMULA;
		} else if (c_type.toLowerCase().equals("d"))
			res_type = CellType.NUMERIC;
		else {
			res_type = CellType.valueOf(c_type);
		}
		return res_type;
	}

	public static murStyle readStyle(murFont font, Node n_style, NodeList n_color, NodeList n_border) throws Exception {
		murStyle resStyle = new murStyle();
		resStyle.setFont(Optional.of(font));
		resStyle.setName(Integer.valueOf(readtSingleAttVal(n_style, DocTags.NAME)));
		/* colors */
		resStyle.setFontColor(readtSingleAttVal(n_color.item(0), DocTags.FONT));
		resStyle.setFrontColor(readtSingleAttVal(n_color.item(0), DocTags.FRONT));
		resStyle.setBackColor(readtSingleAttVal(n_color.item(0), DocTags.BACK));
		/* borders */
		resStyle.setLeft(readtSingleAttVal(n_border.item(0), DocTags.LEFT));
		resStyle.setRight(readtSingleAttVal(n_border.item(0), DocTags.RIGHT));
		resStyle.setTop(readtSingleAttVal(n_border.item(0), DocTags.TOP));
		resStyle.setBottom(readtSingleAttVal(n_border.item(0), DocTags.BOTTOM));
		resStyle.setPattern(n_color.item(0).getTextContent());
		return resStyle;
	}
}
