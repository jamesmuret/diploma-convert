package ru.maxidom.convcore.parse;

import ru.maxidom.convcore.excel.murCell;
import ru.maxidom.convcore.excel.murColumn;
import ru.maxidom.convcore.excel.murRow;
import ru.maxidom.convcore.excel.murSheet;
import ru.maxidom.convcore.filedata.FileDataBase;

public interface iDirectWrite extends iWrite {
	public void setFileData(FileDataBase m_data) throws Exception;
	void startCell(murCell cell) throws Exception;
	void endCell() throws Exception;
	void startRow(murRow row) throws Exception;
	void endRow() throws Exception;
	void startSheet(murSheet sheet) throws Exception;
	void endSheet() throws Exception;
	void startColumn(murSheet sheet,murColumn col) throws Exception;
	void closeDoc() throws Exception;
	void startDoc() throws Exception;
	void finalizeData() throws Exception;
}
