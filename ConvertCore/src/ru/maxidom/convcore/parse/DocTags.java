package ru.maxidom.convcore.parse;

public  class DocTags {

	public static final String LIST = "ul";
	public static  String SCORE = "SCORE";
	public static  String ENCODE = "UTF-8";
	public static  String Filename = "li";
	public static  String DATEPATTERN = "dd.MM.yyyy HH:mm:ss";
	public static  String DATA = "data";
	public static  String DOCNAME = "dnm";
	public static  String AUTHOR = "a";
	public static  String DATE = "dt";
	public static  String STYLES = "ss";

	public static  String FONT = "fn";
	public static  String NAME = "na";
	public static  String SIZE = "sz";
	public static  String ITALIC = "i";
	public static  String BOLD = "b";

	public static  String STRIKE = "st";
	public static  String UNDERLINED = "u";
	public static  String BORDER = "bd";
	public static  String LEFT = "l";
	public static  String TOP = "t";
	public static  String RIGHT = "r";
	public static  String BOTTOM = "b";

	public static  String ALIGN = "al";
	public static  String VERTICAL = "vr";
	public static  String HORIZONTAL = "hr";
	
	public static  String COLOR = "cr";
	public static  String FRONT = "fr";
	public static  String BACK = "bk";

	public static  String WORKSHEET = "ws";
	public static  String WORKSHEETS = "wss";
	public static  String VALIDATIONS = "vls";
	public static  String VALIDATION = "vl";
	public static  String MIN = "min";
	public static  String MAX = "max";
	public static  String DATATYPE = "t";
	public static  String OPERATOR = "op";
	public static  String REGIONS = "rg";

	public static  String ROW = "r";
	public static  String ROWS = "rs";
	public static  String NUMBER = "nm";
	public static  String HEIGHT = "h";

	public static  String CELL = "c";
	public static  String FORMATS = "fs";
	public static  String FORMAT = "f";
	public static  String STYLE = "s";
	public static  String VALUE = "v";
	public static  String FORMULA = "fr";
	public static  String CELLFORMAT = "f";
	public static  String TYPE = "t";
	public static  String WIDTH = "w";
	public static  String COLUMN = "cl";
	public static  String COLUMNS = "cls";
	
	
	public static String TEST = "TEST";
	public static String INFO = "INFO";
	
	public static String Quantity = "Quantity";
	
	
	public static String Name  ="Name";
	

	public static String QuestionsCount  ="QuestionsCount";
	public static String TestType  ="TestType";
	public static String IsExamMode  ="IsExamMode";
	public static String IsTimeLimit  ="IsTimeLimit";
	public static String TimeLimit  ="TimeLimit";
	public static String IsBack  ="IsBack";
	public static String IsShowScore  ="IsShowScore";
	
	public static String IsShowResultsMessage  ="IsShowResultsMessage";

	
	
	
	public static String THEMES = "THEMES";
	public static String THEME = "THEME_";
	public static String Caption = "Caption";
	public static String ShowInResults = "ShowInResults";
	public static String ShowInResultsBody = "TRUE";
	public static String QUESTIONS = "QUESTIONS";
	public static String QUESTION = "QUESTION_";
	public static String TEXT_HTML = "TEXT_HTML";
	public static String ANSWERS = "ANSWERS";
	public static String ANSWER_ = "ANSWER_";
	public static String ISRIGHT = "ISRIGHT";
	public static String AQuantity = "AQuantity";
	public static String Type  = "Type";
	public static String ThemeID = "ThemeID";
	public static String Weight = "Weight";
	public static String TEXT = "TEXT";
	public static String NumExam = "NumExam";
	public static String Autor = " <Author>Максидом</Author>";
	public static String GeneralTest = " Обобщенный";
	public static String Description = "Description_HTML";
	public static String brake = "&lt;p class=rvps1&gt;&lt;span class=rvts2&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;";
	
}
