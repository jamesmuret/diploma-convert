package ru.maxidom.convcore.parse;

public enum DocTypes {
	XLS, ODT, XLSX,XML,FXLS,FXLSX,FXML, UXLSX,EDM,EDS,EDF,EDD,PDF,BXLSX,BXML, TXLSX, TXML
}
