package ru.maxidom.convcore.parse;

import ru.maxidom.convcore.filedata.FileDataBase;

public interface iData extends AutoCloseable {
	public void setFileData(FileDataBase m_data) throws Exception;
}
