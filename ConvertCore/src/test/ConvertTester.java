package test;

import ru.maxidom.convcore.worker.ConvertWorker;

public class ConvertTester {
	public static void main(String[] args) {
	ConvertWorker wk = new ConvertWorker(true);
	long start = System.currentTimeMillis();
	String algName = "ru.maxidom.convcore.algs.RhwAlg";
	String inFile = "C:\\Users\\dmimur\\22\\SunRav_24062020_262495_TXLSX11.xlsx";
	String outFile= "C:\\Users\\dmimur\\22\\AutoTest.zip";
	String inType ="TXLSX";
	String outType="TXML#z";
	try {
		wk.doConvert(inType,inFile,outType,outFile,algName);
	}
	catch(Throwable ex) {
		System.out.println(ex.getMessage());
		ex.printStackTrace();
	}
	finally {
	}
	long fin = System.currentTimeMillis();	
	System.out.print("Time conversion"+ (fin-start)/1000);
	}
}