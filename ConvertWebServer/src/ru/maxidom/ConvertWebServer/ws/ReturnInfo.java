package ru.maxidom.ConvertWebServer.ws;

public class ReturnInfo {
	public boolean isError;
	public String ReturnMsg;
	
	public ReturnInfo() {
		isError = false;
		ReturnMsg = "";
	}
	
	public void SetErrorMsg( String Msg ) {
		isError = true;
		ReturnMsg = Msg;		
	}

}
