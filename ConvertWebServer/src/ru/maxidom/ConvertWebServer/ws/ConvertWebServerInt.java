package ru.maxidom.ConvertWebServer.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import ru.maxidom.ConvertWebServer.ws.AnswerClass;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC  )
public interface ConvertWebServerInt 
{
	@WebMethod
	public AnswerClass getHelloString();
	@WebMethod
	public AnswerClass testDBConnection();
	@WebMethod
	public TaskInfoAnswer createTask(  @WebParam(name = "UserId")  String UserId, 
									   @WebParam(name = "InType")   String InType, 
									   @WebParam(name = "OutType")    String OutType, 
									   @WebParam(name = "BodyBase64")  String BodyBase64,
									   @WebParam(name = "Mode")   int Mode);

	@WebMethod
	public AnswerClass addFileData( @WebParam(name = "TaskId") long TaskId,
									@WebParam(name = "FileName")  String FileName, 
									@WebParam(name = "BodyBase64")String BodyBase64,
									@WebParam(name = "Mode") int Mode);
	@WebMethod
	public TaskInfoAnswer readTaskStatus( @WebParam(name = "TaskId") long TaskId );
	@WebMethod
	public FileBodyAnswer readTaskOutFile(@WebParam(name = "TaskId")  long TaskId,
										  @WebParam(name = "FileOutName") 	String FileOutName, 
										  @WebParam(name = "StartPos")long StartPos, 
										  @WebParam(name = "LenRead") long LenRead );
	@WebMethod
	public AnswerClass getLastTaskId();
	@WebMethod
	public TaskInfoAnswer createSignTask(@WebParam(name ="UserId") String userId,
										 @WebParam(name = "MessageBase64")String messageBase64,
										 @WebParam(name = "sha1Base64") String sha1);
	

	@WebMethod 
	public FileBodyAnswer readTestTemplateFile( @WebParam(name = "StartPos")long StartPos, @WebParam(name = "LenRead") long LenRead ); 
	
}
