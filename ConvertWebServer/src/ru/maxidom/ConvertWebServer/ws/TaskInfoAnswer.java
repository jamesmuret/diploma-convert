package ru.maxidom.ConvertWebServer.ws;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="TaskInfoAnswer")
public class TaskInfoAnswer extends AnswerClass 
{
	private long TaskId;
	private Date TaskDtTm;
	private int Status;
	private Date StatusDtTm;
	private String InFileType;
	private String InFile;
	private long InFileLength;
	private String OutFileType;
	private String OutFile;
	private long OutFileLength;
	private String ErrorMsg;
	private String SendUserId;
	
	public TaskInfoAnswer() 
	{
		super();
		Clear();
	}
	
	public void Clear() 
	{
		super.Clear();
		ClearInfo();
	}

	public void ClearInfo() 
	{
		TaskId = 0;
		TaskDtTm = new Date();
		Status = 0;
		StatusDtTm = new Date();
		InFileType = "";
		InFile = "";
		InFileLength = 0;
		OutFileType = "";
		OutFile = "";
		OutFileLength = 0;
		ErrorMsg = "";
		SendUserId = "";
	}
	
	@XmlElement(name="TaskId") 
	public long getTaskId(){ return TaskId;	}
	
	public void setTaskId( long value ) { TaskId = value; }
	
	@XmlElement(name="TaskDate") 
	public Date getTaskDate() { return TaskDtTm; }
	
	public void setTaskDate( Date value ) { TaskDtTm = value; }


	@XmlElement(name="Status") 
	public int getStatus() { return Status; }
	
	public void setStatus( int value ) { Status = value; }
	
	@XmlElement(name="StatusDate") 
	public Date getStatusDate() { return StatusDtTm; }
	
	public void setStatusDate( Date value ){ StatusDtTm = value; }

	@XmlElement(name="InFileType") 
	public String getInFileType() { return InFileType; }
	
	public void setInFileType( String value ) { InFileType = value; }
	
	@XmlElement(name="InFile") 
	public String getInFile() { return InFile; }
	
	public void setInFile( String value ) { InFile = value; }
	
	@XmlElement(name="InFileLength") 
	public long getInFileLength() { return InFileLength; }
	
	public void setInFileLength( long value ) { InFileLength = value; }
	
	@XmlElement(name="OutFileType") 
	public String getOutFileType() { return OutFileType; }
	
	public void setOutFileType( String value ) { OutFileType = value; }
	
	@XmlElement(name="OutFile") 
	public String getOutFile() { return OutFile; }
	
	public void setOutFile( String value ) { OutFile = value; }

	@XmlElement(name="OutFileLength") 
	public long getOutFileLength() { return OutFileLength; }
	
	public void setOutFileLength( long value ) { OutFileLength = value; }
	
	@XmlElement(name="ErrorMsg") 
	public String getErrorMsg() { return ErrorMsg; }
	
	public void setErrorMsg( String value ) { ErrorMsg = value; }
	
	@XmlElement(name="SendUserId") 
	public String getSendUserId() { return SendUserId; }
	
	public void setSendUserId( String value ) { SendUserId = value; }
}
