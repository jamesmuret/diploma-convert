package ru.maxidom.ConvertWebServer.ws;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import ru.maxidom.ConvertWebServer.endpoint.ConvertWebServerPublisher;

public class WorkFile 
{
	private static  Set<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
	static {
 		perms.add(PosixFilePermission.OWNER_WRITE);
        perms.add(PosixFilePermission.OWNER_READ);
        perms.add(PosixFilePermission.OWNER_EXECUTE);
        perms.add(PosixFilePermission.GROUP_WRITE);
        perms.add(PosixFilePermission.GROUP_READ);
        perms.add(PosixFilePermission.GROUP_EXECUTE);
        perms.add(PosixFilePermission.OTHERS_WRITE);
        perms.add(PosixFilePermission.OTHERS_READ);
        perms.add(PosixFilePermission.OTHERS_EXECUTE);
	}
	
	
	public String GenerateFileName( long TaskId, String Type, boolean IsOut, String WorkDirectory )
	{
		StringBuilder  FileName = new StringBuilder(WorkDirectory);
		try
		{
			DateFormat dtForm = new SimpleDateFormat("yyyyMMddHHMMSS");

			FileName.append("task_");
			FileName.append(dtForm.format( new Date() ));
			FileName.append("_");
			FileName.append(String.format("%08d", TaskId ));
			FileName.append("_");
			FileName.append(Type.trim().split("#")[0]);
		}
		catch( Exception e )
		{			
			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
			FileName.setLength(0);
			FileName.append(WorkDirectory);
			FileName.append("task_");
			FileName.append(String.format("%08d", TaskId ));
			FileName.append("_");
			FileName.append(Type.trim().split("#")[0]);
		}
		
		if( IsOut) FileName.append(".out");
		else FileName.append(".in");
		String res = FileName.toString();
		FileName.setLength(0);
		return res;
	}
	
	public boolean CreateTemplateFiles( String InFileName, String OutFileName )
	{
		boolean Ret = false;
		File inFile = null;
		File outFile = null;
		
		try
		{
			ConvertWebServerPublisher.PrintMessage( false,  "Create template file: "  + InFileName );
			inFile = new File( InFileName );
			if( inFile.exists() ) inFile.delete();
			inFile.createNewFile();
			SetPosixAtts(InFileName);
			ConvertWebServerPublisher.PrintMessage( false,  "Create template file: "  + OutFileName );
			outFile = new File( OutFileName );
			if( outFile.exists() ) outFile.delete();
			outFile.createNewFile();
			SetPosixAtts(OutFileName);
			Ret = true;
		}
		catch( Exception e )
		{
			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
			
			if( inFile != null ) try{ inFile.delete(); }catch( Exception ei) {}
			if( outFile != null ) try{ outFile.delete(); }catch( Exception eo ) {}
			
			Ret = false;
		}
		
		return Ret;
	}
	
	public boolean DeleteTemplateFiles( String InFileName, String OutFileName )
	{
		boolean Ret = false;
		File inFile = null;
		File outFile = null;
		
		try
		{
			if( InFileName.length() > 0 )
			{
				inFile = new File( InFileName );
				if( inFile.exists() )
				{
					ConvertWebServerPublisher.PrintMessage( false,  "Delete template file: "  + InFileName );
					inFile.delete();
				}
			}
			
			if( OutFileName.length() > 0 )
			{
				outFile = new File( OutFileName );
				if( outFile.exists() )
				{
					ConvertWebServerPublisher.PrintMessage( false,  "Delete template file: "  + OutFileName );
					outFile.delete();
				}
			}
			
			Ret = true;
		}
		catch( Exception e )
		{
			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
			
			if( inFile != null ) try{ inFile.delete(); }catch( Exception ei) {}
			if( outFile != null ) try{ outFile.delete(); }catch( Exception eo ) {}
			
			Ret = false;
		}
		
		return Ret;
	}
	
	public long GetFileSize( String FileName )
	{
		long Len = 0;
		File InfoFile = null;
		
		try
		{
			InfoFile = new File( FileName );
			if( InfoFile.exists() )
				Len = InfoFile.length();
		}
		catch( Exception e )
		{
			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
			Len = 0;
		}
		
		return Len;
	}
	
	public boolean SetPosixAtts(String FileName) throws IOException {
	        Files.setPosixFilePermissions(Paths.get(FileName), perms);
		return true;
	}
	
	
	public boolean SaveFileData( String FileName, byte[] Body )
	{
		boolean Ret = false;

		RandomAccessFile TemplFile = null;
		
		try
		{
			TemplFile = new RandomAccessFile( FileName, "rw" );
			TemplFile.seek( TemplFile.length() );
			TemplFile.write( Body );
			
			Ret = true;
		}
		catch( IOException ioe )
		{
			ConvertWebServerPublisher.PrintMessage( true,  ioe.getMessage() );
		}
		catch( Exception e )
		{
			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
		}
		finally
		{
			if( TemplFile != null ) try{ TemplFile.close(); }catch( Exception e) {}
		}
		
		return Ret;
	}
}
