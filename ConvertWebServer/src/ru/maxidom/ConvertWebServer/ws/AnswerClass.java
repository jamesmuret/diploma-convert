package ru.maxidom.ConvertWebServer.ws;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="Answer")
public class AnswerClass {
	private String m_DateResponse;
	private boolean m_Error;
	private int m_ErrorCode;
	private String m_ReturnMessage;
	
	public AnswerClass()
	{
		Clear();
	}
	
	protected void Clear()
	{
		SetCurrentDateResponse();
		
		m_Error = false;
		m_ErrorCode = 0;
		m_ReturnMessage = "";
	}
	
	public void SetCurrentDateResponse()
	{
		DateFormat dtForm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date curDate = new Date();
		
		m_DateResponse = dtForm.format( curDate );
	}
	
	@XmlAttribute(name="DateResponse") 
	public String getDateResponse()
	{
		return m_DateResponse;
	}

	@XmlAttribute(name="Error") 
	public boolean getError()
	{
		return m_Error;
	}

	@XmlAttribute(name="ErrorCode") 
	public int getErrorCode()
	{
		return m_ErrorCode;
	}

	@XmlElement(name="ReturnMessage") 
	public String getReturnMessage()
	{
		return m_ReturnMessage;
	}

	public void setDateResponse(String value )
	{
		m_DateResponse = value;
	}

	public void setError( boolean value )
	{
		m_Error = value ;
	}

	public void setErrorCode( int value)
	{
		m_ErrorCode = value;
	}

	public void setReturnMessage( String value )
	{
		try
		{
			m_ReturnMessage = new String( value.getBytes("UTF-8"), "UTF-8");
		}
		catch( Exception e )
		{
			m_ReturnMessage = value;
		}
	}
}
