package ru.maxidom.ConvertWebServer.ws;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;

import ru.maxidom.ConvertWebServer.endpoint.ConvertWebServerPublisher;

public class WorkDBSQL {
	Connection DBSQLConnection;
	String DBConnection;
	String DBName;
	String DBUser;
	String DBPassword;
	
	public WorkDBSQL( String dbConnection
					, String dbName 
			        , String dbUser 
			        , String dbPassword ) 
	{
		
		DBSQLConnection = null;
		DBConnection = dbConnection;
		DBName = dbName;
		DBUser = dbUser;
		DBPassword = dbPassword;
	}
	
	public boolean IsOpenConnect()
	{
		boolean Ret = false;
		
		try 
		{
			if( DBSQLConnection != null )
				Ret = !DBSQLConnection.isClosed();
		}
		catch( Exception e ) 
		{
			
			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
		}
		
		return Ret;
	}
	
	public ReturnInfo OpenConnect()
	{
		ReturnInfo Ret = new ReturnInfo();
		CloseConnect();
		
		try 
		{
			
			DBSQLConnection = DriverManager.getConnection( DBConnection, DBUser, DBPassword);
			
		}
		catch( Exception e )
		{
			
			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
			Ret.SetErrorMsg(e.getMessage());
		}
		
		return Ret;
	}
	
	public void CloseConnect() 
	{
		if( !IsOpenConnect()) return;
		
		try { DBSQLConnection.close(); }catch( Exception e) {}
	}
	
	public boolean SetAutoCommit( boolean IsAuto )
	{
		boolean Ret = false;
		
		try
		{
			if( IsOpenConnect() == false ) throw( new Exception("No database connection"));
			
			DBSQLConnection.setAutoCommit( IsAuto );
			Ret = true;
		}
		catch( SQLException sqle)
		{
			ConvertWebServerPublisher.PrintMessage( true, sqle.getMessage() );
			Ret = false;
		}
		catch( Exception e)
		{
			ConvertWebServerPublisher.PrintMessage( true, e.getMessage() );
			Ret = false;
		}
		
		return Ret;
	}
	
	public boolean Commit( )
	{
		boolean Ret = false;
		
		try
		{
			if( IsOpenConnect() == false ) throw( new Exception("No database connection"));
			
			DBSQLConnection.commit();
			Ret = true;
		}
		catch( SQLException sqle)
		{
			ConvertWebServerPublisher.PrintMessage( true, sqle.getMessage() );
			Ret = false;
		}
		catch( Exception e)
		{
			ConvertWebServerPublisher.PrintMessage( true, e.getMessage() );
			Ret = false;
		}
		
		return Ret;
	}
	
	public boolean RollBack( )
	{
		boolean Ret = false;
		
		try
		{
			if( IsOpenConnect() == false ) throw( new Exception("No database connection"));
			
			DBSQLConnection.rollback();
			Ret = true;
		}
		catch( SQLException sqle)
		{
			ConvertWebServerPublisher.PrintMessage( true, sqle.getMessage() );
			Ret = false;
		}
		catch( Exception e)
		{
			ConvertWebServerPublisher.PrintMessage( true, e.getMessage() );
			Ret = false;
		}
		
		return Ret;
	}
	
	public long CreateTask( String UserId )
	{
		long TaskId = 0;
		Statement stmtCmd = null;
		ResultSet resSet = null;
		
		try
		{
			if( IsOpenConnect() == false ) throw( new Exception("No database connection"));
			
			String strQuery;
			
			strQuery = "INSERT INTO " + DBName.trim() + ".tasks ( send_user_id ) "
					 + "VALUES('" + UserId + "')";

			stmtCmd = DBSQLConnection.createStatement();
			stmtCmd.executeUpdate( strQuery, Statement.RETURN_GENERATED_KEYS );
			resSet = stmtCmd.executeQuery("SELECT LAST_INSERT_ID()"); 
			
			if( resSet.first() == false ) throw( new Exception("Can't create task!"));
			else 
			{
				if( resSet.getObject( 1 ) == null ) throw( new Exception("Can't read new task id!"));
				else if( resSet.getLong( 1 ) == 0 ) throw( new Exception("Incorrect task id type!"));
					
				TaskId = resSet.getLong( 1 );
			}
		}
		catch( SQLException sqle)
		{
			ConvertWebServerPublisher.PrintMessage( true, sqle.getMessage() );
			TaskId = 0;
		}
		catch( Exception e)
		{
			ConvertWebServerPublisher.PrintMessage( true, e.getMessage() );
			TaskId = 0;
		}
		finally
		{
			try { if( stmtCmd != null ) stmtCmd.close(); } catch( SQLException se ) {}
		}

		return TaskId;
	}
	
	public long GetLastTaskId() {
		PreparedStatement stmtCmd = null;
		ResultSet resSet = null;
		long TaskId = -1;
		try
		{
			if( IsOpenConnect() == false ) throw( new Exception("No database connection"));
			String strQuery = null;
			strQuery = "SELECT MAX(t.task_id) FROM " + DBName.trim() + ".tasks t ";  
			stmtCmd = DBSQLConnection.prepareStatement( strQuery );
			resSet = stmtCmd.executeQuery();
			if( resSet.first() == false ) throw( new Exception("Can't find task"));
			else 
			{
				if( resSet.getObject( 1 ) != null ) TaskId = resSet.getLong( 1 );
			}
		}
		catch( SQLException sqle)
		{
			ConvertWebServerPublisher.PrintMessage( true, sqle.getMessage() );
			TaskId = -1;
		}
		catch( Exception e)
		{
			ConvertWebServerPublisher.PrintMessage( true, e.getMessage() );
			TaskId = -1;
		}
		finally
		{
			try { if( stmtCmd != null ) stmtCmd.close(); } catch( SQLException se ) {}
		}
		return TaskId;
	}
		
	
	
	
	public boolean UpdateTaskFiles( long TaskId
			                  , String InFileType, String InFile
			                  , String OutFileType, String OutFile )
	{
		PreparedStatement stmtCmd = null;
		boolean Ret = false;
		
		
		try
		{
			if( IsOpenConnect() == false ) throw( new Exception("No database connection"));
			
			String strQuery;
			
			strQuery = "UPDATE " + DBName.trim() + ".tasks SET "
					 + "  in_file_type ='" + InFileType.trim() + "' "
					 + ", in_file = '" + InFile.trim() + "' "
					 + ", out_file_type = '" + OutFileType.trim() + "' "
					 + ", out_file = '" + OutFile.trim() + "' "
					 + " WHERE task_id = " + TaskId ;  

			stmtCmd = DBSQLConnection.prepareStatement( strQuery );
			stmtCmd.executeUpdate();
			
			Ret = true;
		}
		catch( SQLException sqle)
		{
			ConvertWebServerPublisher.PrintMessage( true, sqle.getMessage() );
			Ret = false;
		}
		catch( Exception e)
		{
			ConvertWebServerPublisher.PrintMessage( true, e.getMessage() );
			Ret = false;
		}
		finally
		{
			try { if( stmtCmd != null ) stmtCmd.close(); } catch( SQLException se ) {}
		}

		return Ret;
	}
	
	public boolean UpdateTaskStatus( long TaskId, int Status )
	{
		PreparedStatement stmtCmd = null;
		boolean Ret = false;
		
		try
		{
			if( IsOpenConnect() == false ) throw( new Exception("No database connection"));
			
			String strQuery;
			
			strQuery = "UPDATE " + DBName.trim() + ".tasks SET "
					 + "  status = " + Status 
					 + " WHERE task_id = " + TaskId ;  

			stmtCmd = DBSQLConnection.prepareStatement( strQuery );
			stmtCmd.executeUpdate();
			
			Ret = true;
		}
		catch( SQLException sqle)
		{
			ConvertWebServerPublisher.PrintMessage( true, sqle.getMessage() );
			Ret = false;
		}
		catch( Exception e)
		{
			ConvertWebServerPublisher.PrintMessage( true, e.getMessage() );
			Ret = false;
		}
		finally
		{
			try { if( stmtCmd != null ) stmtCmd.close(); } catch( SQLException se ) {}
		}

		return Ret;
	}
	
	public int GetTaskStatus( long TaskId )
	{
		PreparedStatement stmtCmd = null;
		ResultSet resSet = null;
		int TaskStatus = -1;
		
		try
		{
			if( IsOpenConnect() == false ) throw( new Exception("No database connection"));
			
			String strQuery;
			
			strQuery = "SELECT status FROM " + DBName.trim() + ".tasks "
					 + " WHERE task_id = " + TaskId ;  

			stmtCmd = DBSQLConnection.prepareStatement( strQuery );
			resSet = stmtCmd.executeQuery();
			
			if( resSet.first() == false ) throw( new Exception("Can't find task"));
			else 
			{
				if( resSet.getObject( 1 ) != null ) TaskStatus = resSet.getInt( 1 );
			}
			
		}
		catch( SQLException sqle)
		{
			ConvertWebServerPublisher.PrintMessage( true, sqle.getMessage() );
			TaskStatus = -1;
		}
		catch( Exception e)
		{
			ConvertWebServerPublisher.PrintMessage( true, e.getMessage() );
			TaskStatus = -1;
		}
		finally
		{
			try { if( stmtCmd != null ) stmtCmd.close(); } catch( SQLException se ) {}
		}

		return TaskStatus;
	}
	
	public TaskInfoAnswer GetTaskInfo( long TaskId )
	{
		TaskInfoAnswer Ret = new TaskInfoAnswer();
		PreparedStatement stmtCmd = null;
		ResultSet resSet = null;
		
		try
		{
			if( IsOpenConnect() == false ) throw( new Exception("No database connection"));
			
			String strQuery;
			
			strQuery = "SELECT task_id, dt_task, status, dt_status, in_file_type, in_file"
					 + "     , out_file_type, out_file, error_msg, send_user_id"
					 + "  FROM " + DBName.trim() + ".tasks " 
					 + " WHERE task_id = " + TaskId;
		
			stmtCmd = DBSQLConnection.prepareStatement( strQuery );
			resSet = stmtCmd.executeQuery();
			
			if( resSet.first() == false ) throw( new Exception("Can't find task"));
			else 
			{
				if( resSet.getObject( 1 ) != null ) Ret.setTaskId( resSet.getLong( 1 ) );
				if( resSet.getObject( 2 ) != null ) Ret.setTaskDate( resSet.getDate( 2 ) ); 
				if( resSet.getObject( 3 ) != null ) Ret.setStatus( resSet.getInt( 3 ) ); 
				if( resSet.getObject( 4 ) != null ) Ret.setStatusDate( resSet.getDate( 4 ) ); 
				if( resSet.getObject( 5 ) != null ) Ret.setInFileType( resSet.getString( 5 ).trim() ); 
				if( resSet.getObject( 6 ) != null ) Ret.setInFile( resSet.getString( 6 ).trim() ); 
				if( resSet.getObject( 7 ) != null ) Ret.setOutFileType( resSet.getString( 7 ).trim() ); 
				if( resSet.getObject( 8 ) != null ) Ret.setOutFile( resSet.getString( 8 ).trim() );
				if( resSet.getObject( 9 ) != null ) Ret.setErrorMsg( resSet.getString( 9 ).trim() );
				if( resSet.getObject( 10 ) != null ) Ret.setSendUserId(resSet.getString( 10 ).trim() );
				
				if( Ret.getTaskId() == 0 ||
					Ret.getInFileType() == "" || Ret.getInFile() == "" ||
					Ret.getOutFileType() == "" || Ret.getOutFile() == "" ) 
					throw( new Exception("Error in task info data!") );
			}

		}
		catch( SQLException sqle)
		{
			ConvertWebServerPublisher.PrintMessage( true, sqle.getMessage() );
			Ret.setErrorCode( 6 );
			Ret.setReturnMessage( sqle.getMessage() );
		}
		catch( Exception e)
		{
			ConvertWebServerPublisher.PrintMessage( true, e.getMessage() );
			Ret.setErrorCode( 7 );
			Ret.setReturnMessage( e.getMessage() );
		}
		finally
		{
			try { if( stmtCmd != null ) stmtCmd.close(); } catch( SQLException se ) {}
		}
		
		return Ret;
	}
}
