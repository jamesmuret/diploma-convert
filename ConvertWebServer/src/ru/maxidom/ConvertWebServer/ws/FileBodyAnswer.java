package ru.maxidom.ConvertWebServer.ws;

import java.util.Base64;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="FileBobyAnswer")
public class FileBodyAnswer extends AnswerClass 
{
	private String m_FileOutBody;
	private int m_BodyBase64Length;
	
	public FileBodyAnswer()
	{
		super();
		Clear();
	}
	
	public void Clear() 
	{
		super.Clear();
		
		m_FileOutBody = "";
		m_BodyBase64Length = 0;
	}
	
	@XmlElement(name="FileOutBodyBase64") 
	public String getFileOutBodyBase64()
	{
		return m_FileOutBody;
	}

	public void setFileOutBodyBase64(byte[] value )
	{
		m_FileOutBody = Base64.getEncoder().encodeToString( value  );
		m_BodyBase64Length = m_FileOutBody.length();
	}

	@XmlElement(name="BodyBase64Length") 
	public int getBodyBase64Length()
	{
		return m_BodyBase64Length;
	}

	public void setBodyBase64Length(int value )
	{
		m_BodyBase64Length = value;
	}
}
