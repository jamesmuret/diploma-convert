package ru.maxidom.ConvertWebServer.ws;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Base64;
import javax.jws.WebService;
import ru.maxidom.ConvertWebServer.endpoint.ConvertWebServerPublisher;
import ru.maxidom.ConvertWebServer.ws.AnswerClass;

@WebService(endpointInterface = "ru.maxidom.ConvertWebServer.ws.ConvertWebServer")

public class ConvertWebServer implements ConvertWebServerInt {

	private String WorkDirectory;
	private String DBConnection;
	private String DBName;
	private String DBUser;
	private String DBPassword;
	private String TestTemplateFile;
	
	public ConvertWebServer( String workDirectory
			               , String dbConnection
			               , String dbName
			               , String dbUser
			               , String dbPassword
			               , String testTemplateFile) 
	{
		WorkDirectory = workDirectory;
		DBConnection = dbConnection;
		DBName = dbName;
		DBUser = dbUser;
		DBPassword = dbPassword;
		TestTemplateFile = testTemplateFile;
	}
	
	@Override
	public AnswerClass getHelloString()
	{
		ConvertWebServerPublisher.PrintMessage( false,  "Run getHelloString" );
		AnswerClass Ret = new AnswerClass();
		Ret.setReturnMessage("Hello from ConvertWebService!");
		return Ret; 
	}

	@Override
	public AnswerClass testDBConnection()
	{
		ConvertWebServerPublisher.PrintMessage( false,  "Run testDBConnection" );
		AnswerClass Ret = new AnswerClass();
		WorkDBSQL WorkDB = new WorkDBSQL( DBConnection, DBName, DBUser, DBPassword );
		ReturnInfo RetInfo;
		try{
			
			RetInfo = WorkDB.OpenConnect();
			if( RetInfo.isError ) throw( new Exception( RetInfo.ReturnMsg ));
			
		}
		catch( Exception e )
		{
			Ret.setError( true );
			Ret.setErrorCode(1);
			Ret.setReturnMessage( e.getMessage());
		}
		finally 
		{
			WorkDB.CloseConnect();
		}
		return Ret; 
	}
	
	@Override
	public TaskInfoAnswer createTask( String UserId, String InType, String OutType, String BodyBase64, int Mode)
	{
		TaskInfoAnswer RetAnswer = new TaskInfoAnswer();
		WorkDBSQL WorkDB = new WorkDBSQL( DBConnection, DBName, DBUser, DBPassword );
		ReturnInfo RetInfo = new ReturnInfo();
		long TaskId = 0;
		String InFile = "";
		String OutFile = "";
		WorkFile workFile = new WorkFile();
		
		ConvertWebServerPublisher.PrintMessage( false,  "Run createTask" );
		ConvertWebServerPublisher.PrintMessage( false,  "InType" + InType  +" OutType" + OutType );
		try 
		{
			RetInfo = WorkDB.OpenConnect();
			if( RetInfo.isError ) throw( new Exception("Error in connect to database!"));

			WorkDB.SetAutoCommit( false );
			
			TaskId = WorkDB.CreateTask( UserId );
			if( TaskId == 0 ) throw( new Exception("Error in create task"));
			
			InFile = workFile.GenerateFileName( TaskId, InType, false, WorkDirectory );
			OutFile = workFile.GenerateFileName( TaskId, OutType, true, WorkDirectory );
			
			if( workFile.CreateTemplateFiles( InFile, OutFile ) == false )
				throw( new Exception("Error in create template files"));
			
			if( BodyBase64 != null )
			{
				ConvertWebServerPublisher.PrintMessage( false,  "Base64: " + BodyBase64 );
				
				if( workFile.SaveFileData( InFile, Base64.getDecoder().decode( BodyBase64 ) ) == false )
					throw( new Exception("Error in save infile data"));
			}
			else 	ConvertWebServerPublisher.PrintMessage( false,  "BodyBase64 not found check addFileData service" );
			
			if( WorkDB.UpdateTaskFiles( TaskId,  InType, InFile, OutType, OutFile) == false )
				throw( new Exception("Error in update template files in task"));
			
			if( Mode == 1 && WorkDB.UpdateTaskStatus( TaskId,  10 ) == false )
				throw( new Exception("Error in set task status"));
			
			RetAnswer = WorkDB.GetTaskInfo( TaskId );
			if( RetAnswer.getErrorCode() != 0 )  throw( new Exception("Error in get task information!"));

			RetAnswer.setInFileLength( workFile.GetFileSize( RetAnswer.getInFile() ) );
			
			
			WorkDB.Commit();
			WorkDB.SetAutoCommit( true );
		}
		catch( Exception e )
		{
			WorkDB.RollBack();
			WorkDB.SetAutoCommit( true );
			
			workFile.DeleteTemplateFiles( InFile, OutFile );
			
			RetAnswer.setError( true );
			if( RetAnswer.getErrorCode() == 0 ) RetAnswer.setErrorCode( 1 );
			if( RetAnswer.getReturnMessage().length() == 0 ) RetAnswer.setReturnMessage(e.getMessage());
			else RetAnswer.setReturnMessage( RetAnswer.getReturnMessage() + ";\n" + e.getMessage());

			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
		}
		finally
		{
			RetAnswer.SetCurrentDateResponse();
			WorkDB.CloseConnect();
		}
		
		return RetAnswer;
	}

	@Override
	public AnswerClass addFileData( long TaskId, String FileName, String BodyBase64, int Mode)
	{
		WorkDBSQL WorkDB = new WorkDBSQL( DBConnection, DBName, DBUser, DBPassword );
		ReturnInfo RetInfo = new ReturnInfo();
		AnswerClass RetAnswer = new AnswerClass();
		WorkFile workFile = new WorkFile();
		
		ConvertWebServerPublisher.PrintMessage( false,  "Run addFileData" );
		
		try
		{
			RetInfo = WorkDB.OpenConnect();
			if( RetInfo.isError ) throw( new Exception( RetInfo.ReturnMsg + ";\nError in connect to database!"));
			
			if( WorkDB.GetTaskStatus( TaskId ) != 0 )
				throw( new Exception("Incorrect task status!"));
			
			if( workFile.SaveFileData( FileName, Base64.getDecoder().decode( BodyBase64 ) ) == false )
				throw( new Exception("Error in save infile data"));
			
			if( Mode == 1 && WorkDB.UpdateTaskStatus( TaskId,  10 ) == false )
				throw( new Exception("Error in set task status"));
		}
		catch( Exception e )
		{
			RetAnswer.setError( true );
			if( RetAnswer.getErrorCode() == 0 ) RetAnswer.setErrorCode( 2 );
			if( RetAnswer.getReturnMessage().length() == 0 ) RetAnswer.setReturnMessage(e.getMessage());
			else RetAnswer.setReturnMessage( RetAnswer.getReturnMessage() + ";\n" + e.getMessage());

			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
		}
		finally
		{
			RetAnswer.SetCurrentDateResponse();
			WorkDB.CloseConnect();
		}
		
		return RetAnswer;
	}

	@Override
	public TaskInfoAnswer readTaskStatus( long TaskId )
	{
		TaskInfoAnswer RetAnswer = new TaskInfoAnswer();
		WorkDBSQL WorkDB = new WorkDBSQL( DBConnection, DBName, DBUser, DBPassword );
		ReturnInfo RetInfo = new ReturnInfo();
		WorkFile workFile = new WorkFile();
		
		ConvertWebServerPublisher.PrintMessage( false,  "Run readTaskStatus" );
		
		try
		{
			RetInfo = WorkDB.OpenConnect();
			if( RetInfo.isError ) throw( new Exception( RetInfo.ReturnMsg + ";\nError in connect to database!"));
			
			RetAnswer = WorkDB.GetTaskInfo( TaskId );
			if( RetAnswer.getErrorCode() != 0 )  throw( new Exception("Error in get task information!"));
			
			RetAnswer.setInFileLength( workFile.GetFileSize( RetAnswer.getInFile() ) );
			RetAnswer.setOutFileLength( workFile.GetFileSize( RetAnswer.getOutFile() ) );
		}
		catch( Exception e )
		{
			RetAnswer.ClearInfo();
			RetAnswer.setError( true );
			if( RetAnswer.getErrorCode() == 0 ) RetAnswer.setErrorCode( 3 );
			if( RetAnswer.getReturnMessage().length() == 0 ) RetAnswer.setReturnMessage(e.getMessage());
			else RetAnswer.setReturnMessage( RetAnswer.getReturnMessage() + ";\n" + e.getMessage());

			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
		}
		finally
		{
			RetAnswer.SetCurrentDateResponse();
			WorkDB.CloseConnect();
		}
		ConvertWebServerPublisher.PrintMessage( false,"task_id:"+ RetAnswer.getTaskId()+ " send_user_id:"+RetAnswer.getSendUserId()+ " state_id:"+RetAnswer.getStatus() +" state_date:"+RetAnswer.getStatusDate());
		return RetAnswer;
	}

	@Override
	public FileBodyAnswer readTaskOutFile( long TaskId, String FileOutName, long StartPos, long LenRead )
	{
		WorkDBSQL WorkDB = new WorkDBSQL( DBConnection, DBName, DBUser, DBPassword );
		ReturnInfo RetInfo = new ReturnInfo();
		FileBodyAnswer RetAnswer = new FileBodyAnswer();
		RandomAccessFile ReadFile = null;
		
		ConvertWebServerPublisher.PrintMessage( false,  "Run readTaskOutFile" );

		try
		{
			RetInfo = WorkDB.OpenConnect();
			if( RetInfo.isError ) throw( new Exception( RetInfo.ReturnMsg + ";\nError in connect to database!"));
			
			if( WorkDB.GetTaskStatus( TaskId ) != 30 )
				throw( new Exception("Incorrect task status!"));
			
			ReadFile = new RandomAccessFile( FileOutName, "r" );
			
			if( LenRead == 0 ) LenRead = ReadFile.length() - StartPos;
			
			if( (StartPos + LenRead ) > ReadFile.length() ||
			     StartPos < 0 || LenRead < 0 )
				throw( new Exception("Unable to determine file block parameters!"));
			if(StartPos + LenRead > 1_000_000)
				throw(new Exception("Asked file length is too big use portions"));
			
			byte[] fileBody = new byte[ (int) LenRead ];
			ReadFile.seek( StartPos );
			ReadFile.read( fileBody, 0, (int) LenRead );
			
			RetAnswer.setFileOutBodyBase64( fileBody );
		}
		catch( IOException ioe)
		{
			RetAnswer.setError( true );
			if( RetAnswer.getErrorCode() == 0 ) RetAnswer.setErrorCode( 4 );
			if( RetAnswer.getReturnMessage().length() == 0 ) RetAnswer.setReturnMessage(ioe.getMessage());
			else RetAnswer.setReturnMessage( RetAnswer.getReturnMessage() + ";\n" + ioe.getMessage());

			ConvertWebServerPublisher.PrintMessage( true,  ioe.getMessage() );
		}
		catch( Exception e )
		{
			RetAnswer.setError( true );
			if( RetAnswer.getErrorCode() == 0 ) RetAnswer.setErrorCode( 4 );
			if( RetAnswer.getReturnMessage().length() == 0 ) RetAnswer.setReturnMessage(e.getMessage());
			else RetAnswer.setReturnMessage( RetAnswer.getReturnMessage() + ";\n" + e.getMessage());

			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
		}
		finally
		{
			RetAnswer.SetCurrentDateResponse();
			if( ReadFile != null ) try { ReadFile.close(); }catch( Exception e) {}
			WorkDB.CloseConnect();
		}
		
		return RetAnswer;
	}



	@Override
	public AnswerClass getLastTaskId() {
		ReturnInfo RetInfo = new ReturnInfo();
		AnswerClass Ret = new AnswerClass();
		WorkDBSQL WorkDB = new WorkDBSQL( DBConnection, DBName, DBUser, DBPassword );
		ConvertWebServerPublisher.PrintMessage( false,  "Run getLastTaskId" );
		try
		{
			RetInfo = WorkDB.OpenConnect();
			if( RetInfo.isError ) throw( new Exception( RetInfo.ReturnMsg + ";\nError in connect to database!"));
			long res = WorkDB.GetLastTaskId();
			Ret.setReturnMessage(String.valueOf(res));
		}
		catch( Exception e )
		{
			Ret.setReturnMessage(e.getMessage());
			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
		}
		finally
		{
			WorkDB.CloseConnect();
		}
		ConvertWebServerPublisher.PrintMessage( false,  "last taskId is "+Ret.getReturnMessage() );
		return Ret;
	}

	private boolean isParamNull(String userId,String sha1) {
		ConvertWebServerPublisher.PrintMessage( false,  "Signt task param validation" );
		ConvertWebServerPublisher.PrintMessage( false,  "userId:" + userId+" sha1base64:" + sha1 );
		if(userId==null || sha1==null|| userId.isEmpty() ||sha1.isEmpty())
			return true;
		return false;
	}
	
	@Override
	public TaskInfoAnswer createSignTask(String userId, String messageBase64,String sha1base64) {
		TaskInfoAnswer RetAnswer;
		ConvertWebServerPublisher.PrintMessage( false,  "Run createSignTask" );
		if(isParamNull(userId,sha1base64)){
			ConvertWebServerPublisher.PrintMessage( true, "one of required parameter is null" );
			RetAnswer = new TaskInfoAnswer();
			RetAnswer.setError( true );
			RetAnswer.setErrorCode(1);
			RetAnswer.setErrorMsg("one of required parameter is null.UserId:"+userId+" sha1base64:"+sha1base64);
			return RetAnswer;
		}
		String decodedSignSha1 = ""; 
		try {
			decodedSignSha1 = new String(Base64.getDecoder().decode(sha1base64));
		}
		catch(Exception ex) {
			RetAnswer = new TaskInfoAnswer();
			RetAnswer.setError( true );
			RetAnswer.setErrorCode(1);
			RetAnswer.setErrorMsg("sha1 base64 error" + ex.getMessage());
			ConvertWebServerPublisher.PrintMessage( true, "sha1 base64 error" + ex.getMessage());
			ex.printStackTrace();
			return RetAnswer;
		}
		RetAnswer = createTask(userId, "ANY", "EDM#" +decodedSignSha1, messageBase64, 1);
		return RetAnswer;
	}
	
	
	
	@Override
	public FileBodyAnswer readTestTemplateFile(long StartPos, long LenRead) {
		
		ReturnInfo RetInfo = new ReturnInfo();
		FileBodyAnswer RetAnswer = new FileBodyAnswer();
		RandomAccessFile ReadFile = null;
		ConvertWebServerPublisher.PrintMessage( false,  "Run readTestTemplateFile" );
		try
		{
			
			ReadFile = new RandomAccessFile( TestTemplateFile , "r" );
			if( LenRead == 0 ) LenRead = ReadFile.length() - StartPos;
			if( (StartPos + LenRead ) > ReadFile.length() ||
			     StartPos < 0 || LenRead < 0 )
				throw( new Exception("Unable to determine file block parameters!"));
			byte[] fileBody = new byte[ (int) LenRead ];
			ReadFile.seek( StartPos );
			ReadFile.read( fileBody, 0, (int) LenRead );
			RetAnswer.setFileOutBodyBase64( fileBody );
		}
		catch( IOException ioe)
		{
			RetAnswer.setError( true );
			if( RetAnswer.getErrorCode() == 0 ) RetAnswer.setErrorCode( 4 );
			if( RetAnswer.getReturnMessage().length() == 0 ) RetAnswer.setReturnMessage(ioe.getMessage());
			else RetAnswer.setReturnMessage( RetAnswer.getReturnMessage() + ";\n" + ioe.getMessage());
			ConvertWebServerPublisher.PrintMessage( true,  ioe.getMessage() );
		}
		catch( Exception e )
		{
			RetAnswer.setError( true );
			if( RetAnswer.getErrorCode() == 0 ) RetAnswer.setErrorCode( 4 );
			if( RetAnswer.getReturnMessage().length() == 0 ) RetAnswer.setReturnMessage(e.getMessage());
			else RetAnswer.setReturnMessage( RetAnswer.getReturnMessage() + ";\n" + e.getMessage());
			ConvertWebServerPublisher.PrintMessage( true,  e.getMessage() );
		}
		finally
		{
			RetAnswer.SetCurrentDateResponse();
			if( ReadFile != null ) try { ReadFile.close(); }catch( Exception e) {}
		}
		return RetAnswer;
	}
}
