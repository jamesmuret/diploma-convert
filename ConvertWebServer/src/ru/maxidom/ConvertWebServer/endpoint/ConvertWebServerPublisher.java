package ru.maxidom.ConvertWebServer.endpoint;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.ws.Endpoint;

import ru.maxidom.ConvertWebServer.ws.ConvertWebServer;

public class ConvertWebServerPublisher {

	public static void main(String[] args) {
		
		Date CurDate = null;
		String WebServicePath;
		
		PrintMessage( false, "Start ConvertWebServer");
		
		try{
			
			if( args.length <1 ) throw( new Exception("Give me start config file"));
			
			StartParams startParams = new StartParams();
			if( startParams.LoadParamsFromFile( args[0] ) == false )
				throw( new Exception("Can't load start params"));
			
			File EndFile = new File( startParams.StopFileName );
			
			WebServicePath = "http://" 
						   + startParams.WebHostame
						   + ":" + startParams.WebPort
						   + "/ConvertWebServer";
			
			Endpoint work = Endpoint.publish(WebServicePath
					                        , new ConvertWebServer( startParams.WorkDirectory
					                        		              , startParams.DBConnection
					                        		              , startParams.DBName
					                        		              , startParams.DBUser
					                        		              , startParams.DBPassword
					                        		              , startParams.TemplateTestName) );
			
			PrintMessage( false, "Publishing " + WebServicePath);
			PrintMessage( false, java.time.LocalDate.now().toString());
			while( !EndFile.exists() && work.isPublished() )
			{
				Thread.sleep( startParams.SleepTime );
				
				CurDate = new Date();
				if( CurDate.getHours() == 7 && CurDate.getMinutes() >= 58 ) break;
				CurDate = null;
			}
			
			if( EndFile.exists() )
			{
				PrintMessage( false, "Find " + startParams.StopFileName );
				EndFile.delete();
			}
			if( work.isPublished()) work.stop();
			
			PrintMessage( false, "End ConvertWebServer");
		}
		catch( Exception ex )
		{
			PrintMessage( true, ex.getMessage() );
		}
	}
	
    static public void PrintMessage( boolean IsError, String strMsg )
    {
    	DateFormat dtForm = null;
    	Date curDate = null;
    	try
    	{
        	dtForm = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        	curDate = new Date();
        	
    		if( IsError == true)
    			System.out.println( dtForm.format(curDate) + " Error: " + strMsg );
    		else
    			System.out.println( dtForm.format(curDate) + " Info: " + strMsg );
    	}

    	catch( Exception e )
    	{
			System.out.println( " Error: " + e.getMessage() );
    	}
    	finally
    	{
    		dtForm = null;
    		curDate = null;
    	}
    }
}
