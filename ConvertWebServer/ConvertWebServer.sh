#!/bin/bash

#
# Config
#

#test -r ${Config} && . ${Config}

export ComProc=/apps/conv-bin
export BatchLog=/apps/conv-bin/log
export WorkDir=/apps/conv-data

export pid="$( ps -ef | grep ConvertWebServer.jar | grep -v grep | awk '{print $2}')"

if [ "$pid" = "" ]
then
   if [ ! -d "$BatchLog/" ]; then 
      mkdir -p $BatchLog
      chmod 775 $BatchLog
   fi

   export FileStartParams=$WorkDir/web_start_params.xml

   echo '<?xml version="1.0" encoding="koi8-r" ?>' > $FileStartParams
   echo '<start_params>' >> $FileStartParams
   echo "	<StopFile>$WorkDir/stopweb.flg</StopFile>" >> $FileStartParams
   echo "	<WorkDirectory>$WorkDir/</WorkDirectory>" >> $FileStartParams
   echo "	<SleepTime>10000</SleepTime>" >> $FileStartParams
   echo "	<WebHostame>0.0.0.0</WebHostame>" >> $FileStartParams
   echo "	<WebPort>4144</WebPort>" >> $FileStartParams
   echo "	<DBConnection>jdbc:mysql://localhost:3306/convdb?autoReconnect=true&amp;useSSL=false</DBConnection>" >> $FileStartParams
   echo "	<DBName>convdb</DBName>" >> $FileStartParams
   echo "	<DBUser>convuser</DBUser>" >> $FileStartParams
   echo "	<DBPassword>eiK&amp;o4wa</DBPassword>" >> $FileStartParams
   echo '</start_params>' >> $FileStartParams

   nice -n 19 java -jar $ComProc/ConvertWebServer.jar $FileStartParams >> $BatchLog/ConvertWebServer.log 2>&1 &
else
   echo  `date "+%d/%m/%y %H:%M:%S"` " ConvertWebServer is already started (pid=$pid)"
fi

