#!/bin/bash

export LogDir=/apps/conv-bin/log
export LogArhDir=/apps/conv-bin/log/arh

export LogArhFileDate="$(date '+%Y%m%d_%H%M%S')"
export LogArhFile="$LogArhDir/convserver_$LogArhFileDate.zip"

if [ ! -d "$LogArhDir/" ]; then 
   mkdir -p $LogArhDir
   chmod 775 $LogArhDir
fi

zip -mj $LogArhFile $LogDir/*.*

find $LogArhDir/*.* -mtime +21 -exec rm {} \;

