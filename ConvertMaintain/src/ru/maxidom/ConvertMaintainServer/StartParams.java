package ru.maxidom.ConvertMaintainServer;

import java.io.File;
import java.nio.file.Path;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class StartParams {

    public Integer DBMonths;
    public String DBConnection;
    public String DBName;
    public String DBUser;
    public String DBPassword;
    public Boolean isProduction;
    public AtomicBoolean isPathsFetched;
    public LinkedBlockingQueue<String> filePathsBuffer;

    public StartParams() {
	DBMonths = 12;
	DBConnection = "";
	DBName = "";
	DBUser = "";
	DBPassword = "";
	isProduction = false;
	filePathsBuffer = new LinkedBlockingQueue<>();
	isPathsFetched = new AtomicBoolean();

    }

    public boolean loadParamsFromFile(String ParamsFileName) {

	try {

	    DocumentBuilderFactory DocFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder DocBuilder = DocFactory.newDocumentBuilder();
	    Document Doc = DocBuilder.parse(new File(ParamsFileName));
	    return findParams(Doc);
	} catch (Exception ex) {
	    Logger.logMessage(true, ex.getMessage());
	    return false;
	}
    }

    private boolean findParams(Node Doc) {

	boolean Ret;
	String ParamName;
	String ParamValue;
	Ret = false;
	NodeList DocList = Doc.getChildNodes();

	for (int i = 0; i < DocList.getLength(); i++) {
	    Node Node = DocList.item(i);
	    if (Node.getNodeName() == "start_params") {
		NodeList ParamsList = Node.getChildNodes();

		for (int j = 0; j < ParamsList.getLength(); j++) {

		    Node Param = ParamsList.item(j);
		    if (Param.getNodeName() == null || Param.hasChildNodes() == false
			    || Param.getFirstChild().getNodeValue() == null)
			continue;
		    ParamName = Param.getNodeName();
		    ParamValue = Param.getFirstChild().getNodeValue().toString().trim();
		    Logger.logMessage(false, "Find: " + ParamName + " - " + ParamValue);
		    if (ParamName == "DBConnection")
			DBConnection = ParamValue.trim();
		    else if (ParamName == "DBName")
			DBName = ParamValue.trim();
		    else if (ParamName == "DBUser")
			DBUser = ParamValue.trim();
		    else if (ParamName == "DBPassword")
			DBPassword = ParamValue.trim();
		    else if (ParamName == "DBMonths")
			DBMonths = Integer.parseInt(ParamValue);
		    else if (ParamName == "ISProd")
			isProduction = Boolean.parseBoolean(ParamValue);
		}
	    }
	}
	Logger.logMessage(false, "ConvertMaintainServer version " + "1.0 only delete files");
	if (DBConnection != "" && DBName != "" && DBUser != "" && DBPassword != "")
	    Ret = true;

	return Ret;
    }
}
