package ru.maxidom.ConvertMaintainServer;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;

public class WorkDBSQL {

    private Connection DBSQLConnection;
    private StartParams dbParams;

    public WorkDBSQL(StartParams params) {
	dbParams = params;
    }

    public boolean IsOpenConnect() {

	boolean ret = false;

	try {

	    if (DBSQLConnection != null)
		ret = !DBSQLConnection.isClosed();
	} catch (Exception e) {
	    Logger.logMessage(true, e.getMessage());
	}

	return ret;
    }

    public boolean OpenConnect() {

	boolean ret = false;

	try {

	    DBSQLConnection = DriverManager.getConnection(dbParams.DBConnection, dbParams.DBUser, dbParams.DBPassword);
	    if (SetAutoCommit(false) == false)
		return ret;
	    ret = true;
	} catch (Exception e) {

	    Logger.logMessage(true, e.getMessage());
	    e.printStackTrace();
	}
	return ret;
    }

    public void CloseConnect() {

	if (!IsOpenConnect())
	    return;

	try {

	    DBSQLConnection.close();
	} catch (SQLException sqle) {
	    Logger.logMessage(true, sqle.getMessage());
	}
    }

    public boolean SetAutoCommit(boolean IsAuto) {

	boolean ret = false;

	try {

	    if (IsOpenConnect() == false)
		return ret;
	    DBSQLConnection.setAutoCommit(IsAuto);
	    ret = true;
	} catch (SQLException sqle) {
	    Logger.logMessage(true, sqle.getMessage());
	    ret = false;
	}

	return ret;
    }

    public boolean commit() {

	boolean ret = false;

	try {

	    if (IsOpenConnect() == false)
		throw (new Exception("No database connection"));

	    DBSQLConnection.commit();
	    ret = true;
	} catch (SQLException sqle) {

	    Logger.logMessage(true, sqle.getMessage());
	    ret = false;
	} catch (Exception e) {

	    Logger.logMessage(true, e.getMessage());
	    ret = false;
	}

	return ret;
    }

    public boolean rollBack() {

	boolean ret = false;

	try {

	    if (IsOpenConnect() == false)
		throw (new Exception("No database connection"));

	    DBSQLConnection.rollback();
	    ret = true;
	} catch (SQLException sqle) {

	    Logger.logMessage(true, sqle.getMessage());
	    ret = false;
	} catch (Exception e) {

	    Logger.logMessage(true, e.getMessage());
	    ret = false;
	}

	return ret;
    }

    public boolean deleteCompletedTasks() {
	
	Logger.logMessage(false, "start task updates");
	boolean ret = false;

	String strQuery = "SELECT t.task_id,t.status, t.in_file,t.out_file FROM " + dbParams.DBName.trim()
		+ ".tasks t	where DATEDIFF(NOW(),t.dt_task) > 4*7*" + dbParams.DBMonths
		+ " and t.status in (30,60,35,0)";

	try (Statement stmt = DBSQLConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
		ResultSet.CONCUR_UPDATABLE); ResultSet resSet = stmt.executeQuery(strQuery)) {

	    while (resSet.next()) {

		String inFileName = resSet.getString(3);
		String outFileName = resSet.getString(4);
		
		if(inFileName!=null)
		dbParams.filePathsBuffer.add(inFileName);
		if(outFileName!=null)
		dbParams.filePathsBuffer.add(outFileName);
		resSet.updateInt("status", -1);
		resSet.updateRow();
	    }
	    ret = true;
	} catch (SQLException sqe) {
	    Logger.logMessage(true, sqe.getMessage());
	    ret = false;
	} catch(NullPointerException e) {
	    Logger.logMessage(true, e.getMessage());
	    ret = false;
	}
	finally {
		dbParams.isPathsFetched.set(true);
                dbParams.filePathsBuffer.add("");
	}
	if (dbParams.isProduction && ret) {
	    String delQuery = "DELETE FROM " + dbParams.DBName.trim() + ".tasks WHERE status = -1";
	    try (PreparedStatement stmt = DBSQLConnection.prepareStatement(delQuery);) {

		int res = stmt.executeUpdate();
		Logger.logMessage(false, res + " rows have been deleted");
		ret = true;
	    } catch (SQLException sqe) {
		Logger.logMessage(true, sqe.getMessage());
    		ret = false;
    	    }

	}
	return ret;
    }
}
