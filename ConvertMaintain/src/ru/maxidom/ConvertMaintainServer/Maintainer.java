package ru.maxidom.ConvertMaintainServer;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

public class Maintainer implements IMaintain {

    private StartParams initParams;
    private WorkDBSQL database;
    private IClean fileDeleter;

    @Override
    public void maintain() {

	ForkJoinPool.commonPool().execute(() -> {

	    try {
		fileDeleter.clean();
	    } catch (Exception e) {
		Logger.logMessage(true, e.getMessage());
	    }
	});

	ForkJoinPool.commonPool().execute(() -> {

	    if (database.deleteCompletedTasks() == false)
		database.rollBack();
	    else
		database.commit();

	});
	ForkJoinPool.commonPool().awaitQuiescence(1, TimeUnit.HOURS);
    }

    @Override
    public void init(String[] args) throws Exception {

	if (args.length < 1)
	    throw (new Exception("Give me start config file"));

	initParams = new StartParams();

	if (initParams.loadParamsFromFile(args[0]) == false)
	    throw (new Exception("Can't load start params"));

	database = new WorkDBSQL(initParams);
	fileDeleter = new DeleteCleaner(initParams);

	if (database.OpenConnect() == false) {
	    Logger.logMessage(true, "not connected to DB");
	    return;
	}

    }

    @Override
    public void close() {

	database.CloseConnect();
    }

}
