package ru.maxidom.ConvertMaintainServer;

public interface IMaintain extends AutoCloseable {

    public void maintain();

    public void init(String[] args) throws Exception;

    public void close();
}
