package ru.maxidom.ConvertMaintainServer;

public class App {

    public static void main(String[] args) {
	IMaintain server = null;
	
	try  {
	    server = new Maintainer();
   	    server.init(args);
	    server.maintain();

	} catch (NullPointerException e) {
	    Logger.logMessage(true, e.getMessage());
	    e.printStackTrace();
	}
	catch (Exception ex) {
	    Logger.logMessage(true, ex.getMessage());
	    ex.printStackTrace();
	}
	finally {
	    server.close();
	}

    }
}
