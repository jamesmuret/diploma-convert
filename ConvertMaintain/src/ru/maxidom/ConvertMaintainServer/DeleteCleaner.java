package ru.maxidom.ConvertMaintainServer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DeleteCleaner implements IClean {

    StartParams fParams;

    public DeleteCleaner(StartParams params) {
	fParams = params;
    }

    @Override
    public void clean() throws Exception {
	
	Logger.logMessage(false, "start file deleter updates");
	
	while (!fParams.isPathsFetched.get()) {
	    // blocks until producer puts smth
	    String deletePath = fParams.filePathsBuffer.take();
	    
	    if (deletePath != null && !deletePath.isEmpty()) {
		Logger.logMessage(false, "trying to delete a file " + deletePath);
		
		try {
		    if (fParams.isProduction)
			Files.deleteIfExists(Paths.get(deletePath));
		} catch (IOException e) {
		    Logger.logMessage(true, "couldn't delete file" + e.getMessage());
		} catch(NullPointerException e) {
		    Logger.logMessage(true, "couldn't delete file" + e.getMessage());
		}
		 
	    }
	}
	 Logger.logMessage(false, "All files work over" );
    }
}
