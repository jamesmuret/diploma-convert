package ru.maxidom.ConvertMaintainServer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	
	static public  void logMessage( boolean IsError, String strMsg )
    {
    	DateFormat dtForm = null;
    	Date curDate = null;

    	try
    	{
        	dtForm = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        	curDate = new Date();
        
    		if( IsError == true)
    			System.out.println( dtForm.format(curDate) + " Error: " + strMsg );
    		else
    			System.out.println( dtForm.format(curDate) + " Info: " + strMsg );
    	}

    	catch( Exception e )
    	{
			System.out.println( " Error: " + e.getMessage() );
    	}
    	finally
    	{
    		dtForm = null;
    		curDate = null;
    	}
    }
}
