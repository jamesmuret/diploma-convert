#!/bin/bash

#
# Config
#

#test -r ${Config} && . ${Config}

export ComProc=/apps/conv-bin
export BatchLog=/apps/conv-bin/log
export WorkDir=/apps/conv-data

export pid="$( ps -ef | grep ConvertMaintainServer.jar | grep -v grep | awk '{print $2}')"

if [ "$pid" = "" ]
then
   if [ ! -d "$BatchLog/" ]; then 
      mkdir -p $BatchLog
      chmod 775 $BatchLog
   fi

   export FileStartParams=$ComProc/maintain_start_params.xml

   echo '<?xml version="1.0" encoding="koi8-r" ?>' > $FileStartParams
   echo '<start_params>' >> $FileStartParams
   echo "	<DBMonths>12</DBMonths>" >> $FileStartParams
   echo "	<isProd>false</isProd>" >> $FileStartParams
   echo "	<DBConnection>jdbc:mysql://convert.m0.maxidom.ru:3306/convdb?autoReconnect=true&amp;useSSL=false</DBConnection>" >> $FileStartParams
   echo "	<DBName>convdb</DBName>" >> $FileStartParams
   echo "	<DBUser>convuser</DBUser>" >> $FileStartParams
   echo "	<DBPassword>eiK&amp;o4wa</DBPassword>" >> $FileStartParams
   echo '</start_params>' >> $FileStartParams
   chmod 777 $FileStartParams

   nice -n 19 java -jar $ComProc/ConvertMaintainServer.jar $FileStartParams >> $BatchLog/ConvertMaintainServer.log 2>&1 &

   chmod 777 $BatchLog/ConvertMaintainServer.log
else
   echo  `date "+%d/%m/%y %H:%M:%S"` " ConvertWorkServer is already started (pid=$pid)"
fi
