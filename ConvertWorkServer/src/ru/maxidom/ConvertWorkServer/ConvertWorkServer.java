package ru.maxidom.ConvertWorkServer;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;

public class ConvertWorkServer 
{
	static private StartParams StartParams = null;
	static private Connection DBConnect = null;
    static private Statement stmtCmd = null;
	static private ConvertTransformClass[] ConvTransThreads = null;		
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args)
    {
		int FreeThread;
		int WaitTheadsCount;
		Date CurDate = null;
  	    ResultSet resSet = null;
		String strQuery;
		long WorkTaskId;

		
		try
		{
			if( args.length < 1 ) throw ( new Exception("Give me start config file"));

			StartParams = new StartParams();
			if( StartParams.LoadParamsFromFile( args[0] ) == false )
				throw ( new Exception("Can't load start params") );

			if( ConnectToDatabase() == false )
				throw( new Exception("Can't connect to database"));
			
			strQuery = "SELECT task_id FROM convdb.tasks WHERE status = 10 and task_id > 0 "
					 + " LIMIT " + StartParams.ThreadsCount;
			
			ConvTransThreads = new ConvertTransformClass[ StartParams.ThreadsCount ];
			
			for( FreeThread = 0; FreeThread < StartParams.ThreadsCount; FreeThread++)
			{
				ConvTransThreads[FreeThread] = new ConvertTransformClass( StartParams.SleepTime
						                                                , DBConnect, StartParams.DBName );
				if( ConvTransThreads[FreeThread].Initial() == false ) 
					throw ( new Exception("Can't initial thead " + FreeThread));
				ConvTransThreads[FreeThread].start();
			}
			File EndFile = new File( StartParams.StopFileName );
			PrintMessage( false, "For end listen create file : " + EndFile.getAbsolutePath());
			while (!EndFile.exists())
			{
				resSet = stmtCmd.executeQuery( strQuery );
				while( resSet.next() )
				{
					if( resSet.getObject( 1 ) == null ) continue;

					WorkTaskId = resSet.getLong( 1 );
					if( resSet.wasNull() == true || WorkTaskId == 0 ) continue;
                	
               	    for( FreeThread = 0; FreeThread < StartParams.ThreadsCount; FreeThread++ )
               	    	if( ConvTransThreads[ FreeThread ].GetWorkTask() == 0 ||
               	    	    ConvTransThreads[ FreeThread ].GetWorkTask() == WorkTaskId ) break;
					
               	    if( FreeThread < StartParams.ThreadsCount  )
               	    {
               	    	if( ConvTransThreads[ FreeThread ].GetWorkTask() == 0  )
               	    	{
               	    		PrintMessage( false, "Read task " + WorkTaskId + " and send to thread " +  FreeThread );
               	    		ConvTransThreads[ FreeThread ].SetWorkTask( WorkTaskId );
               	    	}
               	    }
				}
				try { if( resSet != null ) resSet.close(); } catch( SQLException se ) {}
				Thread.sleep( StartParams.SleepTime ); 
				CurDate = new Date();
				if( CurDate.getHours() == 7 && CurDate.getMinutes() >= 58 ) break;
				CurDate = null;
			}
			if(EndFile.exists())
			{
				PrintMessage( false, "Find " + StartParams.StopFileName );
				EndFile.delete();
			}
		}
		catch( Exception e )
		{
			PrintMessage( true, e.getMessage() );
		}
		finally
		{
			for( FreeThread = 0; FreeThread < StartParams.ThreadsCount; FreeThread++ )
				ConvTransThreads[ FreeThread ].finish();
			WaitTheadsCount = StartParams.ThreadsCount;
			while(  WaitTheadsCount > 0 )
			{
				WaitTheadsCount = StartParams.ThreadsCount;
				for( FreeThread = 0; FreeThread < StartParams.ThreadsCount; FreeThread++ )
					if( ConvTransThreads[ FreeThread ].isAlive() == false ) WaitTheadsCount--;
				try {
					Thread.sleep( StartParams.SleepTime );
				} catch (InterruptedException e) {
					PrintMessage( true, e.getMessage() );
				}
			}
			ConvTransThreads = null;
			try { if( stmtCmd != null ) stmtCmd.close(); } catch( SQLException se ) {}
			try{ if( DBConnect != null ) DBConnect.close(); }catch( SQLException se ) {}
			PrintMessage( false, "End ConvertWorkServer");
		}
    }

	static public boolean ConnectToDatabase()
	{
		boolean Ret = false;
		
		try
		{
			if( DBConnect != null  )
			{
				if( DBConnect.isClosed() == false ) return true;
				
				if( stmtCmd != null ) try{ stmtCmd.close(); }catch( Exception e) {}
				
				stmtCmd = null;
				DBConnect = null;
			}
			
			DBConnect = DriverManager.getConnection( StartParams.DBConnection,
													 StartParams.DBUser, StartParams.DBPassword );
			stmtCmd = DBConnect.createStatement();
			
			PrintMessage( false, "Connect to database " + StartParams.DBConnection );
			
			Ret = true;
		}
		catch( SQLException sqle)
		{
			ConvertWorkServer.PrintMessage( true, sqle.getMessage() );
			Ret = false;
		}
		catch( Exception e)
		{
			ConvertWorkServer.PrintMessage( true, e.getMessage() );
			Ret = false;
		}

		return Ret;
	}

    static public void PrintMessage( boolean IsError, String strMsg )
    {
    	DateFormat dtForm = null;
    	Date curDate = null;
    	
    	try
    	{
        	dtForm = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        	curDate = new Date();
        	
    		if( IsError == true)
    			System.out.println( dtForm.format(curDate) + " Error: " + strMsg );
    		else
    			System.out.println( dtForm.format(curDate) + " Info: " + strMsg );
    	}
    	catch( Exception e )
    	{
			System.out.println( " Error: " + e.getMessage() );
    	}
    	finally
    	{
    		dtForm = null;
    		curDate = null;
    	}
    }
}
