package ru.maxidom.ConvertWorkServer;

import java.sql.ResultSet;
import java.sql.SQLException;

import ru.maxidom.convcore.worker.ConvertWorker;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class ConvertTransformClass extends Thread
{
	private int SleepTime;
	private volatile boolean IsFinish;
	private volatile long WorkTaskId;
	private volatile Connection DBConnect;
	private volatile String DBName;
	private volatile String FileIn;
	private volatile String FileInExt;
	private volatile String FileOut;
	private volatile String FileOutExt;
	private volatile String AlgName;
	private volatile ConvertWorker workConverter = null;
	
	public ConvertTransformClass( int Sleep, Connection dbConnect, String dbName ) 
	{
		ClearAll();
		
		SleepTime = Sleep;
		DBConnect = dbConnect;
		DBName = dbName;
	}
	
	public boolean Initial()
	{
		boolean Ret = true;

		try
		{
			workConverter  = new ConvertWorker(false);
			Ret = true;
		}
		catch( Error er )
		{
			PrintMessage( true, er.getMessage() + "\n");
			Ret = false;
		}
		catch( Exception e )
		{
			PrintMessage( true, e.getMessage() + "\n");
			Ret = false;
		}

		return Ret;
	}

	
	public void ClearAll()
	{
		IsFinish = false;
		WorkTaskId = 0;
		SleepTime = 0;
		DBConnect = null;
		DBName = "";
		
		FileIn = "";
		FileInExt = "";
		FileOut = "";
		FileOutExt = "";
	}
	
	public void finish()
	{
		IsFinish = true;
	}

	public long GetWorkTask()
	{
		return WorkTaskId;
	}
	
	public boolean SetWorkTask( long TaskId )
	{
		if( workConverter == null || WorkTaskId != 0 ) return false;
		
		WorkTaskId = TaskId;
		
		return true;
	}
	
	@Override
	public void run()
	{
		while( IsFinish == false )
		{
			if( WorkTaskId == 0 )
			{
				try
				{
					Thread.sleep( SleepTime );
				}
				catch( Exception e) {}
				continue;
			}
			try
			{
				PrintMessage( false, "Start processing" );
				if( ChangeTaskStatus( 20, null  ) == false ) 
					throw( new Exception("Can't get task to work!"));
				if( GetTaskInfo() == false )
					throw( new Exception("Can't get task information!"));
				PrintMessage( false, " In file: (" +  FileInExt + ") " + FileIn);
				PrintMessage( false, "Out file: (" +  FileOutExt + ") " + FileOut);
				
				// Main convert work
			 float convTime =	 workConverter.doConvert(FileInExt,FileIn,FileOutExt,FileOut,AlgName);
				if( ChangeTaskStatus( 30, null  ) == false )
					throw( new Exception("Can't complete task"));
				PrintMessage(false,"Convert time "+ convTime);
			}
			
			catch( Error er )
			{
				PrintMessage( true, er.getMessage() + "\n");
				if( ChangeTaskStatus( 60, Is100Length(er.getMessage())) == false )
					PrintMessage( true,"Can't set error status to task\n");
			}
			catch( Exception e)
			{
				if( ChangeTaskStatus( 60, Is100Length(e.getMessage())) == false )
					PrintMessage( true,"Can't set error status to task\n");
			}
			finally
			{
				PrintMessage( false, "End processing task");
				WorkTaskId = 0;
			}
		}
	}
	
	private String Is100Length(String message) {
		
		if(message!=null && message.length()>100)
			return message.substring(0, 100);
		
		return message;
	}
	
	private boolean IsOpenConnect()
	{
		boolean Ret = false;
		
		try 
		{
			if( DBConnect != null )
				Ret = !DBConnect.isClosed();
		}
		catch( Exception e ) 
		{
			PrintMessage( true, e.getMessage() );
		}
		
		return Ret;
	}
	
	private boolean ChangeTaskStatus( int NewStatus, String Msg )
	{
		PreparedStatement stmtCmd = null;
		boolean Ret = false;
		
		try
		{
			if( IsOpenConnect() == false ) throw( new Exception("No database connection"));
			
			String strQuery;
			
			strQuery = "UPDATE " + DBName.trim() + ".tasks SET "
					 + "  status = " + NewStatus ;
			
			if( Msg != null )
			{
				Msg = Msg.replace('\'', '-');
				strQuery = strQuery
						 + ", error_msg = '" + Msg.trim() + "'" ;
			}
			
			strQuery = strQuery 
					 + " WHERE task_id = " + WorkTaskId ;  

			stmtCmd = DBConnect.prepareStatement( strQuery );
			stmtCmd.executeUpdate();
			
			Ret = true;
		}
		catch( SQLException sqle)
		{
			PrintMessage( true, sqle.getMessage() );
			Ret = false;
		}
		catch( Exception e)
		{
			PrintMessage( true, e.getMessage() );
			Ret = false;
		}
		finally
		{
			try { if( stmtCmd != null ) stmtCmd.close(); } catch( SQLException se ) {}
		}

		return Ret;
	}
	
	public boolean GetTaskInfo()
	{
		PreparedStatement stmtCmd = null;
		ResultSet resSet = null;
		boolean Ret = false;
		
		try
		{
			if( IsOpenConnect() == false ) throw( new Exception("No database connection"));
			
			String strQuery;
			
			strQuery = "SELECT in_file_type, in_file, out_file_type, out_file,alg_name"
					 + "  FROM " + DBName.trim() + ".tasks " 
					 + " WHERE task_id = " + WorkTaskId;
		
			stmtCmd = DBConnect.prepareStatement( strQuery );
			resSet = stmtCmd.executeQuery();
			
			if( resSet.first() == false ) throw( new Exception("Can't find task"));
			else 
			{
				if( resSet.getObject( 1 ) != null ) FileInExt = resSet.getString( 1 ).trim(); 
				if( resSet.getObject( 2 ) != null ) FileIn = resSet.getString( 2 ).trim(); 
				if( resSet.getObject( 3 ) != null ) FileOutExt = resSet.getString( 3 ).trim(); 
				if( resSet.getObject( 4 ) != null ) FileOut = resSet.getString( 4 ).trim();
				if( resSet.getObject( 5 ) != null ) AlgName = resSet.getString(5).trim();
				
				if( FileInExt == "" || FileIn == "" ||
					FileOutExt == "" || FileOut == "" ) 
					throw( new Exception("Error info data in task!") );
			}
			
			Ret = true;
		}
		catch( SQLException sqle)
		{
			PrintMessage( true, sqle.getMessage() );
		}
		catch( Exception e)
		{
			PrintMessage( true, e.getMessage() );
		}
		finally
		{
			try { if( stmtCmd != null ) stmtCmd.close(); } catch( SQLException se ) {}
		}
		
		return Ret;
	}
	
	private void PrintMessage( boolean IsError, String Message )
	{
		ConvertWorkServer.PrintMessage( IsError, "Task: " +  WorkTaskId +  ": " + Message );
	}
}
