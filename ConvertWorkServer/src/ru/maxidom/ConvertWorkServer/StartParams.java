package ru.maxidom.ConvertWorkServer;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class StartParams {

	public String StopFileName;
	public int SleepTime;
	public int ThreadsCount;
	public String DBConnection;
	public String DBName;
	public String DBUser;
	public String DBPassword;

	public StartParams() {
		StopFileName = "";
		SleepTime = 0;
		ThreadsCount = 1;
		DBConnection = "";
		DBName = "";
		DBUser = "";
		DBPassword = "";

	}

	public boolean LoadParamsFromFile(String ParamsFileName) {

		try {
			DocumentBuilderFactory DocFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder DocBuilder = DocFactory.newDocumentBuilder();

			Document Doc = DocBuilder.parse(new File(ParamsFileName));

			if (FindParams(Doc) == false)
				throw (new Exception("Can't find all parameters!"));
		} catch (Exception e) {
			ConvertWorkServer.PrintMessage(true, e.getMessage());
			return false;
		}

		return true;
	}

	private boolean FindParams(Node Doc) {
		boolean Ret;
		String ParamName;
		String ParamValue;
		Ret = false;
		StopFileName = "";
		NodeList DocList = Doc.getChildNodes();
		for (int i = 0; i < DocList.getLength(); i++) {
			Node Node = DocList.item(i);

			if (Node.getNodeName() == "start_params") {
				NodeList ParamsList = Node.getChildNodes();

				for (int j = 0; j < ParamsList.getLength(); j++) {
					Node Param = ParamsList.item(j);

					if (Param.getNodeName() == null || Param.hasChildNodes() == false
							|| Param.getFirstChild().getNodeValue() == null)
						continue;
					ParamName = Param.getNodeName();
					ParamValue = Param.getFirstChild().getNodeValue().toString().trim();
					ConvertWorkServer.PrintMessage(false, "Find: " + ParamName + " - " + ParamValue);
					if (ParamName == "StopFile")
						StopFileName = ParamValue.trim();
					else if (ParamName == "SleepTime")
						SleepTime = Integer.parseInt(ParamValue);
					else if (ParamName == "ThreadsCount")
						ThreadsCount = Integer.parseInt(ParamValue);
					else if (ParamName == "DBConnection")
						DBConnection = ParamValue.trim();
					else if (ParamName == "DBName")
						DBName = ParamValue.trim();
					else if (ParamName == "DBUser")
						DBUser = ParamValue.trim();
					else if (ParamName == "DBPassword")
						DBPassword = ParamValue.trim();
				}
			}
		}
		ConvertWorkServer.PrintMessage(false,
				"ConvertCore version " + ru.maxidom.convcore.filedata.Dictionary.getCoreConvVer());
		ConvertWorkServer.PrintMessage(false,
				"ConvertCore date " + ru.maxidom.convcore.filedata.Dictionary.getCoreConvDate());
		if (StopFileName != "" && SleepTime != 0 && ThreadsCount > 0 && DBConnection != "" && DBName != ""
				&& DBUser != "" && DBPassword != "")
			Ret = true;

		return Ret;
	}
}
